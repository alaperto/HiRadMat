#include <TStyle.h>
#include <TCanvas.h>
#include <TF1.h> 
#include <TAxis.h>
#include <TGraph.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  //Module 1
  // 17/07/2017 17.22 10161
  //        17/07/2017 17.26 23781
  // 	17/07/2017 18.14 18927
  // 	17/07/2017 19.02 14073
  // 	// 18/07/2017 08.26 46695
  // 	18/07/2017 08.27 6239
  // 	18/07/2017 08.35 5833
  // 	19/07/2017 09.59 2744
  // 	20/07/2017 08.21 2056
  // 	20/07/2017 08.24 1721
  // 	24/07/2017 10.31 969
  // 	// 25/07/2017 14.11 -5749
  // 	// 17/07/2017 19.06 0
  // 	//Module 2
  // 	// 17/07/2017 19.07 10426
  // 	// 17/07/2017 19.29 8454
  // 	// 18/07/2017 09.01 5040
  // 	// 24/07/2017 18.01 778
  // 	// 25/07/2017 14.40 5952

  // d[1]= 23781. ;
  // d[2]= 18927. ;
  // d[3]= 14073. ;
  // d[4]= 6239. ;
  // d[5]= 5833. ;
  // d[6]= 2744. ;
  // d[7]= 2056. ;
  // d[8]= 1721. ;
  // d[9]= 969. ;
 
  // d[10]= 10426. ;
  // d[11]= 8454. ;
  // d[12]= 5040. ;
  // d[13]= 778. ;
  // d[14]= 5952. ;           

  //New definition: 1-58 bin columns

  // 17/07/2017 17.22 9763
  // 17/07/2017 17.26 22714
  // 17/07/2017 18.14 18070
  // 17/07/2017 19.02 13354
  // 18/07/2017 08.26 43169
  // 18/07/2017 08.27 5753
  // 18/07/2017 08.35 5381
  // 19/07/2017 09.59 2213
  // 20/07/2017 08.21 1509
  // 20/07/2017 08.24 1267
  // 24/07/2017 10.31 613
  // 25/07/2017 14.11 -1070
  // 17/07/2017 19.06
  // 17/07/2017 19.07 9782
  // 17/07/2017 19.29 7628
  // 18/07/2017 09.01 4624
  // 24/07/2017 18.01 491
  // 25/07/2017 14.40 1974

  TDatime *b[100]; Double_t c[100], d[100];
  // TLine *l[100]; TLine *l2[100]; TLine *l3[100];
 
  d[1]= 22714. ;
  d[2]= 18070. ;
  d[3]= 13354. ;
  d[4]= 5753. ;
  d[5]= 5381. ;
  d[6]= 2213. ;
  d[7]= 1509. ;
  d[8]= 1267. ;
  d[9]= 613. ;
 
  d[10]= 9782. ;
  d[11]= 7628. ;
  d[12]= 4624. ;
  d[13]= 491. ;
  d[14]= 1974. ;           


  // b[] = new TDatime(2017,07,17,,00); //13:37 1 b; 5 10e10
  b[1] = new TDatime(2017,07,17,17,26,00); //13:37 1 b; 5 10e10
  b[2] = new TDatime(2017,07,17,18,14,00); //13:37 1 b; 5 10e10
  b[3] = new TDatime(2017,07,17,19,2,00); //13:37 1 b; 5 10e10
  b[4] = new TDatime(2017,07,18,8,27,00); //13:37 1 b; 5 10e10
  b[5] = new TDatime(2017,07,18,8,35,00); //13:37 1 b; 5 10e10
  b[6] = new TDatime(2017,07,19,9,59,00); //13:37 1 b; 5 10e10
  b[7] = new TDatime(2017,07,20,8,21,00); //13:37 1 b; 5 10e10
  b[8] = new TDatime(2017,07,20,8,24,00); //13:37 1 b; 5 10e10
  b[9] = new TDatime(2017,07,24,10,31,00); //13:37 1 b; 5 10e10

  b[10] = new TDatime(2017,07,17,19,7,00); //13:37 1 b; 5 10e10
  b[11] = new TDatime(2017,07,17,19,29,00); //13:37 1 b; 5 10e10
  b[12] = new TDatime(2017,07,18,9,1,00); //13:37 1 b; 5 10e10
  b[13] = new TDatime(2017,07,24,18,1,00); //13:37 1 b; 5 10e10
  b[14] = new TDatime(2017,07,25,14,40,00); //13:37 1 b; 5 10e10

  for(int a = 1; a < 15; a++){
    c[a] = b[a]->Convert();
    // cout<<int(c[a])-1500305160<<"  "<<d[a]<<endl;
    cout<<int(c[a])<<"  "<<d[a]<<endl;
    // cout<<" a "<<a<<" c[a "<<int(c[a])<<" b[a "<<b[a]<<" d[a "<<d[a]<<endl;
  }


  TString Module = "4";
  TString list_name = "lists/list_Sources_M"+Module+"_v2.txt";

  TString macro_name = "Decay_M";
  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  gStyle->SetOptTitle(0);
  int qq =200;
  macro_name+=Module;
  TCanvas *plot_mc_data[qq];

  TF1 *expo[qq];
  int m = 0;
  plot_mc_data[m] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);

  TGraph *graph24 = new TGraph(list_name);
  graph24->GetXaxis()->SetTimeDisplay(1);

	
  graph24->GetXaxis()->SetTitle("Day");
  graph24->GetYaxis()->SetTitle("Counts / second");
  graph24->GetYaxis()->SetTitleOffset(1.5);
  graph24->SetMarkerStyle(20);
  graph24->SetMarkerSize(1.2);
  graph24->Draw("AP");

  // expo[m] = new TF1("hey","[0]*exp(-x/[1])",0,579900); //v2 bad 
  // expo[m] = new TF1("hey","[0]*exp(-x/[1])",0,54060);// v3  0.42 d ...
  //  expo[m] = new TF1("hey","[0]*exp(-x/[1])",54060,226680);// v4  1.6 d
  // expo[m] = new TF1("hey","[0]*exp(-x/[1])",226680,579900);// v5 7.11 d
  int Start, Stop;
  if(Module=="1") {Start = 54060; Stop = 579900;}
  if(Module=="2") {Start = 56100; Stop = 606900;}
  if(Module=="3") {Start = 54060; Stop = 606900;}
  if(Module=="3") {Start = 6700; Stop = 606900;}
  if(Module=="3") {Start = 2000; Stop = 606900;}
  if(Module=="4") {Start = 1500307000; Stop = 1500915000;}
  if(Module=="5") {Start = 1500360000; Stop = 1500915000;}

    expo[m] = new TF1("hey","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
 
  expo[m]->SetParameter(0,5000);
  // expo[m]->SetParameter(1,86400*(2.6+3.17+6.183));
  expo[m]->SetParameter(1,0.5*86400/0.6931);
  expo[m]->SetParName(0,"N0");
  expo[m]->SetParName(1,"tau");

  graph24->Fit("hey","R");
  //expo[m]->Draw("Same");

  double ciop0 = expo[m]->GetParameter(0);
  double ciop1 = expo[m]->GetParameter(1);

  cout<<endl<<" "<<ciop0<<" "<<ciop1/86400<<endl;


  m=1;

  plot_mc_data[m] = new TCanvas("Canvas_2_"+macro_name,"Canvas_2_"+macro_name,700,550);

  TGraph *graph = new TGraph(list_name);
  	
  graph->GetXaxis()->SetTitle("Day");
  graph->GetYaxis()->SetTitle("Counts / second");
  graph->GetYaxis()->SetTitleOffset(1.5);

  graph->SetMarkerStyle(20);
  graph->SetMarkerSize(1.2);
  graph->GetXaxis()->SetTimeDisplay(1);
  graph->Draw("AP");
 
  expo[m] = new TF1("hey2","[0]*(exp(-(x-1500305160)/[1]))+[2]*(exp(-(x-1500305160)/[3]))",Start,Stop);// v6 

  // expo[m]->SetParameter(0,23781);
  expo[m]->SetParameter(0,5000);
  expo[m]->SetParameter(1,0.5*86400/0.6931);
  expo[m]->SetParName(0,"N1");
  expo[m]->SetParName(1,"tau1");
  expo[m]->SetParameter(2,1000);
  expo[m]->SetParameter(3,6.2*86400/0.6931);
  expo[m]->SetParName(2,"N2");
  expo[m]->SetParName(3,"tau2");
  expo[m]->SetNpx(1000);

  graph->Fit("hey2","R");
  //expo[m]->Draw("Same");

  double ciap0 = expo[m]->GetParameter(0);
  double ciap1 = expo[m]->GetParameter(1);
  double ciap2 = expo[m]->GetParameter(2);
  double ciap3 = expo[m]->GetParameter(3);

  cout<<endl<<" "<<ciap0<<" "<<ciap1/86400<<" "<<ciap2<<" "<<ciap3/86400<<endl;

  m=99;
    expo[m] = new TF1("hey99","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,ciap0);
  expo[m]->SetParameter(1,ciap1);
  expo[m]->SetLineColor(3);
  expo[m]->SetNpx(1000);
  expo[m]->Draw("SAME");

  m=98;
    expo[m] = new TF1("hey98","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,ciap2);
  expo[m]->SetParameter(1,ciap3);
  expo[m]->SetLineColor(4);
  expo[m]->SetNpx(1000);
  expo[m]->Draw("SAME");

  m=2;

  plot_mc_data[m] = new TCanvas("Canvas_3_"+macro_name,"Canvas_3_"+macro_name,700,550);

  TGraph *graph3 = new TGraph(list_name);
  graph3->GetXaxis()->SetTimeDisplay(1);

	
  graph3->GetXaxis()->SetTitle("Day");
  graph3->GetYaxis()->SetTitle("Counts / second");
  graph3->GetYaxis()->SetTitleOffset(1.5);

  graph3->SetMarkerStyle(20);
  graph3->SetMarkerSize(1.2);
  graph3->Draw("AP");
 
  expo[m] = new TF1("hey3","[0]*(exp(-(x-1500305160)/[1]))+[2]*(exp(-(x-1500305160)/[3]))+[4]*(exp(-(x-1500305160)/[5]))",Start,Stop);// v6 
 
  // expo[m]->SetParameter(0,5000);
  // expo[m]->SetParameter(1,0.5*86400);
  // expo[m]->SetParName(0,"N1");
  // expo[m]->SetParName(1,"tau1");
  // expo[m]->SetParameter(2,1000);
  // expo[m]->SetParameter(3,6*86400);
  // expo[m]->SetParName(2,"N2");
  // expo[m]->SetParName(3,"tau2");
  // expo[m]->SetParameter(4,2000);
  // expo[m]->SetParameter(5,15*86400);
  // expo[m]->SetParName(4,"N3");
  // expo[m]->SetParName(5,"tau3");

  expo[m]->SetParameter(0,1000);
  expo[m]->SetParameter(1,0.5*86400/0.6931);
  expo[m]->SetParName(0,"N1");
  expo[m]->SetParName(1,"tau1");
  expo[m]->SetParameter(2,500);
  expo[m]->SetParameter(3,6*86400/0.6931);
  expo[m]->SetParName(2,"N2");
  expo[m]->SetParName(3,"tau2");
  expo[m]->SetParameter(4,3000);
  expo[m]->SetParameter(5,0.06*86400/0.6931);
  expo[m]->SetParName(4,"N3");
  expo[m]->SetParName(5,"tau3");
  expo[m]->SetNpx(1000);


  graph3->Fit("hey3","R");
  //expo[m]->Draw("Same");

  double cip0 = expo[m]->GetParameter(0);
  double cip1 = expo[m]->GetParameter(1);
  double cip2 = expo[m]->GetParameter(2);
  double cip3 = expo[m]->GetParameter(3);
  double cip4 = expo[m]->GetParameter(4);
  double cip5 = expo[m]->GetParameter(5);

  cout<<endl<<" "<<cip0<<" "<<cip1/86400<<" "<<cip2<<" "<<cip3/86400<<endl;
  cout<<endl<<" "<<cip4<<" "<<cip5/86400<<endl;


  m=97;
    expo[m] = new TF1("hey97","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,cip0);
  expo[m]->SetParameter(1,cip1);
  expo[m]->SetLineColor(3);
  expo[m]->SetNpx(1000);
  expo[m]->Draw("SAME");

  m=96;
    expo[m] = new TF1("hey96","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,cip2);
  expo[m]->SetParameter(1,cip3);
  expo[m]->SetLineColor(4);
  expo[m]->SetNpx(1000);
  expo[m]->Draw("SAME");

  m=95;
    expo[m] = new TF1("hey95","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,cip4);
  expo[m]->SetParameter(1,cip5);
  expo[m]->SetLineColor(5);
  expo[m]->SetNpx(1000);
  expo[m]->Draw("SAME");

    
  m=3;

  plot_mc_data[m] = new TCanvas("Canvas_4_"+macro_name,"Canvas_4_"+macro_name,700,550);

  TGraph *graph4 = new TGraph(list_name);
  graph4->GetXaxis()->SetTimeDisplay(1);
	
  graph4->GetXaxis()->SetTitle("Day");
  graph4->GetYaxis()->SetTitle("Counts / second");
  graph4->GetYaxis()->SetTitleOffset(1.5);

  graph4->SetMarkerStyle(20);
  graph4->SetMarkerSize(1.2);
  graph4->Draw("AP");
 
  expo[m] = new TF1("hey4","[0]*(exp(-(x-1500305160)/[1]))+[2]*(exp(-(x-1500305160)/[3]))+[4]*(exp(-(x-1500305160)/[5]))+[6]*(exp(-(x-1500305160)/[7]))",Start,Stop);// v6 
 
 // expo[m]->SetParameter(0,23781);
  expo[m]->SetParameter(0,5000);
  expo[m]->SetParameter(1,0.5*86400/0.6931);
  expo[m]->SetParName(0,"N1");
  expo[m]->SetParName(1,"tau1");
  expo[m]->SetParameter(2,1000);
  expo[m]->SetParameter(3,6*86400/0.6931);
  expo[m]->SetParName(2,"N2");
  expo[m]->SetParName(3,"tau2");
  expo[m]->SetParameter(4,2000);
  expo[m]->SetParameter(5,15*86400/0.6931);
  expo[m]->SetParName(4,"N3");
  expo[m]->SetParName(5,"tau3");
  expo[m]->SetParameter(6,10000);
  expo[m]->SetParameter(7,0.06*86400/0.6931);
  expo[m]->SetParName(6,"N4");
  expo[m]->SetParName(7,"tau4");
  expo[m]->SetNpx(2000);

  graph4->Fit("hey4","R");
  //expo[m]->Draw("Same");

  double ciup0 = expo[m]->GetParameter(0);
  double ciup1 = expo[m]->GetParameter(1);
  double ciup2 = expo[m]->GetParameter(2);
  double ciup3 = expo[m]->GetParameter(3);
  double ciup4 = expo[m]->GetParameter(4);
  double ciup5 = expo[m]->GetParameter(5);
  double ciup6 = expo[m]->GetParameter(6);
  double ciup7 = expo[m]->GetParameter(7);

  cout<<endl<<" "<<ciup0<<" "<<ciup1/86400<<" "<<ciup2<<" "<<ciup3/86400<<endl;
  cout<<endl<<" "<<ciup4<<" "<<ciup5/86400<<endl;
  cout<<endl<<" "<<ciup6<<" "<<ciup7/86400<<endl;


  m=94;
    expo[m] = new TF1("hey94","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,ciup0);
  expo[m]->SetParameter(1,ciup1);
  expo[m]->SetLineColor(3);
  expo[m]->SetNpx(2000);
  expo[m]->Draw("SAME");

  m=93;
    expo[m] = new TF1("hey93","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,ciup2);
  expo[m]->SetParameter(1,ciup3);
  expo[m]->SetLineColor(4);
  expo[m]->SetNpx(2000);
  expo[m]->Draw("SAME");

  m=92;
    expo[m] = new TF1("hey92","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,ciup4);
  expo[m]->SetParameter(1,ciup5);
  expo[m]->SetLineColor(5);
  expo[m]->SetNpx(2000);
  expo[m]->Draw("SAME");

  m=91;
    expo[m] = new TF1("hey91","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,ciup6);
  expo[m]->SetParameter(1,ciup7);
  expo[m]->SetLineColor(6);
  expo[m]->SetNpx(2000);
  expo[m]->Draw("SAME");


  m=4;

  plot_mc_data[m] = new TCanvas("Canvas_5_"+macro_name,"Canvas_5_"+macro_name,700,550);

  TGraph *graph45 = new TGraph(list_name);
  graph45->GetXaxis()->SetTimeDisplay(1);
	
  graph45->GetXaxis()->SetTitle("Day");
  graph45->GetYaxis()->SetTitle("Counts / second");
  graph45->GetYaxis()->SetTitleOffset(1.5);

  graph45->SetMarkerStyle(20);
  graph45->SetMarkerSize(1.2);
  graph45->Draw("AP");
 
  expo[m] = new TF1("hey5","[0]*(exp(-(x-1500305160)/[1]))+[2]*(exp(-(x-1500305160)/[3]))+[4]*(exp(-(x-1500305160)/[5]))+[6]*(exp(-(x-1500305160)/[7]))+[8]*(exp(-(x-1500305160)/[9]))",Start,Stop);
 
 // expo[m]->SetParameter(0,23781);
  expo[m]->SetParameter(0,0.5*11000);
  expo[m]->FixParameter(1,0.06*86400/0.6931);
  expo[m]->SetParName(0,"N1");
  expo[m]->SetParName(1,"tau1");
  expo[m]->SetParameter(2,0.5*8000);
  expo[m]->FixParameter(3,0.5*86400/0.6931);
  expo[m]->SetParName(2,"N2");
  expo[m]->SetParName(3,"tau2");
  expo[m]->SetParameter(4,0.5*6000);
  expo[m]->FixParameter(5,0.5*86400/0.6931);
  expo[m]->SetParName(4,"N3");
  expo[m]->SetParName(5,"tau3");
  expo[m]->SetParameter(6,0.5*5000);
  expo[m]->FixParameter(7,15*86400/0.6931);
  expo[m]->SetParName(6,"N4");
  expo[m]->SetParName(7,"tau4");
  expo[m]->SetParameter(8,0.5*3000);
  expo[m]->FixParameter(9,6.24*86400/0.6931);
  expo[m]->SetParName(8,"N5");
  expo[m]->SetParName(9,"tau5");
  expo[m]->SetNpx(2000);

  graph45->Fit("hey5","R");
  //expo[m]->Draw("Same");

  double ciupa0 = expo[m]->GetParameter(0);
  double ciupa1 = expo[m]->GetParameter(1);
  double ciupa2 = expo[m]->GetParameter(2);
  double ciupa3 = expo[m]->GetParameter(3);
  double ciupa4 = expo[m]->GetParameter(4);
  double ciupa5 = expo[m]->GetParameter(5);
  double ciupa6 = expo[m]->GetParameter(6);
  double ciupa7 = expo[m]->GetParameter(7);
  double ciupa8 = expo[m]->GetParameter(8);
  double ciupa9 = expo[m]->GetParameter(9);

  cout<<endl<<" "<<ciupa0<<" "<<ciupa1/86400<<endl;
  cout<<endl<<" "<<ciupa2<<" "<<ciupa3/86400<<endl;
  cout<<endl<<" "<<ciupa4<<" "<<ciupa5/86400<<endl;
  cout<<endl<<" "<<ciupa6<<" "<<ciupa7/86400<<endl;
  cout<<endl<<" "<<ciupa8<<" "<<ciupa9/86400<<endl;

  m=80;
  expo[m] = new TF1("hey80","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,ciupa0);
  expo[m]->SetParameter(1,ciupa1);
  expo[m]->SetLineColor(3);
  expo[m]->SetNpx(2000);
  expo[m]->Draw("SAME");

  m=81;
    expo[m] = new TF1("hey81","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,ciupa2);
  expo[m]->SetParameter(1,ciupa3);
  expo[m]->SetLineColor(4);
  expo[m]->SetNpx(2000);
  expo[m]->Draw("SAME");

  m=82;
    expo[m] = new TF1("hey82","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,ciupa4);
  expo[m]->SetParameter(1,ciupa5);
  expo[m]->SetLineColor(5);
  expo[m]->SetNpx(2000);
  expo[m]->Draw("SAME");

  m=83;
  expo[m] = new TF1("hey83","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,ciupa6);
  expo[m]->SetParameter(1,ciupa7);
  expo[m]->SetLineColor(6);
  expo[m]->SetNpx(2000);
  expo[m]->Draw("SAME");

  m=84;
  expo[m] = new TF1("hey84","[0]*exp(-(x-1500305160)/[1])",Start, Stop);// v6 
  expo[m]->SetParameter(0,ciupa8);
  expo[m]->SetParameter(1,ciupa9);
  expo[m]->SetLineColor(7);
  expo[m]->SetNpx(2000);
  expo[m]->Draw("SAME");
      
  app.Run(true);
  return 0;
}
