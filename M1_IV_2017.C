#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
#include <TMultiGraph.h>
#include <TColor.h>
#include <TLegendEntry.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  // void HiradMat_HV_On()
  // {

  //=========Macro generated from canvas: c1/c1
  //=========  (Wed Nov 22 16:50:29 2017) by ROOT version6.10/06

  TCanvas *c1 = new TCanvas("c1", "c1",200,100,700,700);
  // c1->Range(-3.923595,-9.508502e-07,24.9236,8.453653e-06);
  c1->SetFillColor(0);
  c1->SetBorderMode(0);
  c1->SetBorderSize(2);
  c1->SetFrameBorderMode(0);
  c1->SetFrameBorderMode(0);
   
  TMultiGraph *multigraph = new TMultiGraph();
  multigraph->SetName("Module 1 - Stable beams");
  multigraph->SetTitle("Module 1 - Stable beams");
   
  Double_t Reference_fx1[8] = {
    0,
    3,
    6,
    8.979775,
    11.94746,
    15,
    18,
    21};
  Double_t Reference_fy1[8] = {
    6.81e-09,
    2.94e-08,
    5.86e-08,
    1.337133e-07,
    2.479164e-07,
    3.44e-07,
    4.97e-07,
    4.97e-07};
  TGraph *graph = new TGraph(8,Reference_fx1,Reference_fy1);
  graph->SetName("Reference");
  graph->SetTitle("Reference");
  graph->SetLineColor(8);
  graph->SetMarkerColor(8);
  graph->SetMarkerStyle(22);
  graph->SetMarkerSize(0.8);
   
  TH1F *Graph_Reference1 = new TH1F("Graph_Reference1","Reference",100,0,23.1);
  Graph_Reference1->SetMinimum(6.129e-09);
  Graph_Reference1->SetMaximum(5.46019e-07);
  Graph_Reference1->SetDirectory(0);
  Graph_Reference1->SetStats(0);

  Int_t ci;      // for color index setting
  TColor *color; // for color definition with alpha
  ci = TColor::GetColor("#000099");
  Graph_Reference1->SetLineColor(ci);
  Graph_Reference1->GetXaxis()->SetLabelFont(42);
  Graph_Reference1->GetXaxis()->SetLabelSize(0.035);
  Graph_Reference1->GetXaxis()->SetTitleSize(0.035);
  Graph_Reference1->GetXaxis()->SetTitleFont(42);
  Graph_Reference1->GetYaxis()->SetLabelFont(42);
  Graph_Reference1->GetYaxis()->SetLabelSize(0.035);
  Graph_Reference1->GetYaxis()->SetTitleSize(0.035);
  Graph_Reference1->GetYaxis()->SetTitleOffset(0);
  Graph_Reference1->GetYaxis()->SetTitleFont(42);
  Graph_Reference1->GetZaxis()->SetLabelFont(42);
  Graph_Reference1->GetZaxis()->SetLabelSize(0.035);
  Graph_Reference1->GetZaxis()->SetTitleSize(0.035);
  Graph_Reference1->GetZaxis()->SetTitleFont(42);
  Graph_Reference1->SetLineColor(8);
  Graph_Reference1->SetLineStyle(1);
  Graph_Reference1->SetLineWidth(1);
  Graph_Reference1->SetMarkerColor(8);
  Graph_Reference1->SetMarkerStyle(22);
  Graph_Reference1->SetMarkerSize(0.8);
  graph->SetHistogram(Graph_Reference1);
   
  multigraph->Add(graph,"");
   
  Double_t a17_Jul_fx2[8] = {
    -0.003374193,
    2.973034,
    6,
    9,
    12,
    15,
    18,
    21};
  Double_t a17_Jul_fy2[8] = {
    5.74513e-07,
    1.9017e-06,
    2.26e-06,
    2.52e-06,
    2.79e-06,
    2.98e-06,
    2.98e-06,
    2.98e-06};
  graph = new TGraph(8,a17_Jul_fx2,a17_Jul_fy2);
  graph->SetName("17 Jul");
  graph->SetTitle("17 Jul");
  graph->SetLineColor(4);
  graph->SetMarkerColor(4);
  graph->SetMarkerStyle(21);
  graph->SetMarkerSize(0.8);
   
  TH1F *Graph_17sPJul2 = new TH1F("Graph_17sPJul2","17 Jul",100,0,23.1);
  Graph_17sPJul2->SetMinimum(3.202e-07);
  Graph_17sPJul2->SetMaximum(3.2218e-06);
  Graph_17sPJul2->SetDirectory(0);
  Graph_17sPJul2->SetStats(0);

  ci = TColor::GetColor("#000099");
  Graph_17sPJul2->SetLineColor(ci);
  Graph_17sPJul2->GetXaxis()->SetLabelFont(42);
  Graph_17sPJul2->GetXaxis()->SetLabelSize(0.035);
  Graph_17sPJul2->GetXaxis()->SetTitleSize(0.035);
  Graph_17sPJul2->GetXaxis()->SetTitleFont(42);
  Graph_17sPJul2->GetYaxis()->SetLabelFont(42);
  Graph_17sPJul2->GetYaxis()->SetLabelSize(0.035);
  Graph_17sPJul2->GetYaxis()->SetTitleSize(0.035);
  Graph_17sPJul2->GetYaxis()->SetTitleOffset(0);
  Graph_17sPJul2->GetYaxis()->SetTitleFont(42);
  Graph_17sPJul2->GetZaxis()->SetLabelFont(42);
  Graph_17sPJul2->GetZaxis()->SetLabelSize(0.035);
  Graph_17sPJul2->GetZaxis()->SetTitleSize(0.035);
  Graph_17sPJul2->GetZaxis()->SetTitleFont(42);
  Graph_17sPJul2->SetLineColor(4);
  Graph_17sPJul2->SetLineStyle(1);
  Graph_17sPJul2->SetLineWidth(1);
  Graph_17sPJul2->SetMarkerColor(4);
  Graph_17sPJul2->SetMarkerStyle(22);
  Graph_17sPJul2->SetMarkerSize(0.8);
  graph->SetHistogram(Graph_17sPJul2);
   
  multigraph->Add(graph,"");
   
  Double_t b24_Jul_fx3[8] = {
    0,
    2.935955,
    6,
    9,
    12,
    15,
    18,
    21};
  Double_t b24_Jul_fy3[8] = {
    4.01e-07,
    4.605681e-06,
    5.63e-06,
    6.16e-06,
    6.5e-06,
    6.73e-06,
    6.94e-06,
    7.16e-06};
  graph = new TGraph(8,b24_Jul_fx3,b24_Jul_fy3);
  graph->SetName("24 Jul");
  graph->SetTitle("24 Jul");
  graph->SetLineColor(2);
  graph->SetMarkerColor(2);
  graph->SetMarkerStyle(20);
  graph->SetMarkerSize(0.8);
   
  TH1F *Graph_24sPJul3 = new TH1F("Graph_24sPJul3","24 Jul",100,0,23.1);
  Graph_24sPJul3->SetMinimum(3.609e-07);
  Graph_24sPJul3->SetMaximum(7.8359e-06);
  Graph_24sPJul3->SetDirectory(0);
  Graph_24sPJul3->SetStats(0);

  ci = TColor::GetColor("#000099");
  Graph_24sPJul3->SetLineColor(ci);
  Graph_24sPJul3->GetXaxis()->SetLabelFont(42);
  Graph_24sPJul3->GetXaxis()->SetLabelSize(0.035);
  Graph_24sPJul3->GetXaxis()->SetTitleSize(0.035);
  Graph_24sPJul3->GetXaxis()->SetTitleFont(42);
  Graph_24sPJul3->GetYaxis()->SetLabelFont(42);
  Graph_24sPJul3->GetYaxis()->SetLabelSize(0.035);
  Graph_24sPJul3->GetYaxis()->SetTitleSize(0.035);
  Graph_24sPJul3->GetYaxis()->SetTitleOffset(0);
  Graph_24sPJul3->GetYaxis()->SetTitleFont(42);
  Graph_24sPJul3->GetZaxis()->SetLabelFont(42);
  Graph_24sPJul3->GetZaxis()->SetLabelSize(0.035);
  Graph_24sPJul3->GetZaxis()->SetTitleSize(0.035);
  Graph_24sPJul3->GetZaxis()->SetTitleFont(42);
  Graph_24sPJul3->SetLineColor(2);
  Graph_24sPJul3->SetLineStyle(1);
  Graph_24sPJul3->SetLineWidth(1);
  Graph_24sPJul3->SetMarkerColor(2);
  Graph_24sPJul3->SetMarkerStyle(22);
  Graph_24sPJul3->SetMarkerSize(0.8);
  graph->SetHistogram(Graph_24sPJul3);
   
  multigraph->Add(graph,"");
  multigraph->Draw("APL");   multigraph->GetXaxis()->SetTitle("Bias Voltage [V]");
  multigraph->GetXaxis()->SetLabelFont(42);
  multigraph->GetXaxis()->SetLabelSize(0.03);
  multigraph->GetXaxis()->SetTitleSize(0.03);
  multigraph->GetXaxis()->SetTitleFont(42);
  multigraph->GetYaxis()->SetTitle("Leakage current (normalized at 273 K) [A]");
  multigraph->GetYaxis()->SetLabelFont(42);
  multigraph->GetYaxis()->SetLabelSize(0.03);
  multigraph->GetYaxis()->SetTitleSize(0.03);
  multigraph->GetYaxis()->SetTitleOffset(1.2);
  multigraph->GetYaxis()->SetTitleFont(42);
   
   
  TLegend *leg2 = new TLegend(0.67,0.58,0.85,0.77);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.038);
  leg2->AddEntry(Graph_Reference1,"Reference ","pl");
  leg2->AddEntry(Graph_17sPJul2,"17 July 2017 ","pl");
  leg2->AddEntry(Graph_24sPJul3,"24 July 2017 ","pl");
  leg2->Draw("SAME");    

  app.Run(true);
  return 0;
}

// }
