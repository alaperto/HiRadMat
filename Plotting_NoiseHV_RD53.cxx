#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  TString PRINT = "Yes";
  TString fol = "./RD53_NoiseHV/";

  //TString list_name =fol+"0.txt";
  //TString M_name = "F02_91_02_25_1E";
  // TString M_name = "F02_02_50_1E_01";
  // TString M_name = "F02_02_50_1E_03";

  //TString M_name="M2_Diff_1000e", title="F02_91_02_25_1E", Volts="_5V";//Lower
  //TString M_name="M3_Diff_1000e", title="F02_02_50_1E_03", Volts="_1V";//Limit
  //TString M_name="E33_Diff_1000e", title="F03_30_50x50-1E_E3.3", Volts="_1V";//Limit
  //TString M_name="A12_Diff_1000e", title="F03_30_25x100-2E_A1.2", Volts="_1V";//Limit
  //TString M_name="D113_Diff_1000e", title="F03_30_25x100-1E_D11.3", Volts="_1V";//Limit
  //TString M_name="W53_Diff_1000e", title="F02_05_25x100-1E_03", Volts="_1V";//Limit
  //TString M_name="B44_Diff_1000e", title="F03_30_25x100-2E_B4.4", Volts="_1V";//Limit
  TString M_name="C43_Diff_1000e", title="F03_30_25x100-2E_C4.3", Volts="_1V";//Limit

  int ccc = 100;
  TString u[ccc];
  Float_t v[ccc], ev[ccc], a[ccc], ea[ccc], sa[ccc];
  int cc=0;
 
  ifstream input3(fol+M_name+Volts+".dat");//reduced 0 5 10-20-100
  while(!input3.eof()){
    //       Volt +/- unc  <Noise> (sigma) +/- unc
    input3 >>v[cc]>>ev[cc]>>a[cc]>>ea[cc]>>sa[cc];
    cout<<cc<<"  v  "<<v[cc]<<"  at  "<<a[cc]<<"  +/-  "<<sa[cc]<<" , sigma  "<<ea[cc]<<endl;
    cc++;
  }

  cc--;//remove empty lastone

  // TString uB[ccc];
  // Float_t vB[ccc], aB[ccc];
  // int ccB=0;
 
  // ifstream input3B(fol+M_name+"_bared.dat");//from wafer 0-80
  // while(!input3B.eof()){
  //   input3B >>vB[ccB]>>aB[ccB];
  //   cout<<ccB<<"  vB  "<<vB[ccB]<<"  aB  "<<aB[ccB]<<endl;
  //   ccB++;
  // }
  
  TCanvas *c3 = new TCanvas("c3","c3",20,10,800,500);
  c3->cd();  
  TGraphErrors *IV = new TGraphErrors(cc,&v[0],&a[0],&ev[0],&sa[0]);
  IV->SetTitle(title);
  IV->GetYaxis()->SetTitle("Noise [e]");
  IV->GetXaxis()->SetTitle("Bias [V]");
  IV->SetMarkerStyle(20);
  IV->SetMarkerSize(1);
  IV->Draw("ALP");
   // TGraph *IVB = new TGraph(ccB,&vB[0],&aB[0]);
   // for (int i=0;i<IVB->GetN();i++) IVB->GetY()[i] /= 1e-6; 
  // IVB->SetMarkerStyle(20);
  // IVB->SetMarkerSize(1);
  // TGraphErrors *IVB = new TGraphErrors(cc,&v[0],&a[0],&ev[0],&ea[0]);
  // IVB->SetFillColor(2);
  // IVB->Draw("sameCF");
  
  TLegend *leg2 = new TLegend(0.72,0.88,0.85,0.8);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.033);
  leg2->AddEntry(IV,"1000e Diff FE");
  // leg2->AddEntry(IVB,"wafer");
  leg2->Draw("SAME");   

  if(PRINT=="Yes") {
    TString plotName="plots_to_eos/"+title+"_NoiseHV.png";
    c3->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }

  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
         
  app.Run(true);
  return 0;
}
