#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  // TString mod = "IFAE";
  //TString mod = "_A8.5";
  //TString mod = "_M23";
  //TString mod = "_M2_25C";
  TString mod = "_M2_all";

  TString folder = "RD53_IV";
  TString list_name ="lists/list_"+folder+mod;

  int limit = 350;  

  TString PRINT = "Yes";
  TString separate_plots = "Yeso"; //"Yes" separate canvas, "whatever" superimpose on the same canvas
  TString save_GIF="Yeso"; //"Yes" save animated GIF, "whatever" not save
  if(save_GIF=="Yes") separate_plots="Yeso";
  if(separate_plots=="Yes") save_GIF="No";

  int ccc = 400;

  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  // gStyle->SetOptTitle(0);
  int qq =400;
  TCanvas *plot_mc_data[qq];
  TGraph *IV[qq];
  TString plotName="";
  TString macro_name = ""; int m = 0;

  // const char *IVbunch[8] = {"","IFAE -60 C","0 C","-10 C","-20 C","-30 C","-40 C","-40 C (bis)"};
  //const char *IVbunch[4] = {"","module","module (after stress)","wafer"};
  //const char *IVbunch[14] = {"","module","module","module","module","module","module","module","module","module","module","module","module","module"};
  //const char *IVbunch[7] = {"","300V","250V","220V","110V","130V","50V"};
  //const char *IVbunch[7] = {"","0 C","-10 C","-20 C","-25 C","-30 C","-40 C"};
  const char *IVbunch[7] = {"","-40 C","-30 C","-25 C","-20 C","-10 C","0 C"};
  
  TLegend *leg2 = new TLegend(0.15,0.87,0.25,0.7);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.033);
 
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________WELCOME____TO___THE____PLOTTING_______________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;

  cout<<list_name<<endl;

  ifstream input2(list_name); 
  while(!input2.eof()){
    input2 >> macro_name;
    cout<<macro_name<<endl;
    m++;
    
    if(macro_name!=""){

      if(separate_plots=="Yes")
	plot_mc_data[m] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);
      else
       	if(m==1) {plot_mc_data[1] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);
	  // plot_mc_data[1]->DrawFrame(-100,-1e-7,0,0);
	}
      
      TString u[ccc];
      Float_t v[ccc], a[ccc];
      int cc=0;
      for(int o = 0;  o<ccc; o++) {v[o] = 0; a[o]=0;}
      
      ifstream input3(macro_name); 
      while(!input3.eof()){

	input3 >>v[cc]>>a[cc];
	cout<<cc<<"  v  "<<v[cc]<<"  a  "<<a[cc]<<endl;
	cc++;

	// input3 >>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>v[cc]>>u[cc]>>u[cc]>>u[cc]>>a[cc]>>u[cc];
	// cout<<cc<<"  v  "<<v[cc]<<"  a  "<<a[cc]<<endl;
      }

      cc--;

      // Float_t vp[ccc], ap[ccc];
      // for(int o = 0;  o<ccc; o++) {vp[o] = -1*v[o]; ap[o] = -1*a[o];}
  
      IV[m] = new TGraph(cc,&v[0],&a[0]);
      // IV[m]->SetTitle("IV plot");
      IV[m]->SetTitle(" ");
      // IV[m]->GetYaxis()->SetTitle("Leakage current (36.5 #circC) [#muA]");
      IV[m]->GetXaxis()->SetTitleOffset(1.4);
      IV[m]->GetXaxis()->SetTitle("Bias voltage [V]");
      IV[m]->GetYaxis()->SetTitleOffset(1.2);
      IV[m]->GetYaxis()->SetTitle("Current [#muA]");
      IV[m]->SetMarkerColor(m);
      if(m>=5)IV[m]->SetMarkerColor(m+1);
      if(m>=9)IV[m]->SetMarkerColor(m-8);
      IV[m]->SetMarkerStyle(20);
      IV[m]->SetMarkerSize(1);
      for (int i=0;i<IV[m]->GetN();i++) IV[m]->GetY()[i] /= 1e-6; 
      IV[m]->GetYaxis()->SetRangeUser(0,limit);
      IV[m]->GetYaxis()->SetRangeUser(-0.5,210.5);
      //IV[m]->GetYaxis()->SetRangeUser(-0.5,15.5);


      
      if(separate_plots=="Yes") IV[m]->Draw("ALP");
      else  {
      	if(m==1) IV[m]->Draw("ALP");
      	else IV[m]->Draw("sameLP");

	leg2->AddEntry(IV[m],IVbunch[m],"p");
	if(folder == "RD53_IV" && m==3) leg2->Draw("SAME");    
 	
      	if(save_GIF=="Yes"){
      	  plotName="GIF/"+folder+".gif";
      	  plot_mc_data[1]->Print(plotName+"+100");
      	}
      }

    } 
  } 

    
  
  if(save_GIF=="Yes") cout<<"GIF Saved in: "<<plotName<<endl<<endl;
    
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           

  app.Run(true);
  return 0;
}
