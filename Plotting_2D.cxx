#include <TStyle.h>
#include <TCanvas.h>
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){
  //type: "D" for Digital, "A" for Analog, "T" for threshold off, "O" for threshold on, "S" for source 
  //Please insert: "type_module_date_time.C"

  //reference 1, 2, 3 modules
  //TString macro_name = "O_1_19_1239.C";
  // TString macro_name = "O_2_19_1406.C";
  //TString macro_name = "OS_3_23_1319.C";

  //TString macro_name = "S_1_18_0826.C";
  // TString macro_name = "S_1_17_1809.C";
   TString macro_name = "S_1_25_1420.C";

  int m = 0;
  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  gStyle->SetOptTitle(0);
  int qq =200;

  TCanvas *plot_mc_data[qq];
  TH2F *Occup_0_0_MA;
  int aa[40000]; double bb[40000], cc[40000], dd[40000], ff[400000]; int a=0,b=0,c=0,d=0,e=0,f=1;


    if(macro_name!=""){
      plot_mc_data[m] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);//550

    Occup_0_0_MA = new TH2F("Plot_"+macro_name,"Plot_"+macro_name,80,-0.5,79.5,336,-0.5,335.5);

    a=0,b=0,c=0,d=0,e=0,f=1;

    TString list_name = "tables/"+macro_name+".txt";
    ifstream input3(list_name); 
    while(!input3.eof()){
      input3 >> ff[f];
      if(d==0)
      if(ff[2]!=0){
	if(f%3==2) {aa[f/3+1]=ff[f]; e=f/3+1;
	  if(ff[f]==0) d=1;}
	if(f%3==0){bb[f/3]=ff[f];}
      }
      else{
	if(f%4==3) {aa[f/4]=ff[f]; e=f/4;
	  if(ff[f]==0) d=1;}
	if(f%4==0){bb[f/4]=ff[f];}
      }
      f++;
    }
    
    //for debug purpose
    //for(int x = 1; x < 26; x++)
    // for(int x = 1; x < e; x++)
    //   cout<<x<<"   "<<aa[x]<<"   "<<bb[x]<<endl;
    
    for(int x = 1; x < e; x++)
      {
	// if(bb[x]==1.053496) bb[x]=0;
	Occup_0_0_MA->SetBinContent(aa[x],bb[x]);
      }
    
    Occup_0_0_MA->Draw();
    Occup_0_0_MA->SetMinimum(0);
    Occup_0_0_MA->SetMaximum(300);
    Occup_0_0_MA->GetXaxis()->SetTitle("Column");
    // Occup_0_0_MA->GetXaxis()->SetNdivisions(-401);
    // Occup_0_0_MA->GetXaxis()->SetLabelFont(42);
    // Occup_0_0_MA->GetXaxis()->SetTitleSize(0.03);
    // Occup_0_0_MA->GetXaxis()->SetTitleOffset(0.8);
    // Occup_0_0_MA->GetXaxis()->SetTitleFont(42);
    Occup_0_0_MA->GetYaxis()->SetTitle("Row");
    // Occup_0_0_MA->GetYaxis()->SetBinLabel(336,"0");
    // Occup_0_0_MA->GetYaxis()->SetBinLabel(236,"100");
    // Occup_0_0_MA->GetYaxis()->SetBinLabel(136,"200");
    // Occup_0_0_MA->GetYaxis()->SetBinLabel(36,"300");
    // Occup_0_0_MA->GetYaxis()->SetNdivisions(-401);
    // Occup_0_0_MA->GetYaxis()->SetLabelFont(42);
    // Occup_0_0_MA->GetYaxis()->SetLabelSize(0.045);
    // Occup_0_0_MA->GetYaxis()->SetTitleSize(0.03);
    // Occup_0_0_MA->GetYaxis()->SetTickLength(0.015);
    // Occup_0_0_MA->GetYaxis()->SetTitleOffset(1.1);
    // Occup_0_0_MA->GetYaxis()->SetTitleFont(42);
    // Occup_0_0_MA->GetZaxis()->SetLabelFont(42);
    // Occup_0_0_MA->GetZaxis()->SetLabelSize(0.035);
    // Occup_0_0_MA->GetZaxis()->SetTitleSize(0.035);
    // Occup_0_0_MA->GetZaxis()->SetTitleFont(42);
    Occup_0_0_MA->Draw("COLZ");
   
    }
      
  app.Run(true);
  return 0;
}
