#include <TStyle.h>
#include <TCanvas.h>
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  TString which_list = "_O"; //"" for all, "_D" for Digital, "_A" for Analog, "_T" for threshold off, "_O" for threshold on, "_S" for source 
  TString module = "1"; //"1": M1, "2": M2, "3" M1 special, "4" M2 special
  which_list+=module;
  TString list_name= "lists/list_2D_macros"+which_list;
  TString plot_projections = "Yes"; //"Yes" plot X and Y projections in separate canvas, "whatever" to do not plot them
  TString separate_plots = "Yeso"; //"Yes" separate canvas, "whatever" superimpose on the same canvas
  TString save_GIF="Yesa"; //"Yes" save animated GIF, "whatever" not save
  int k=3; //skip to plot: O3->3, T3->5
  TString do_differential="Yeso"; //"Yes" subtract the previous map, "whatever" do not subtract

  int qq =200;
  TCanvas *Canvas_2D[qq];
  TCanvas *Canvas_Xpro[qq];
  TCanvas *Canvas_Ypro[qq];
  TH1D *Xpro[qq], *Ypro[qq];
  TH2F *Occup_0_0_MA[qq];
  int aa[40000]; double bb[40000], cc[40000], dd[40000], ff[400000]; int a=0,b=0,c=0,d=0,e=0,f=1;
  TString macro_name = ""; int m = 0;
  TString plotName="";
  if(separate_plots=="Yes") save_GIF="No";
  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  // gStyle->SetOptTitle(0);
  
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________WELCOME____TO___THE____FITTING_______________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;

  ifstream input2(list_name); 
  while(!input2.eof()){
    input2 >> macro_name;
    m++;
    if(macro_name!=""){

      // if(separate_plots=="Yes")
      // 	Canvas_2D[m] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);
      // else
      // 	if(m==1) Canvas_2D[1] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);

      Occup_0_0_MA[m] = new TH2F("Plot_"+macro_name,"Plot_"+macro_name,80,-0.5,79.5,336,-0.5,335.5);

      a=0,b=0,c=0,d=0,e=0,f=1;

      TString list_name = "tables/"+macro_name+".txt";
      ifstream input3(list_name); 
      while(!input3.eof()){
	input3 >> ff[f];
	if(d==0)
	  if(ff[2]!=0){
	    if(f%3==2) {aa[f/3+1]=ff[f]; e=f/3+1;
	      if(ff[f]==0) d=1;}
	    if(f%3==0){bb[f/3]=ff[f];}
	  }
	  else{
	    if(f%4==3) {aa[f/4]=ff[f]; e=f/4;
	      if(ff[f]==0) d=1;}
	    if(f%4==0){bb[f/4]=ff[f];}
	  }
	f++;
      }
  
    
      //for debug purpose
      //for(int x = 1; x < 26; x++)
      // for(int x = 1; x < e; x++)
      //   cout<<x<<"   "<<aa[x]<<"   "<<bb[x]<<<<endl;
    
      for(int x = 1; x < e; x++)
	{
	  if(plot_projections=="Yes"){
	    if(bb[x]>1500) bb[x]=0;
	    if(bb[x]<0) bb[x]=0;
	  }
	  Occup_0_0_MA[m]->SetBinContent(aa[x],bb[x]);
	}

      Occup_0_0_MA[1]->SetTitle(macro_name);//Change Title for every file!
      Occup_0_0_MA[m]->GetXaxis()->SetTitle("Column");
      Occup_0_0_MA[m]->GetXaxis()->SetNdivisions(-401);
      Occup_0_0_MA[m]->GetXaxis()->SetLabelFont(42);
      Occup_0_0_MA[m]->GetXaxis()->SetTitleSize(0.03);
      Occup_0_0_MA[m]->GetXaxis()->SetTitleOffset(0.8);
      Occup_0_0_MA[m]->GetXaxis()->SetTitleFont(42);
      Occup_0_0_MA[m]->GetYaxis()->SetTitle("Row");
      Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(336,"0");
      Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(236,"100");
      Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(136,"200");
      Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(36,"300");
      Occup_0_0_MA[m]->GetYaxis()->SetNdivisions(-401);
      Occup_0_0_MA[m]->GetYaxis()->SetLabelFont(42);
      Occup_0_0_MA[m]->GetYaxis()->SetLabelSize(0.045);
      Occup_0_0_MA[m]->GetYaxis()->SetTitleSize(0.03);
      Occup_0_0_MA[m]->GetYaxis()->SetTickLength(0.015);
      Occup_0_0_MA[m]->GetYaxis()->SetTitleOffset(1.1);
      Occup_0_0_MA[m]->GetYaxis()->SetTitleFont(42);
      Occup_0_0_MA[m]->GetZaxis()->SetLabelFont(42);
      Occup_0_0_MA[m]->GetZaxis()->SetLabelSize(0.035);
      Occup_0_0_MA[m]->GetZaxis()->SetTitleSize(0.035);
      Occup_0_0_MA[m]->GetZaxis()->SetTitleFont(42);      
      Occup_0_0_MA[m]->SetMinimum(0);
      Occup_0_0_MA[m]->SetMaximum(200);
      if(which_list=="_T1") Occup_0_0_MA[m]->SetMaximum(500);
      if(which_list=="_T2") Occup_0_0_MA[m]->SetMaximum(500);
      if(which_list=="_T3") Occup_0_0_MA[m]->SetMaximum(500);
      if(which_list=="_O1") Occup_0_0_MA[m]->SetMaximum(200);
      if(which_list=="_O2") Occup_0_0_MA[m]->SetMaximum(200);
      if(which_list=="_O3") Occup_0_0_MA[m]->SetMaximum(300);
      if(which_list=="_S1") Occup_0_0_MA[m]->SetMaximum(100);
      if(which_list=="_S2") Occup_0_0_MA[m]->SetMaximum(100);
      if(which_list=="_D") Occup_0_0_MA[m]->SetMaximum(220);
      if(which_list=="_A") Occup_0_0_MA[m]->SetMaximum(220);

      // if(separate_plots=="Yes") Occup_0_0_MA[m]->Draw("COLZ");
      // else  {
      // 	if(m==1) Occup_0_0_MA[m]->Draw("COLZ");
      // 	else Occup_0_0_MA[m]->Draw("COLZSAME");
	
      // 	// 	if(save_GIF=="Yes"){
      // 	// 	  plotName="saved_plots/Animated_2D"+which_list+".gif+50";
      // 	// 	  Canvas_2D[1]->Print(plotName.Data());
      // 	// 	}
      // }


      if(m==k){
	// Canvas_Xpro[k] = new TCanvas("Canvas_Xpro_"+macro_name,"Canvas_Xpro_"+macro_name,700,550);
	Canvas_Ypro[k] = new TCanvas("Canvas_Xproj_"+macro_name,"Canvas_Xproj_"+macro_name,700,850);
	Canvas_Ypro[k]->Divide(1,2);
      }
      if(m>k)
	if(plot_projections=="Yes"){
	  
	  if(do_differential=="Yes") Occup_0_0_MA[m]->Add( Occup_0_0_MA[m-1],-1); //skipper: differential
	  else  Occup_0_0_MA[m]->Add( Occup_0_0_MA[k],-1);
	  
	   Occup_0_0_MA[m]->SetMaximum(500);
	   // Occup_0_0_MA[m]->SetMaximum(300);
	  Occup_0_0_MA[m]->SetMinimum(0);
	
	  // Canvas_Xpro[m] = new TCanvas("Canvas_Xpro_"+macro_name,"Canvas_Xpro_"+macro_name,700,550);
	  // Canvas_Xpro[k]->cd();
	  Xpro[m] = Occup_0_0_MA[m]->ProjectionX();//"Xpro_"+macro_name,20,60); //80
	  Xpro[k+1]->SetTitle("X_projection_"+macro_name);
	  Xpro[m]->GetXaxis()->SetRangeUser(5,75);
	  // Xpro[m]->GetYaxis()->SetRangeUser(-10000,75000);
	  Xpro[m]->GetYaxis()->SetRangeUser(-5000,60000);
	  //Xpro[m]->GetYaxis()->SetRangeUser(-10000,30000);
	  //Xpro[m]->GetYaxis()->SetRangeUser(-20000,50000);
	  Xpro[m]->SetLineColor(m%8+1);
	  //Xpro[m]->Draw("SAME");
	  //Xpro[m]->Scale(1/336);
	  
	  // if(save_GIF=="Yesa"){
	  //   plotName="saved_plots/Animated_Xpro_2D"+which_list+".gif+150";
	  //   Canvas_Xpro[k]->Print(plotName.Data());
	  // }  
	      
	  Canvas_Ypro[k]->cd(1);
	  Occup_0_0_MA[m]->Draw("COLZ");

	  Canvas_Ypro[k]->cd(2);
	  Xpro[m]->Draw("SAME");
	
	  if(save_GIF=="Yesa"){  
	    // plotName="saved_plots/Animated_Xproj_2D"+which_list+".gif+150";
	    if(do_differential=="Yes")  plotName="saved_plots/Animated_Xproj_2D"+which_list+"_differ_skip_"; //skipper: differential
	    else  plotName="saved_plots/Animated_Xproj_2D"+which_list+"_skip_";
	    plotName+=k;
	    plotName+=".gif";
	    Canvas_Ypro[k]->Print(plotName+"+150");
	    if(do_differential=="Yes")   Occup_0_0_MA[m]->Add( Occup_0_0_MA[m-1],1); //skipper: differential
	  }  
	  
	  // Canvas_Ypro[m] = new TCanvas("Canvas_Ypro_"+macro_name,"Canvas_Ypro_"+macro_name,700,550);
	  // Ypro[m] = Occup_0_0_MA[m]->ProjectionY();//"Ypro_"+macro_name,50,250); //336
	  // Ypro[m]->GetXaxis()->SetRangeUser(10,330);
	  // Ypro[m]->Draw();
	  
	}
      
      
    }
  }
  
  if(save_GIF=="Yes") cout<<"GIF Saved in: "<<plotName<<endl;
  if(save_GIF=="Yesa") cout<<"GIF Saved in: "<<plotName<<endl;
  
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
      
  app.Run(true);
  return 0;
}
