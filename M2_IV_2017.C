#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
#include <TMultiGraph.h>
#include <TColor.h>
#include <TLegendEntry.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  // void HiradMat_HV_Off()
  // {

  //=========Macro generated from canvas: c1/c1
  //=========  (Wed Nov 22 17:17:01 2017) by ROOT version6.10/06

  TCanvas *c1 = new TCanvas("c1", "c1",200,100,700,700);
  // c1->Range(-3.9375,-9.753386e-07,24.9375,8.778047e-06);
  c1->SetFillColor(0);
  c1->SetBorderMode(0);
  c1->SetBorderSize(2);
  c1->SetFrameBorderMode(0);
  c1->SetFrameBorderMode(0);
   
  TMultiGraph *multigraph = new TMultiGraph();
  multigraph->SetName("Module 2 - Stand-by");
  multigraph->SetTitle("Module 2 - Stand-by");
   
  Double_t Reference_fx1[8] = {
    0,
    2.970988,
    6,
    8.969377,
    12,
    15,
    18,
    21};
  Double_t Reference_fy1[8] = {
    1.32494e-07,
    5.030693e-07,
    4.97485e-07,
    5.030693e-07,
    4.97485e-07,
    4.97485e-07,
    4.97485e-07,
    4.97485e-07};
  TGraph *graph = new TGraph(8,Reference_fx1,Reference_fy1);
  graph->SetName("Reference");
  graph->SetTitle("Reference");
  graph->SetLineColor(8);
  graph->SetMarkerColor(8);
  graph->SetMarkerStyle(22);
  graph->SetMarkerSize(0.8);
   
  TH1F *Graph_Reference1 = new TH1F("Graph_Reference1","Reference",100,0,23.1);
  Graph_Reference1->SetMinimum(9.59949e-08);
  Graph_Reference1->SetMaximum(5.339841e-07);
  Graph_Reference1->SetDirectory(0);
  Graph_Reference1->SetStats(0);

  Int_t ci;      // for color index setting
  TColor *color; // for color definition with alpha
  ci = TColor::GetColor("#000099");
  Graph_Reference1->SetLineColor(ci);
  Graph_Reference1->GetXaxis()->SetLabelFont(42);
  Graph_Reference1->GetXaxis()->SetLabelSize(0.035);
  Graph_Reference1->GetXaxis()->SetTitleSize(0.035);
  Graph_Reference1->GetXaxis()->SetTitleFont(42);
  Graph_Reference1->GetYaxis()->SetLabelFont(42);
  Graph_Reference1->GetYaxis()->SetLabelSize(0.035);
  Graph_Reference1->GetYaxis()->SetTitleSize(0.035);
  Graph_Reference1->GetYaxis()->SetTitleOffset(0);
  Graph_Reference1->GetYaxis()->SetTitleFont(42);
  Graph_Reference1->GetZaxis()->SetLabelFont(42);
  Graph_Reference1->GetZaxis()->SetLabelSize(0.035);
  Graph_Reference1->GetZaxis()->SetTitleSize(0.035);
  Graph_Reference1->GetZaxis()->SetTitleFont(42);
  Graph_Reference1->SetLineColor(8);
  Graph_Reference1->SetLineStyle(1);
  Graph_Reference1->SetLineWidth(1);
  Graph_Reference1->SetMarkerColor(8);
  Graph_Reference1->SetMarkerStyle(22);
  Graph_Reference1->SetMarkerSize(0.8);
  graph->SetHistogram(Graph_Reference1);

  multigraph->Add(graph,"");
   
  Double_t a17_Jul_fx2[8] = {
    0,
    2.970988,
    6,
    8.969377,
    12,
    15,
    18,
    21};
  Double_t a17_Jul_fy2[8] = {
    3.52662e-07,
    2.002011e-06,
    2.25743e-06,
    2.268945e-06,
    2.25743e-06,
    2.25743e-06,
    2.25743e-06,
    2.25743e-06};

  graph = new TGraph(8,a17_Jul_fx2,a17_Jul_fy2);
  graph->SetName("17 Jul");
  graph->SetTitle("17 Jul");
  graph->SetLineColor(4);
  graph->SetMarkerColor(4);
  graph->SetMarkerStyle(21);
  graph->SetMarkerSize(0.7);

  TH1F *Graph_17sPJul2 = new TH1F("Graph_17sPJul2","17 Jul",100,0,23.1);
  Graph_17sPJul2->SetMinimum(1.621852e-07);
  Graph_17sPJul2->SetMaximum(2.447907e-06);
  Graph_17sPJul2->SetDirectory(0);
  Graph_17sPJul2->SetStats(0);

  ci = TColor::GetColor("#000099");
  Graph_17sPJul2->SetLineColor(ci);
  Graph_17sPJul2->GetXaxis()->SetLabelFont(42);
  Graph_17sPJul2->GetXaxis()->SetLabelSize(0.035);
  Graph_17sPJul2->GetXaxis()->SetTitleSize(0.035);
  Graph_17sPJul2->GetXaxis()->SetTitleFont(42);
  Graph_17sPJul2->GetYaxis()->SetLabelFont(42);
  Graph_17sPJul2->GetYaxis()->SetLabelSize(0.035);
  Graph_17sPJul2->GetYaxis()->SetTitleSize(0.035);
  Graph_17sPJul2->GetYaxis()->SetTitleOffset(0);
  Graph_17sPJul2->GetYaxis()->SetTitleFont(42);
  Graph_17sPJul2->GetZaxis()->SetLabelFont(42);
  Graph_17sPJul2->GetZaxis()->SetLabelSize(0.035);
  Graph_17sPJul2->GetZaxis()->SetTitleSize(0.035);
  Graph_17sPJul2->GetZaxis()->SetTitleFont(42);
  Graph_17sPJul2->SetLineColor(4);
  Graph_17sPJul2->SetLineStyle(1);
  Graph_17sPJul2->SetLineWidth(1);
  Graph_17sPJul2->SetMarkerColor(4);
  Graph_17sPJul2->SetMarkerStyle(22);
  Graph_17sPJul2->SetMarkerSize(0.8);
  graph->SetHistogram(Graph_17sPJul2);
   
  multigraph->Add(graph,"");

  Double_t b24_Jul_fx3[8] = {
    0,
    2.92962,
    5.122135,
    6.61139,
    9.217586,
    15,
    18,
    21};
  Double_t b24_Jul_fy3[8] = {
    1.735e-07,
    2.659081e-06,
    3.808954e-06,
    4.507091e-06,
    5.554296e-06,
    7.43746e-06,
    7.43746e-06,
    7.43746e-06};

  graph = new TGraph(8,b24_Jul_fx3,b24_Jul_fy3);
  graph->SetName("24 Jul");
  graph->SetTitle("24 Jul");
  graph->SetLineColor(2);
  graph->SetMarkerColor(2);
  graph->SetMarkerStyle(20);
  graph->SetMarkerSize(0.8);
   
  TH1F *Graph_24sPJul3 = new TH1F("Graph_24sPJul3","24 Jul",100,0,23.1);
  Graph_24sPJul3->SetMinimum(1.5615e-07);
  Graph_24sPJul3->SetMaximum(8.163856e-06);
  Graph_24sPJul3->SetDirectory(0);
  Graph_24sPJul3->SetStats(0);

  ci = TColor::GetColor("#000099");
  Graph_24sPJul3->SetLineColor(ci);
  Graph_24sPJul3->GetXaxis()->SetLabelFont(42);
  Graph_24sPJul3->GetXaxis()->SetLabelSize(0.035);
  Graph_24sPJul3->GetXaxis()->SetTitleSize(0.035);
  Graph_24sPJul3->GetXaxis()->SetTitleFont(42);
  Graph_24sPJul3->GetYaxis()->SetLabelFont(42);
  Graph_24sPJul3->GetYaxis()->SetLabelSize(0.035);
  Graph_24sPJul3->GetYaxis()->SetTitleSize(0.035);
  Graph_24sPJul3->GetYaxis()->SetTitleOffset(0);
  Graph_24sPJul3->GetYaxis()->SetTitleFont(42);
  Graph_24sPJul3->GetZaxis()->SetLabelFont(42);
  Graph_24sPJul3->GetZaxis()->SetLabelSize(0.035);
  Graph_24sPJul3->GetZaxis()->SetTitleSize(0.035);
  Graph_24sPJul3->GetZaxis()->SetTitleFont(42);
  Graph_24sPJul3->SetLineColor(2);
  Graph_24sPJul3->SetLineStyle(1);
  Graph_24sPJul3->SetLineWidth(1);
  Graph_24sPJul3->SetMarkerColor(2);
  Graph_24sPJul3->SetMarkerStyle(22);
  Graph_24sPJul3->SetMarkerSize(0.8);
  graph->SetHistogram(Graph_24sPJul3);
     
  multigraph->Add(graph,"");
  multigraph->Draw("APL");
  multigraph->GetXaxis()->SetTitle("Bias Voltage [V]");
  multigraph->GetXaxis()->SetLabelFont(42);
  multigraph->GetXaxis()->SetLabelSize(0.03);
  multigraph->GetXaxis()->SetTitleSize(0.03);
  multigraph->GetXaxis()->SetTitleFont(42);
  multigraph->GetYaxis()->SetTitle("Leakage current (normalized at 273 K) [A]");
  multigraph->GetYaxis()->SetLabelFont(42);
  multigraph->GetYaxis()->SetLabelSize(0.03);
  multigraph->GetYaxis()->SetTitleSize(0.03);
  multigraph->GetYaxis()->SetTitleOffset(1.2);
  multigraph->GetYaxis()->SetTitleFont(42);

  TLegend *leg2 = new TLegend(0.67,0.58,0.85,0.77);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.038);
  leg2->AddEntry(Graph_Reference1,"Reference ","pl");
  leg2->AddEntry(Graph_17sPJul2,"17 July 2017 ","pl");
  leg2->AddEntry(Graph_24sPJul3,"24 July 2017 ","pl");
  leg2->Draw("SAME");    

   
  app.Run(true);
  return 0;
}


// }
