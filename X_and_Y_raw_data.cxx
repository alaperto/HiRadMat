#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  // TString which_list = "_O"; //"" for all, "_D" for Digital, "_A" for Analog, "_T" for threshold off, "_O" for threshold on, "_S" for source 
  // TString module = "1"; //"1": M1, "2": M2, "3" M1 special, "4" M2 special
  // which_list+=module;
  TString Day = "17";
  TString XY = "X";
  TString list_name = "rawData/Raw_"+Day+"_"+XY+".txt";
  TString column = "rawData/Raw_"+XY+".txt";
  // TString plot_projections = "Yes"; //"Yes" plot X and Y projections in separate canvas, "whatever" to do not plot them
  // TString separate_plots = "Yeso"; //"Yes" separate canvas, "whatever" superimpose on the same canvas
  // TString save_GIF="Yesa"; //"Yes" save animated GIF, "whatever" not save
  // int k=3; //skip to plot: O3->3, T3->5
  // TString do_differential="Yeso"; //"Yes" subtract the previous map, "whatever" do not subtract

  int qq =20;
  TF1 *Gauss[qq];
  // TCanvas *Canvas_2D[qq];
  // TCanvas *Canvas_Xpro[qq];
  TCanvas *Canvas_Ypro[qq];
  TH1D *Xpro[qq], *Ypro[qq];
  TH2F *Occup_0_0_MA[qq];
  //int aa[40000]; double bb[40000], cc[40000], dd[40000], ff[400000]; int a=0,b=0,c=0,d=0,e=0,f=1;
  TGraph *Projection[qq];
  Int_t tot = 280; double start = -11, stop = 8;
  if(XY=="Y") { tot = 265;  start = -9.5, stop = 7.5;}
  Double_t aa[300];
  Double_t bb[20][300];
  double ciaone;
  double cc[20], dd[20];
  double mean[20], sigma[20];
  TString macro_name = ""; int m = 0;//, n = 0;
  // TString plotName="";
  // if(separate_plots=="Yes") save_GIF="No";
  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  // gStyle->SetOptTitle(0);
  
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________WELCOME____TO___THE____FITTING_______________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;


  ifstream input2(list_name); 
  while(!input2.eof()){
    if(m==0)  input2 >> cc[1] >> cc[2] >> cc[3] >> cc[4] >> cc[5] >> cc[6] >> cc[7] >> cc[8] >> cc[9] >> cc[10] >> cc[11] >> cc[12] >> cc[13] >> cc[14] >> cc[15] >> cc[16];
    else input2 >> bb[1][m] >> bb[2][m] >> bb[3][m] >> bb[4][m] >> bb[5][m] >> bb[6][m] >> bb[7][m] >> bb[8][m] >> bb[9][m] >> bb[10][m] >> bb[11][m] >> bb[12][m] >> bb[13][m] >> bb[14][m] >> bb[15][m] >> bb[16][m];
    m++;
  }


  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________LOADED____SHIT_______________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;

  m=0;
  ifstream input3(column); 
  while(!input3.eof()){
    if(m==0) { input3 >> ciaone;
      aa[m]=-12;}
    else input3 >> aa[m];
    m++;
  }

  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________LOADED___COLUMN_______________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;


  for(int z = 1; z < 17; z++)
    {
      macro_name = "FIT"; 
      macro_name += "_";
      macro_name += z;
      
      Gauss[z] = new TF1(macro_name,"[0]+[1]*gaus(1)",start,stop);
      Gauss[z]->SetParName(0,"const");
      Gauss[z]->SetParName(1,"height");
      Gauss[z]->SetParName(2,"mean");
      Gauss[z]->SetParName(3,"sigma");
      Gauss[z]->SetParameter(0, 6000);
      if(z<12) Gauss[z]->SetParameter(1, 80);
      else Gauss[z]->SetParameter(1, 120);
      if(z<12) Gauss[z]->SetParameter(2, -3);
      else Gauss[z]->SetParameter(2, -2);
      if(XY=="Y"){
	if(z==12) Gauss[z]->SetParameter(2,-4);
	if(z==13) Gauss[z]->SetParameter(2,-3);
	if(z==14) Gauss[z]->SetParameter(2,-2);
	if(z==15) Gauss[z]->SetParameter(2,3);
	if(z==16) Gauss[z]->SetParameter(2,3);
      } 
      if(z<12) Gauss[z]->SetParameter(3, 2);
      else  Gauss[z]->SetParameter(3, 0.2);
      // Canvas_Xpro[m] = new TCanvas("Canvas_Xproj_"+macro_name,"Canvas_Xproj_"+macro_name,700,850);
      // Gauss[z]->Draw();
    }

  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________LOADED___FIT___FUNCTION_______________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;


  for(int z = 1; z < 17; z++)
    {
      macro_name = "FIT"; 
      macro_name += "_";
      macro_name += z;
      Projection[z] = new TGraph(tot,aa,bb[z]);
      Projection[z]->SetTitle(macro_name);
      macro_name = "Canvas_FIT"; 
      macro_name += "_";
      macro_name += z;
      Canvas_Ypro[z] = new TCanvas("Canvas_Yproj_"+macro_name,"Canvas_Yproj_"+macro_name,700,850);
      Projection[z]->Draw();
      macro_name = "FIT"; 
      macro_name += "_";
      macro_name += z;
      Projection[z]->Fit(macro_name,"R");
      mean[z]=Gauss[z]->GetParameter(2);
      sigma[z]=Gauss[z]->GetParameter(3);
    }


  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________DRAWN_______________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;


  ofstream myFile;
  TString theNAME = "rawData/Mean_Sigma_"+Day+"_"+XY+"_new.txt";
  myFile.open (theNAME); //Attention, not superimpose
  myFile <<555<<"        "<<555<<"\n";//data not available: shot[1]
  myFile <<555<<"        "<<555<<"\n";//data not available: shot[2]
  for(int z = 1; z < 17; z++)
    {
      myFile <<mean[z]<<"        "<<abs(sigma[z])<<"\n";
      if(z==13)   myFile <<555<<"        "<<555<<"\n";//data not available: shot[15]-2 at 20:49
    }


  for(int z = 1; z < 14; z++)
    myFile <<555<<"        "<<555<<"\n";//data not available: shot[20-32] on 18-19-20 July

  myFile.close();



  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________SAVED_____ON_____FILE_______________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;


  app.Run(true);
  return 0;
}
