#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TRandom3.h> 
#include <TF1.h> 
#include <TF2.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){
  //type: "D" for Digital, "A" for Analog, "T" for threshold off, "O" for threshold on, "S" for source 
  //Please insert: "type_module_date_time.C"
  TString macro_name = "T_1_17_1442.C"; //2
  // TString macro_name = "T_1_17_1829.C"; //7

  int m = 0;
  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  gStyle->SetOptTitle(0);
  int qq =200;

  TCanvas *plot_mc_data[qq];
  TH2F *Occup_0_0_MA;
  int aa[40000]; double bb[40000], cc[40000], dd[40000], ff[400000]; int a=0,b=0,c=0,d=0,e=0,f=1;

    if(macro_name!=""){
    plot_mc_data[m] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);

    Occup_0_0_MA = new TH2F("Plot_"+macro_name,"Plot_"+macro_name,80,-0.5,79.5,336,-0.5,335.5);

    a=0,b=0,c=0,d=0,e=0,f=1;

    TString list_name = "tables/"+macro_name+".txt";
    ifstream input3(list_name); 
    while(!input3.eof()){
      input3 >> ff[f];
      if(d==0)
      if(ff[2]!=0){
	if(f%3==2) {aa[f/3+1]=ff[f]; e=f/3+1;
	  if(ff[f]==0) d=1;}
	if(f%3==0){bb[f/3]=ff[f];}
      }
      else{
	if(f%4==3) {aa[f/4]=ff[f]; e=f/4;
	  if(ff[f]==0) d=1;}
	if(f%4==0){bb[f/4]=ff[f];}
      }
      f++;
    }
    
    //for debug purpose
    //for(int x = 1; x < 26; x++)
    // for(int x = 1; x < e; x++)
    //   cout<<x<<"   "<<aa[x]<<"   "<<bb[x]<<endl;
    
    for(int x = 1; x < e; x++)
      {
	// if(bb[x]==1.053496) bb[x]=0;
	Occup_0_0_MA->SetBinContent(aa[x],bb[x]);
      }
    
    Occup_0_0_MA->Draw();
    Occup_0_0_MA->SetMinimum(0);
    Occup_0_0_MA->SetMaximum(200);
    Occup_0_0_MA->GetXaxis()->SetTitle("Column");
    Occup_0_0_MA->GetXaxis()->SetNdivisions(-401);
    Occup_0_0_MA->GetXaxis()->SetLabelFont(42);
    Occup_0_0_MA->GetXaxis()->SetTitleSize(0.03);
    Occup_0_0_MA->GetXaxis()->SetTitleOffset(0.8);
    Occup_0_0_MA->GetXaxis()->SetTitleFont(42);
    Occup_0_0_MA->GetYaxis()->SetTitle("Row");
    Occup_0_0_MA->GetYaxis()->SetBinLabel(336,"0");
    Occup_0_0_MA->GetYaxis()->SetBinLabel(236,"100");
    Occup_0_0_MA->GetYaxis()->SetBinLabel(136,"200");
    Occup_0_0_MA->GetYaxis()->SetBinLabel(36,"300");
    Occup_0_0_MA->GetYaxis()->SetNdivisions(-401);
    Occup_0_0_MA->GetYaxis()->SetLabelFont(42);
    Occup_0_0_MA->GetYaxis()->SetLabelSize(0.045);
    Occup_0_0_MA->GetYaxis()->SetTitleSize(0.03);
    Occup_0_0_MA->GetYaxis()->SetTickLength(0.015);
    Occup_0_0_MA->GetYaxis()->SetTitleOffset(1.1);
    Occup_0_0_MA->GetYaxis()->SetTitleFont(42);
    Occup_0_0_MA->GetZaxis()->SetLabelFont(42);
    Occup_0_0_MA->GetZaxis()->SetLabelSize(0.035);
    Occup_0_0_MA->GetZaxis()->SetTitleSize(0.035);
    Occup_0_0_MA->GetZaxis()->SetTitleFont(42);
    Occup_0_0_MA->Draw("COLZ");
       
    }

  int T1[11]={0,0,1+1+1,4,4,12,24,36+72,144,288,288*2};
  int T1_sum[11]={0,0,1+1+1,3+4,7+4,7+4+12,7+4+12+24,7+4+12+24+36+72,7+4+12+24+36+72+144,7+4+12+24+36+72+144+288,7+4+12+24+36+72+144+288+288*2};
  // T_1_17_1005.C //0
  // T_1_17_1030.C //0
  // T_1_17_1442.C //1+1+1
  // T_1_17_1503.C //4
  // T_1_17_1641.C //4
  // T_1_17_1706.C //12
  // T_1_17_1737.C //24
  // T_1_17_1829.C //36+72
  // T_1_17_1849.C //144
  // T_1_17_1914.C //288
  // T_1_17_1958.C //288 10e10
  // 0.2 mm Shots
  // T_1_17_2112.C //+1+1+1 (8.5e10)

  int T2[10]={0,0,1+1,1,4,4,12,24,36+72+144,288};
  int T2_sum[10]={0,0,1+1,2+1,3+4,7+4,7+4+12,7+4+12+24,7+4+12+24+36+72+144,7+4+12+24+36+72+144+288};
  // 2 mm Shots
  // T_2_17_1005.C //0
  // T_2_17_1030.C //0
  // T_2_17_1403.C //1+1
  // T_2_17_1442.C //1
  // T_2_17_1503.C //4
  // T_2_17_1537.C //4
  // T_2_17_1706.C //12
  // T_2_17_1737.C //24
  //36+72 missing
  // T_2_17_1849.C //(36+72+) 144
  // T_2_17_1914.C //288
  //288 10e10 nothing

  // b[0] = new TDatime(2017,07,17,13,37,00); //13:37 1 b; 5 10e10
  // b[1] = new TDatime(2017,07,17,14,0,00); //14:00 1 b; 5 10e10
  // b[2] = new TDatime(2017,07,17,14,31,00); //14:31 1 b; 5 10e10
  // b[3] = new TDatime(2017,07,17,15,3,00); //15:03 4 b sep 1.7 us; x 5 E10
  // b[4] = new TDatime(2017,07,17,15,37,00); //15:37 4 b sep 1.7 us; x 5 E10
  // b[5] = new TDatime(2017,07,17,17,6,00); //17:06 12 b sep 25 ns; x 5 E10
  // b[6] = new TDatime(2017,07,17,17,37,00); //17:37 24 b sep 25 ns; x 5 E10
  // b[7] = new TDatime(2017,07,17,17,55,00); //17:55 36 b sep 25 ns; x 5 E10
  // b[8] = new TDatime(2017,07,17,18,9,00); //18:09 72 b sep 25 ns; x 5 E10
  // b[9] = new TDatime(2017,07,17,18,48,00); //18:48 144 b sep 25 ns;  5 E10
  // b[10] = new TDatime(2017,07,17,19,14,00);//19:14 288 b sep 25 ns;  5 E10
  // b[11] = new TDatime(2017,07,17,19,56,00);//19:56 288 b sep 25 ns; 10 E10
  // b[12] = new TDatime(2017,07,17,20,40,00);//20:40 1 b; 5 E10
  // b[13] = new TDatime(2017,07,17,20,40,00);//20:40 1 b; 5 E10
  // b[14] = new TDatime(2017,07,17,21,12,00);//21:12 1 b; 8.5 E10
  // b[15] = new TDatime(2017,07,17,22,1,00);//22:01 12 b; 10 E10

  TF2 *f2 = new TF2("f2","[0]*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4])",-0.5,79.5,-0.5,335.5);
  int n_bunches = T1_sum[7];
  f2->SetParameters(n_bunches*5e10,40,13,168,55);
  TH2D *h2=new TH2D("h2","TRandom2",80,-0.5,79.5,336,-0.5,335.5);
  h2->FillRandom("f2",10000000); 
  TCanvas *plot_dose[qq];
  plot_dose[7] = new TCanvas("Canvas_"+macro_name+"_dose","Canvas_"+macro_name+"_dose",700,550);
  h2->Draw("COLZ");


  TCanvas *c1 = new TCanvas("c1","c1",200,10,700,500);
  c1->cd();
  Double_t dam[26880], dos[26880]; int n = 26880;
  for(int p = 0; p < 26880; p++){
    dos[p]=h2->GetBinContent(p);
    dam[p]=Occup_0_0_MA->GetBinContent(p);
    if(dam[p]<=0) {dam[p]=0; dos[p]=0;}
  }

  TGraph *graph = new TGraph(n,dos,dam);//0,10e10,0,10e10 //"Hello","Hello"
  graph->SetTitle("Damage vs Dose");
  graph->GetXaxis()->SetTitle("Flux [n protons]");
  graph->GetYaxis()->SetTitle("Damage [noise]");
  graph->GetYaxis()->SetTitleOffset(1.2);
  graph->GetXaxis()->SetTitleOffset(1.2);
  graph->GetYaxis()->SetRangeUser(0,1500);
  graph->SetMarkerStyle(20);
  graph->SetMarkerSize(1);
  graph->Draw("sameAP");
      
  app.Run(true);
  return 0;
}
