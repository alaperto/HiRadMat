#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  TString PRINT = "Yes";
  TString fol = "./RD53_NoiseDist/";

  //TString M_name = "F02_91_02_25_1E";
  //TString M_name = "F02_02_50_1E_01";
  //TString M_name = "F02_02_50_1E_03"; //XXV
  //TString M_name = "F03_30_25x100-1E_D5-3"; //??
  //TString M_name = "F03_30_50x50-1E_E3.3";//E3-3_50_1E"; //10V
  //TString M_name = "F03_30_25x100-2E_A1.2";//A1-2_25_2E"; //10V
  TString M_name = "F03_30_25x100-1E_D11.3"; //8 V

  //TString volts = "10";
  TString volts = "8";
  //TString volts = "3";

  std::vector<int> data;
  std::ifstream file(fol+volts+"V_"+M_name+"_diff.dat");
  std::copy(std::istream_iterator<int>(file), std::istream_iterator<int>(), std::back_inserter(data));

  // int much = 30;

  // for(int a = 5; a < (5+much); a++){
  //   cout<<data.at(a)<<" ";
  // }
  // cout<<"ue"<<endl;

  std::vector<int> data0;
  std::ifstream file0(fol+"0V_"+M_name+"_diff.dat");
  std::copy(std::istream_iterator<int>(file0), std::istream_iterator<int>(), std::back_inserter(data0));

  // for(int a = 5; a < (5+much); a++){
  //   cout<<data0.at(a)<<" ";
  // }
  // cout<<""<<endl;

  TCanvas *c3 = new TCanvas("c3","c3",20,10,800,500);
  c3->cd();  
  TH1D *IV = new TH1D(volts+"V", volts+"V", data.at(0), data.at(1), data.at(2));
  TH1D *IVB = new TH1D("0V", "0V", data0.at(0), data0.at(1), data0.at(2));

  //cout<<"ue "<<data.at(0)<<endl;
  
  for(int a = 1; a < data.at(0); a++)  IV->SetBinContent(a,data.at(a+5));
  for(int a = 1; a < data0.at(0); a++)  IVB->SetBinContent(a,data0.at(a+5));

  IV->SetTitle(M_name);
  IV->GetYaxis()->SetTitle("Number of pixels");
  IV->GetXaxis()->SetTitle("Noise [e]");
  // IV->SetMarkerStyle(20);
  IV->SetLineWidth(2);
  IV->Draw("HIST");
  // IVB->SetMarkerStyle(20);
  IVB->SetLineWidth(2);
  IVB->SetLineColor(2);
  IVB->Draw("samesHIST");
  // if (data0.at(2)>data.at(2)){
  //   IVB->Draw("HIST");
  //   IV->Draw("samesHIST");
  // }

  c3->Update();

  TPaveStats *st = (TPaveStats*)IVB->FindObject("stats");
  st->SetName("hame");
  st->SetX1NDC(0.55); //new x start position
  st->SetX2NDC(0.75); //new x end position

  TLegend *leg2 = new TLegend(0.15,0.87,0.25,0.73);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.033);
  leg2->AddEntry(IVB,"0 V bias");
  leg2->AddEntry(IV,volts+" V bias");
  leg2->Draw("SAME");   

  if(PRINT=="Yes") {
    TString plotName="plots_to_eos/"+M_name+"_NoiseDist.png";
    c3->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }

  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           

  app.Run(true);
  return 0;
}
