#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  TString PRINT = "Yes";
  TString fol = "./DataTextFile/";
  TString list_name =fol+"AV_8.txt";

  // int ccc = 400;
  // TString a[ccc], c[ccc], d[ccc], f[ccc], g[ccc];
  // Float_t b[ccc], e[ccc];
  // int cc=0;

  // ifstream input3(list_name); 
  // while(!input3.eof()){
  //   cc++;
  //   input3 >> a[cc] >> b[cc] >> c[cc] >> d[cc] >> e[cc] >> f[cc];// >> g[cc];
  //   //cout<<cc<<" a  "<<a[cc]<<" b  "<<b[cc]<<" c  "<<c[cc]<<" d  "<<d[cc]<<" e  "<<e[cc]<<" f  "<<f[cc]<<endl;
  //   //cout<<cc<<"  b  "<<b[cc]<<"  e  "<<e[cc]<<endl;
  // }

  int ccc = 100;
  TString u[ccc];
  Float_t v[ccc], a[ccc];
  int cc=0;

  for(int 
  ifstream input3("./IVScans/IVScan_2018-05-08_16:31:57.dat"); 
  while(!input3.eof()){
    cc++;
    input3 >>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>v[cc]>>u[cc]>>u[cc]>>u[cc]>>a[cc]>>u[cc];
    cout<<cc<<"  v  "<<v[cc]<<"  a  "<<a[cc]<<endl;
  }

  TCanvas *c3 = new TCanvas("c3","c3",20,10,800,500);
  c3->cd();  
  // TGraph *IV = new TGraph(cc,&e[0],&b[0]);
  TGraph *IV = new TGraph(cc,&v[0],&a[0]);
  IV->SetTitle("IV plot");
  IV->GetYaxis()->SetTitle("Current [A]");
  IV->GetXaxis()->SetTitle("Volt [V]");
  IV->SetMarkerStyle(20);
  IV->SetMarkerSize(1);
  IV->Draw("sameAP");



  // //UNIX Time (+2h) http://www.onlineconversion.com/unix_time.htm
  // int Luglio_16_00 = 1500163200;
  // int Luglio_17_00 = 1500249600;
  // int Luglio_17_12 = 1500292800;
  // int Luglio_17_13 = 1500296400;
  // int Luglio_17_22 = 1500328800;
  // int Luglio_25_00 = 1500940800;
  // int Luglio_25_07 = 1500966000;
  // int Luglio_25_17 = 1501002000;
  // int Luglio_26_00 = 1501027200;
  // // int begin = Luglio_17_13 -3600*2 +60; //Time range
  // // int end = Luglio_17_23 -3600*2;
  // int begin = Luglio_25_07 -3600*2 +60; //Time range
  // int end = Luglio_25_17 -3600*2;
  // Double_t start = -1, stop = 3; //Voltage Y range
  // Double_t first = 0.2, last = 1.2; //Current Y range
  // Double_t fist = -0.1, lost = 0.9; //Last day Y range
  // int divisions = -410;

  // //Beam lines
  // TDatime *b[100]; Double_t *c[100];
  // TLine *l[100]; TLine *l2[100]; TLine *l3[100];

  // b[0] = new TDatime(2017,07,17,13,37,00); //13:37 1 b; 5 10e10
  // b[1] = new TDatime(2017,07,17,14,0,00); //14:00 1 b; 5 10e10
  // b[2] = new TDatime(2017,07,17,14,31,00); //14:31 1 b; 5 10e10
  // b[3] = new TDatime(2017,07,17,15,3,00); //15:03 4 bunches sep 1.7 us; x 5 E10
  // b[4] = new TDatime(2017,07,17,15,37,00); //15:37 4 bunches sep 1.7 us; x 5 E10
  // b[5] = new TDatime(2017,07,17,17,6,00); //17:06 12 bunches sep 25 ns; x 5 E10
  // b[6] = new TDatime(2017,07,17,17,37,00); //17:37 24 bunches sep 25 ns; x 5 E10
  // b[7] = new TDatime(2017,07,17,17,55,00); //17:55 36 bunches sep 25 ns; x 5 E10
  // b[8] = new TDatime(2017,07,17,18,9,00); //18:09 72 bunches sep 25 ns; x 5 E10
  // b[9] = new TDatime(2017,07,17,18,48,00); //18:48 144 bunches sep 25 ns;  5 E10
  // b[10] = new TDatime(2017,07,17,19,14,00);//19:14 288 bunches sep 25 ns;  5 E10
  // b[11] = new TDatime(2017,07,17,19,56,00);//19:56 288 bunches sep 25 ns; 10 E10
  // b[12] = new TDatime(2017,07,17,20,40,00);//20:40 1 b; 5 E10
  // b[13] = new TDatime(2017,07,17,20,40,00);//20:40 1 b; 5 E10
  // b[14] = new TDatime(2017,07,17,21,12,00);//21:12 1 b; 8.5 E10
  // b[15] = new TDatime(2017,07,17,22,1,00);//22:01 12 b; 10 E10

  // b[16] = new TDatime(2017,07,25,10,20,00);//: 1 b;
  // b[17] = new TDatime(2017,07,25,10,28,00);//: 20 b;
  // b[18] = new TDatime(2017,07,25,10,55,00);//: 10 b;
  // b[19] = new TDatime(2017,07,25,11,10,00);//: 20 b;
  // b[20] = new TDatime(2017,07,25,11,32,00);//: 20 b;
  // b[21] = new TDatime(2017,07,25,12,43,00);//: 288 b;
  // b[22] = new TDatime(2017,07,25,12,59,00);//: 72 b;
  // b[23] = new TDatime(2017,07,25,13,37,00);//: 1 b;
  // b[24] = new TDatime(2017,07,25,13,38,00);//: 1 b;
  // b[25] = new TDatime(2017,07,25,13,46,00);//: 72 b;
  // b[26] = new TDatime(2017,07,25,14,46,00);//: 288 b;
  // b[27] = new TDatime(2017,07,25,15,18,00);//: Switch off

  // //TLine l(1500292800,-10,1500292800,10);


  // // TCanvas *c1 = new TCanvas("c1","c1",200,10,1400,500);
  // // c1->cd();

  // // TGraph *graph = new TGraph(fol+"TVoltage_7.txt");
  // // graph->GetXaxis()->SetTimeDisplay(1);
  // // graph->GetXaxis()->SetTimeFormat("%H:%M");
  // // graph->SetTitle("Low Voltage in time (M1)");
  // // graph->GetYaxis()->SetTitle("LV [V]");
  // // graph->GetXaxis()->SetTitle("Time");
  // // graph->GetYaxis()->SetRangeUser(start,stop);
  // // graph->GetXaxis()->SetRangeUser(begin,end);
  // // graph->GetXaxis()->SetNdivisions(divisions);
  // // graph->SetMarkerStyle(20);
  // // graph->SetMarkerSize(1);
  // // graph->Draw("sameAP");

  // for(int a =0; a < 28; a++){
  //   c[a] = new Double_t(b[a]->Convert());
  //   l[a]= new TLine(*c[a],start,*c[a],stop);
  //   l[a]->SetLineWidth(2.5);
  //   l[a]->SetLineColor(4);
  //   if(a>11)  l[a]->SetLineColor(8);
  //   l[a]->Draw("same");
  //   l2[a]= new TLine(*c[a],first,*c[a],last);
  //   l2[a]->SetLineWidth(2.5);
  //   l2[a]->SetLineColor(4);
  //   if(a>11)  l2[a]->SetLineColor(8);
  //   l3[a]= new TLine(*c[a],fist,*c[a],lost);
  //   l3[a]->SetLineWidth(2.5);
  //   l3[a]->SetLineColor(4);
  //   if(a>21)  l3[a]->SetLineColor(8);
  //   if(a>26)  l3[a]->SetLineColor(6);
  // }

  // // TCanvas *c2 = new TCanvas("c2","c2",200,10,1400,500);
  // // c2->cd();

  // // TGraph *graph2 = new TGraph(fol+"TVoltage_6.txt");
  // // graph2->GetXaxis()->SetTimeDisplay(1);
  // // graph2->GetXaxis()->SetTimeFormat("%H:%M");
  // // graph2->SetTitle("Low Voltage in time (M2)");
  // // graph2->GetYaxis()->SetTitle("LV [V]");
  // // graph2->GetXaxis()->SetTitle("Time");
  // // graph2->GetXaxis()->SetNdivisions(divisions);
  // // graph2->GetYaxis()->SetRangeUser(start,stop);
  // // graph2->GetXaxis()->SetRangeUser(begin,end);
  // // graph2->SetMarkerColor(2);
  // // graph2->SetMarkerStyle(20);
  // // graph2->SetMarkerSize(1);
  // // graph2->Draw("sameAP");

  // // for(int a =0; a < 16; a++){
  // //   l[a]->Draw("same");
  // // }

  // TCanvas *c3 = new TCanvas("c3","c3",200,10,1400,500);
  // c3->cd();

  // TGraph *graph3 = new TGraph(fol+"TCurrent_7.txt");
  // graph3->GetXaxis()->SetTimeDisplay(1);
  // graph3->GetXaxis()->SetTimeFormat("%H:%M");
  // graph3->SetTitle("Current in time (M1)");
  // graph3->GetYaxis()->SetTitle("Current [A]");
  // graph3->GetXaxis()->SetTitle("Time");
  // graph3->GetXaxis()->SetNdivisions(divisions);
  // if(end != Luglio_25_17 -3600*2)
  //   graph3->GetYaxis()->SetRangeUser(first,last);
  // else
  //   graph3->GetYaxis()->SetRangeUser(fist,lost);
  // graph3->GetXaxis()->SetRangeUser(begin,end);
  // graph3->SetMarkerStyle(20);
  // graph3->SetMarkerSize(1);
  // graph3->Draw("sameAP");

  // for(int a =0; a < 28; a++){
  //   if(a<16) l2[a]->Draw("same");
  //   if(a>=16) l3[a]->Draw("same");
  // }


  // TCanvas *c4 = new TCanvas("c4","c4",200,10,1400,500);
  // c4->cd();

  // TGraph *graph24 = new TGraph(fol+"TCurrent_6.txt");
  // graph24->GetXaxis()->SetTimeDisplay(1);
  // graph24->GetXaxis()->SetTimeFormat("%H:%M");
  // graph24->SetTitle("Current in time (M2)");
  // graph24->GetYaxis()->SetTitle("Current [A]");
  // graph24->GetXaxis()->SetTitle("Time");
  // graph24->GetXaxis()->SetNdivisions(divisions);
  // if(end != Luglio_25_17 -3600*2)
  //   graph24->GetYaxis()->SetRangeUser(first,last);
  // else
  //   graph24->GetYaxis()->SetRangeUser(fist,lost);
  // graph24->GetXaxis()->SetRangeUser(begin,end);
  // graph24->SetMarkerColor(2);
  // graph24->SetMarkerStyle(20);
  // graph24->SetMarkerSize(1);
  // graph24->Draw("sameAP");

  // for(int a =0; a < 28; a++){
  //   if(a<16) l2[a]->Draw("same");
  //   if(a>=16) l3[a]->Draw("same");
  // }

  // if(PRINT=="Yes") {
  //   TString plotName="saved_plots/Voltage_M1_17Luglio.png";
  //   // c1->Print(plotName.Data());
  //   // plotName="saved_plots/Voltage_M2_17Luglio.png";
  //   // c2->Print(plotName.Data());
  //   plotName="saved_plots/Current_M1_25Luglio.png";
  //   c3->Print(plotName.Data());
  //   plotName="saved_plots/Current_M2_25Luglio.png";
  //   c4->Print(plotName.Data());
  //   // plotName="saved_plots/Current_M1_17Luglio.png";
  //   // c3->Print(plotName.Data());
  //   // plotName="saved_plots/Current_M2_17Luglio.png";
  //   // c4->Print(plotName.Data());
  //   cout<<endl<<"Saved files in: "<<plotName<<endl;
  // }
  
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           

  app.Run(true);
  return 0;
}
