#include <TStyle.h>
#include <TCanvas.h>
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  TString which_list = "_S"; //"" for all, "_D" for Digital, "_A" for Analog, "_T"/"_TS" for threshold off, "_O"/"_OS" for threshold on, "_MS" for threshold -10V, "_S" for source 
  TString module = "2"; //"1": M1, "2": M2, "3" 2018 Planar (or M1 special), "4" M2 special
  which_list+=module;
  TString list_name= "lists/list_2D_macros"+which_list;
  // list_name="lists/list_Sources";
  TString separate_plots = "Yes"; //"Yes" separate canvas, "whatever" superimpose on the same canvas
  TString save_GIF="Yes"; //"Yes" save animated GIF, "whatever" not save

  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  gStyle->SetPalette(53); //Radioactivation plots
  // gStyle->SetOptTitle(0);
  int qq =200;
  TCanvas *plot_mc_data[qq];
  TH2F *Occup_0_0_MA[qq];
  int aa[40000]; double bb[40000], cc[40000], dd[40000], ff[400000]; int a=0,b=0,c=0,d=0,e=0,f=1;
  TString plotName="";
  if(separate_plots=="Yes") save_GIF="No";
  TString macro_name = ""; int m = 0;

  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________WELCOME____TO___THE____PLOTTING_______________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;

  ifstream input2(list_name); 
  while(!input2.eof()){
    input2 >> macro_name;
    m++;
    if(macro_name!=""){

      if(separate_plots=="Yes")
	plot_mc_data[m] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);
      else
	if(m==1) plot_mc_data[1] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);

    Occup_0_0_MA[m] = new TH2F("Plot_"+macro_name,"Plot_"+macro_name,80,-0.5,79.5,336,-0.5,335.5);

    a=0,b=0,c=0,d=0,e=0,f=1;

    TString list_name = "tables/"+macro_name+".txt";
    ifstream input3(list_name); 
    while(!input3.eof()){
      input3 >> ff[f];
      if(d==0)
      if(ff[2]!=0){
	if(f%3==2) {aa[f/3+1]=ff[f]; e=f/3+1;
	  if(ff[f]==0) d=1;}
	if(f%3==0){bb[f/3]=ff[f];}
      }
      else{
	if(f%4==3) {aa[f/4]=ff[f]; e=f/4;
	  if(ff[f]==0) d=1;}
	if(f%4==0){bb[f/4]=ff[f];}
      }
      f++;
    }
  
    
    //for debug purpose
    //for(int x = 1; x < 26; x++)
     // for(int x = 1; x < e; x++)
     //   cout<<x<<"   "<<aa[x]<<"   "<<bb[x]<<<<endl;
    
    for(int x = 1; x < e; x++)
      {
	// if(bb[x]==1.053496) bb[x]=0;
	Occup_0_0_MA[m]->SetBinContent(aa[x],bb[x]);
      }

    Occup_0_0_MA[1]->SetTitle(macro_name);//Change Title for every file!
    Occup_0_0_MA[m]->GetXaxis()->SetTitle("Column");
    // Occup_0_0_MA[m]->GetXaxis()->SetNdivisions(-401);
    // Occup_0_0_MA[m]->GetXaxis()->SetLabelFont(42);
    // Occup_0_0_MA[m]->GetXaxis()->SetTitleSize(0.03);
    // Occup_0_0_MA[m]->GetXaxis()->SetTitleOffset(0.8);
    // Occup_0_0_MA[m]->GetXaxis()->SetTitleFont(42);
    Occup_0_0_MA[m]->GetYaxis()->SetTitle("Row");
    // Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(336,"0");
    // Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(236,"100");
    // Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(136,"200");
    // Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(36,"300");
    // Occup_0_0_MA[m]->GetYaxis()->SetNdivisions(-401);
    // Occup_0_0_MA[m]->GetYaxis()->SetLabelFont(42);
    // Occup_0_0_MA[m]->GetYaxis()->SetLabelSize(0.045);
    // Occup_0_0_MA[m]->GetYaxis()->SetTitleSize(0.03);
    // Occup_0_0_MA[m]->GetYaxis()->SetTickLength(0.015);
    // Occup_0_0_MA[m]->GetYaxis()->SetTitleOffset(1.1);
    // Occup_0_0_MA[m]->GetYaxis()->SetTitleFont(42);
    // Occup_0_0_MA[m]->GetZaxis()->SetLabelFont(42);
    // Occup_0_0_MA[m]->GetZaxis()->SetLabelSize(0.035);
    // Occup_0_0_MA[m]->GetZaxis()->SetTitleSize(0.035);
    // Occup_0_0_MA[m]->GetZaxis()->SetTitleFont(42);       
    Occup_0_0_MA[m]->SetMinimum(0);
    Occup_0_0_MA[m]->SetMaximum(1000);
    // cout<<"1-79 Integral for "<<macro_name<<"  "<<Occup_0_0_MA[m]->Integral(1,79,1,335)<<endl;
    // cout<<"1-58 Integral for "<<macro_name<<"  "<<Occup_0_0_MA[m]->Integral(1,58,1,335)<<endl<<endl;
    if(which_list=="_T1") Occup_0_0_MA[m]->SetMaximum(500);
    if(which_list=="_T2") Occup_0_0_MA[m]->SetMaximum(500);
    if(which_list=="_S1") Occup_0_0_MA[m]->SetMaximum(100);
    if(which_list=="_S2") Occup_0_0_MA[m]->SetMaximum(100);
    if(which_list=="_D") Occup_0_0_MA[m]->SetMaximum(220);
    if(which_list=="_A") Occup_0_0_MA[m]->SetMaximum(220);
    if(which_list=="_A3") Occup_0_0_MA[m]->SetMaximum(250);
    if(which_list=="_D3") Occup_0_0_MA[m]->SetMaximum(250);
    if(which_list=="_S3") Occup_0_0_MA[m]->SetMaximum(100);
    if(which_list=="_OS3") Occup_0_0_MA[m]->SetMaximum(400);
    if(which_list=="_TS3") Occup_0_0_MA[m]->SetMaximum(500);
    if(which_list=="_MS3") Occup_0_0_MA[m]->SetMaximum(500);
    if(which_list=="_OM3") Occup_0_0_MA[m]->SetMaximum(5000);
    if(which_list=="_TM3") Occup_0_0_MA[m]->SetMaximum(6000);
    if(which_list=="_MM3") Occup_0_0_MA[m]->SetMaximum(6000);

    // Occup_0_0_MA[m]->SetMinimum(0);
    // Occup_0_0_MA[m]->SetMaximum(1000);

    Occup_0_0_MA[m]->Clear();
    Occup_0_0_MA[m]->Draw("COLZ");

      if(separate_plots=="Yes") Occup_0_0_MA[m]->Draw("COLZ");
      else  {
	if(m==1) Occup_0_0_MA[m]->Draw("COLZ");
	else Occup_0_0_MA[m]->Draw("COLZSAME");
	
	if(save_GIF=="Yes"){
	  plotName="GIF/list"+which_list+".gif";
	  plot_mc_data[1]->Print(plotName+"+150");
	}
      }
 
    }
  }
     
  if(save_GIF=="Yes") cout<<"GIF Saved in: "<<plotName<<endl;
  
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           
  app.Run(true);
  return 0;
}
