#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  //TString folder = "list_Ramps";
  //TString folder = "list_Ramps_all";
  //TString folder = "list_Ramps_full"; //10 down to -220 V
  TString folder = "list_Ramps_limit_10uA";
  //TString folder = "list_IV_scans";
  //TString folder = "list_IV_scans_full"; //3 and useless (time... T...)
  TString list_name ="lists/"+folder;

  int limit = 240;  
  if(folder == "list_Ramps_limit_10uA") limit = 11;

  TString PRINT = "Yeso";
  TString separate_plots = "Yeso"; //"Yes" separate canvas, "whatever" superimpose on the same canvas
  TString save_GIF="Yeso"; //"Yes" save animated GIF, "whatever" not save
  if(save_GIF=="Yes") separate_plots="Yeso";
  if(separate_plots=="Yes") save_GIF="No";

  int ccc = 100;

  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  // gStyle->SetOptTitle(0);
  int qq =200;
  TCanvas *plot_mc_data[qq];
  TGraph *IV[qq];
  TString plotName="";
  TString macro_name = ""; int m = 0;

  const char *IVbunch[10] = {"","Default","1 b.","4 b.","12 b.","24 b.","36 b.","72 b.","144 b.","288 b."};
  TLegend *leg2 = new TLegend(0.15,0.87,0.25,0.6);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.033);
 
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________WELCOME____TO___THE____PLOTTING_______________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;

  ifstream input2(list_name); 
  while(!input2.eof()){
    input2 >> macro_name;
    m++;
    if(macro_name!=""){

      if(separate_plots=="Yes")
	plot_mc_data[m] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);
      else
       	if(m==1) {plot_mc_data[1] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);
	  // plot_mc_data[1]->DrawFrame(-100,-1e-7,0,0);
	}
      
      TString u[ccc];
      Float_t v[ccc], a[ccc];
      int cc=0;
      for(int o = 0;  o<ccc; o++) {v[o] = 0; a[o]=0;}
      
      ifstream input3(macro_name); 
      while(!input3.eof()){
	cc++;
	input3 >>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>u[cc]>>v[cc]>>u[cc]>>u[cc]>>u[cc]>>a[cc]>>u[cc];
	// cout<<cc<<"  v  "<<v[cc]<<"  a  "<<a[cc]<<endl;
      }


      Float_t vp[ccc], ap[ccc];
      for(int o = 0;  o<ccc; o++) {vp[o] = -1*v[o]; ap[o] = -1*a[o];}
  
      IV[m] = new TGraph(cc,&vp[0],&ap[0]);
      // IV[m]->SetTitle("IV plot");
      IV[m]->SetTitle(" ");
      IV[m]->GetYaxis()->SetTitle("Leakage current (36.5 #circC) [#muA]");
      IV[m]->GetYaxis()->SetTitleOffset(1.4);
      IV[m]->GetXaxis()->SetTitle("Bias voltage [V]");
      IV[m]->GetYaxis()->SetTitleOffset(1.2);
      IV[m]->SetMarkerColor(m);
      if(m>=5)IV[m]->SetMarkerColor(m+1);
      if(m>=9)IV[m]->SetMarkerColor(m-8);
      IV[m]->SetMarkerStyle(20);
      IV[m]->SetMarkerSize(1);
      // IV[m]->GetYaxis()->SetRangeUser(-11e-6,0);
      // IV[m]->GetYaxis()->SetRangeUser(-180e-6,0);
      for (int i=0;i<IV[m]->GetN();i++) IV[m]->GetY()[i] /= 1e-6; 
      // IV[m]->GetYaxis()->SetRangeUser(-limit,0);
      IV[m]->GetYaxis()->SetRangeUser(0,limit);

      if(folder == "list_Ramps_full" && m<=4) limit = 11;
      else  if(folder == "list_Ramps_full" && m<=7) limit = 55;
      else  if(folder == "list_Ramps_full" && m<=10) limit = 240;
      //if(folder=="list_Ramps_full") IV[1]->GetYaxis()->SetRangeUser(-limit,0);
      if(folder == "list_Ramps_full") IV[1]->GetYaxis()->SetRangeUser(0,limit);
      if(folder == "list_Ramps_all") IV[1]->GetYaxis()->SetRangeUser(0,210);
      if(folder == "list_Ramps_all") IV[1]->GetXaxis()->SetRangeUser(0,100);
      if(folder == "list_Ramps_limit_10uA") IV[1]->GetYaxis()->SetRangeUser(0,21);
      if(folder == "list_Ramps_limit_10uA") IV[1]->GetXaxis()->SetRangeUser(0,100);

      
      if(separate_plots=="Yes") IV[m]->Draw("AP");
      else  {
      	if(m==1) IV[m]->Draw("AP");
      	else IV[m]->Draw("sameP");

	leg2->AddEntry(IV[m],IVbunch[m],"p");
	if(folder == "list_Ramps_limit_10uA" && m==5) leg2->Draw("SAME");    
	if(folder == "list_Ramps_all" && m==9) leg2->Draw("SAME");    
 	
      	if(save_GIF=="Yes"){
      	  plotName="GIF/"+folder+".gif";
      	  plot_mc_data[1]->Print(plotName+"+100");
      	}
      }

    } 
  } 

    
  
  if(save_GIF=="Yes") cout<<"GIF Saved in: "<<plotName<<endl<<endl;
    
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           

  app.Run(true);
  return 0;
}
