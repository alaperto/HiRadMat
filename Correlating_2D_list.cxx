#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TRandom3.h> 
#include <TF1.h> 
#include <TF2.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  TString version = "_v62"; //"_1" for old generation, "_2" for last elog, "_3" for mix elog + raw data
  TString which_list = "_O"; //"_T" for threshold off, "_O" for threshold on
  TString module = "1"; //"1": M1, "2": M2, "3" M1 special, "4" M2 special
  TString only_17_July = "b"; //"" = no, "b" = yes, "c" = old O1 list
  which_list+=module+only_17_July;
  TString list_name= "lists/list_2D_macros"+which_list;
  // TString separate_plots = "Yes"; //"Yes" separate canvas, "whatever" superimpose on the same canvas
  // TString save_GIF="Yeso"; //"Yes" save animated GIF, "whatever" not save

  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  int qq = 200;
  TCanvas *plot_mc_data[qq];
  TH2F *Occup_0_0_MA[qq];
  int aa[40000]; double bb[40000], cc[40000], dd[40000], ff[400000]; int a=0,b=0,c=0,d=0,e=0,f=1;
  TString plotName="";
  // if(separate_plots=="Yes") save_GIF="No";
  TString macro_name = "", f_name = ""; int m = 0, start = 6;//6 M1, 5 M2
  if(which_list=="_T2b") start=5;
  if(which_list=="_O1b") start=1;
  if(which_list=="_O2b") start=1;
  start=0;
  TString do_correlation = "Yes"; //"Yes" show the correlation plots, "whatever" do not show
  TString do_differential = "Yeso"; //"Yes" subtract time "0" distribution, "whatever" do classic
  TString Generate_new = "Yes";//"Yes" generate new beam profiles, or use saved
  TString Generate_show = "Yes";//"Yes" show the generated beam profiles, or not

  // TString do_logX = "Yeso"; //"Yes" do log X axis & move range, "whatever" keep linear X axis and usual range

  cout<<"______________________________________________________________"<<endl;
  cout<<"______WELCOME____TO___THE____DAMAGE-DOSE____CORRELATING_______"<<endl;
  cout<<"__________________Beam_Profile_Simulation_____________________"<<endl<<endl;

  //Dose simulation  
  TGraph *graph[qq];
  TF2 *f2[qq];
  int pix = 26880, step = 0;
  int how_many_plots = 4; //T1 4, T2 3
  int max = pix * how_many_plots;
  // Double_t dam[max], dos[max];
  Double_t dam[pix], dos[pix];
  TCanvas *plot_dose[qq];
  TCanvas *plot_extra[qq];
  TCanvas *can[qq];
  TH2D *h1[qq], *h2[qq];
  TString dose_name= "", histo_dose_name= "", correl_name= "", noise_name= "";
  double X[qq], Y[qq], j[qq], k[qq], l[qq], w[qq], q[qq]; int content[qq], be[qq]; int n_bunches = -1;
  double x[qq], y[qq], sx[qq], sy[qq];
  int last = 32; //last available simulated beam

  // X and Y from elog
  j[1]=-0.60; k[1]=+1.20; l[1]=0;//unuseful, it is going to be subtracted
  j[2]=-0.60; k[2]=+1.20; l[2]=1;//x y morning, extrapolated...
  j[3]=-3.00; k[3]=-1.80; l[3]=1;
  j[4]=-3.21; k[4]=-1.80; l[4]=1;
  j[5]=-2.64; k[5]=-1.88; l[5]=4;
  j[6]=-2.21; k[6]=-1.93; l[6]=4;
  j[7]=-2.43; k[7]=-1.69; l[7]=12;
  j[8]=-2.28; k[8]=-1.83; l[8]=24;
  j[9]=-2.57; k[9]=-1.79; l[9]=36;
  j[10]=-2.43; k[10]=-1.71; l[10]=72;
  j[11]=-3.64; k[11]=-1.90; l[11]=144;
  j[12]=-3.64; k[12]=-1.93; l[12]=288;
  j[13]=-3.57; k[13]=-1.88; l[13]=288*2;
  j[14]=-1.49; k[14]=-3.82; l[14]=0*1;// 0.2 mm operations
  j[15]=-2.61; k[15]=-2.57; l[15]=0*1;
  j[16]=-2.61; k[16]=-2.00; l[16]=0*0;//extrapolated... not shoot?
  j[17]=-2.62; k[17]=-1.78; l[17]=0*1;
  // j[18]=-2.60; k[18]=+3.27; l[18]=8.5/5; //8.5e10
  // j[19]=-2.62; k[19]=+3.40; l[19]=12*2; //12e10
  j[18]=-2.60; k[18]=+3.27; l[18]=8.5/5; //8.5e10
  j[19]=-2.62; k[19]=+3.40; l[19]=12*2; //12e10
  // j[18]=-2.60; k[18]=0; l[18]=8.5/5; //8.5e10
  // j[19]=-2.62; k[19]=0; l[19]=12*2; //12e10
  j[20]=-2.51; k[20]=+3.34; l[20]=0*1*2; //e10
  j[21]=-2.29; k[21]=+4.08; l[21]=0*1*2; //e10
  j[22]=-2.28; k[22]=+4.13; l[22]=0*1;//23; //23(25)e5 alignment shots
  j[23]=-2.33; k[23]=+4.11; l[23]=0*1;//5; //5()e5 alignment shots
  j[24]=-2.34; k[24]=+4.10; l[24]=0*1;//25; //25()e5 alignment shots
  j[25]=-2.33; k[25]=+4.12; l[25]=0*1;//15; //15()e5 alignment shots
  j[26]=-2.39; k[26]=+4.13; l[26]=0*4*2; //e10
  j[27]=-2.32; k[27]=+4.16; l[27]=0*1;//20; //20()e5 alignment shots
  j[28]=-2.35; k[28]=+4.15; l[28]=0*12*2; //e10
  j[29]=-2.35; k[29]=+4.21; l[29]=0*1;//21; //21()e5 alignment shots
  j[30]=-2.34; k[30]=+4.15; l[30]=0*24*2; //e10
  j[31]=-2.39; k[31]=+4.18; l[31]=0*1;//19; //19()e5 alignment shots
  j[32]=-2.39; k[32]=+4.15; l[32]=0*1;//24; //24()e5 alignment shots

  // SIGMA X and Y from elog
  w[1]=1.0;  q[1]=1.0;  be[1]=1;//unuseful, it is going to be subtracted
  w[2]=2.07; q[2]=1.63; be[2]=1;//x y morning, x&y&sigma extrapolated...
  w[3]=2.07; q[3]=1.63; be[3]=1;
  w[4]=2.05; q[4]=1.80; be[4]=1;
  w[5]=2.20; q[5]=1.70; be[5]=1;
  w[6]=2.15; q[6]=1.40; be[6]=1;
  w[7]=1.77; q[7]=1.32; be[7]=1;
  w[8]=1.70; q[8]=1.30; be[8]=1;//not available, interpolated
  w[9]=1.66; q[9]=1.28; be[9]=1;
  w[10]=1.67; q[10]=1.32; be[10]=1;
  w[11]=1.70; q[11]=1.28; be[11]=1;
  w[12]=1.79; q[12]=1.29; be[12]=1;
  w[13]=2.44; q[13]=1.93; be[13]=1;
  w[14]=0.18; q[14]=0.20; be[14]=1;
  w[15]=0.20; q[15]=0.18; be[15]=1;
  w[16]=0.19; q[16]=0.19; be[16]=1;
  w[17]=0.18; q[17]=0.19; be[17]=1;
  w[18]=0.19; q[18]=0.17; be[18]=1;
  w[19]=0.19; q[19]=0.19; be[19]=1;
  w[20]=0.22; q[20]=0.18; be[20]=1;
  w[21]=0.30; q[21]=0.27; be[21]=1;
  w[22]=0.25; q[22]=0.25; be[22]=23;
  w[23]=0.26; q[23]=0.23; be[23]=5;
  w[24]=0.26; q[24]=0.24; be[24]=25;
  w[25]=0.28; q[25]=0.23; be[25]=15;
  w[26]=0.39; q[26]=0.28; be[26]=1;
  w[27]=0.28; q[27]=0.22; be[27]=20;
  w[28]=0.34; q[28]=0.31; be[28]=1;
  w[29]=0.27; q[29]=0.22; be[29]=21;
  w[30]=0.34; q[30]=0.31; be[30]=1;
  w[31]=0.28; q[31]=0.25; be[31]=19;
  w[32]=0.27; q[32]=0.24; be[32]=24;

  content[1]=1e7;
  content[2]=1e7;
  content[3]=1e7;
  content[4]=1e7;
  content[5]=1e7;
  content[6]=1e7;
  content[7]=1e6;
  content[8]=1e6;
  content[9]=1e6;
  content[10]=1e6;
  content[11]=5e5;
  content[12]=5e5;
  content[13]=5e5;
  content[14]=1e7;
  content[15]=1e7;
  content[16]=1e7;
  content[17]=1e7;
  content[18]=1e7;
  content[19]=1e6;
  content[20]=1e7;
  content[21]=1e7;
  content[22]=1e7;
  content[23]=1e7;
  content[24]=1e7;
  content[25]=1e7;
  content[26]=1e7;
  content[27]=1e7;
  content[28]=1e7;
  content[29]=1e7;
  content[30]=1e6;
  content[31]=1e7;
  content[32]=1e7;

  // ATTENTION HERE.... THE NEW PARAMETERS ARE COMING FROM raw data FITTING!!! Here!

  TString Day = "17";
  int mx = 1; TString XY = "X";
  TString x_name = "rawData/Mean_Sigma_"+Day+"_"+XY+".txt";
  ifstream input0(x_name); 
  while(!input0.eof()){
    input0 >> x[mx] >> sx[mx] ;
    mx++;
  }

  int my = 1; XY = "Y";
  TString y_name = "rawData/Mean_Sigma_"+Day+"_"+XY+".txt";
  ifstream input1(y_name); 
  while(!input1.eof()){
    input1 >> y[my] >> sy[my] ;
    my++;
  }

  for(int ww = 1; ww < 33; ww++)
    {
      if(x[ww]==555) {x[ww]=j[ww]; sx[ww]=w[ww];}
      if(y[ww]==555) {y[ww]=k[ww]; sy[ww]=q[ww];}
    }

  TString simu_name = "lists/list_Simulated_Beams_shoot_x.root";
  if(Generate_new=="Yes")
    {

      TFile simu (simu_name,"RECREATE");

      for(int g = 2; g <= last; g++){ //nothing to generate for shot 0
	dose_name = "Canvas_Dose_n_"; dose_name+=g;
	histo_dose_name = "Histo_h1_Dose_n_"; histo_dose_name+=g;
	f_name = "f2["+g; f_name+="]";
	f2[g] = new TF2(f_name,"[0]*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4])",-0.5,79.5,-0.5,335.5);
	n_bunches = content[g]*l[g];//1e6(*5e4)=5e10
	cout<<"Shot g: "<<g<<"  bunches: "<<l[g]<<endl;
	X[g] = 3.90 * ( y[g] - 3.68) + 62.5; //Calibrated OK
	Y[g] = -21.03 * ( x[g] + 2.61) + 163; //Calibrated OK
	//cout<<" g loop: "<<g<<"  x_beam: "<<j[g]<<"  y_beam: "<<k[g]<<"  X_det: "<<X[g]<<"  Y_det: "<<Y[g]<<endl;//"  x_beam: "<<<<g<<"  x_beam: "<<<<g<<"  x_beam: "<<<<g<<"  x_beam: "<<<<g<<"  x_beam: "<<<<g<<"  x_beam: "<<
	cout<<" g loop: "<<g<<"  x_beam: "<<X[g]<<"  y_beam: "<<Y[g]<<"  sX_det: "<<sx[g]<<"  sY_det: "<<sy[g]<<endl;//"  x_beam: "<<<<g<<"  x_beam: "<<<<g<<"  x_beam: "<<<<g<<"  x_beam: "<<<<g<<"  x_beam: "<<<<g<<"  x_beam: "<<


	if(g<14)
	  f2[g]->SetParameters(1,X[g],q[g]/0.250,Y[g],w[g]/0.050);
	else
	  f2[g]->SetParameters(1,X[g],q[g]/0.250*2,Y[g],w[g]/0.050*2);
	//Sigma invert, since it is inverted, and divide per Pixel (in Pixel)

	// if(g<14)
	//   f2[g]->SetParameters(1,X[g],15*sy[g],Y[g],70*sx[g]);//1,40,13,168,55);
	// else
	//   f2[g]->SetParameters(1,X[g],10*sy[g],Y[g],70*sx[g]);//1,40,13,168,55);
	h1[g] = new TH2D(histo_dose_name,histo_dose_name,80,-0.5,79.5,336,-0.5,335.5);
	h1[g]->FillRandom(f_name,n_bunches); 
	for(int p = 1; p < pix; p++){
	  h1[g]->SetBinContent(p,h1[g]->GetBinContent(p)*5e10/content[g]);//*5e4);//(1e6)*5e4=5e10//(1e7)*5e3=5e10
	}	
	// cout<<" g = "<<g<<"  and be[g] = "<<be[g]<<endl;
	if(be[g]>1) h1[g]->Add(h1[g],be[g]-1);
	// for(int cu = 0; cu < be[g]; cu++){}
	simu.cd();
	h1[g]->Write(); //Save single beam plot "h1"
	
	if(g>2)  h1[g]->Add(h1[g-1],1);
	simu.cd(); //Save integrated dose beam plots "h2"
	histo_dose_name = "Histo_h2_Dose_n_"; histo_dose_name+=g;
	h1[g]->SetName(histo_dose_name);//+"_integr");
	h1[g]->Write();
	// plot_dose[g] = new TCanvas(dose_name,dose_name,700,550);
	// plot_dose[g]->cd();
	// h1[g]->Draw("COLZ");
	// plot_dose[g]->Update();
      }
      
      simu.Close();
    }
  
  
  TFile *simu_file = new TFile(simu_name);
  for(int g = 2; g <= last; g++){
    dose_name = "Canvas_Dose_n_"; dose_name+=g;
    histo_dose_name = "Histo_h2_Dose_n_"; histo_dose_name+=g;// histo_dose_name+="_integr";
    h2[g]= (TH2D*) simu_file->Get(histo_dose_name);
    if(Generate_show=="Yes"){
      plot_dose[g] = new TCanvas(dose_name,dose_name,700,550);
      plot_dose[g]->cd();
      h2[g]->Draw("COLZ");
      plot_dose[g]->Update();
      plotName="saved_plots/"+dose_name+version+".png";
      plot_dose[g]->Print(plotName);
    }
  }
  
  cout<<"______________________________________________________________"<<endl;
  cout<<"________PREPARATION___FOR___DAMAGE-DOSE____CORRELATION________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;

  ifstream input2(list_name); 
  // if(4>8)
  while(!input2.eof()){
    m++;
    input2 >> macro_name;
    cout<<" Shot m: "<<m<<" macro name: "<<macro_name<<endl;//" which list: "<<which_list<<endl;
    if(m<=last)
      if((which_list=="_T1b"&&(m!=2)&&(m!=3)&&(m!=9)&&(m!=14)&&(m!=15)&&(m!=16)&&(m!=17)&&(m!=21)&&(m!=23)&&(m!=24)&&(m!=25)&&(m!=26)&&(m!=27)&&(m!=29)&&(m!=30)&&(m!=31))
	 ||(which_list=="_T2b"&&(m!=2)&&(m!=9)&&(m!=10)&&(m!=13)&&(m!=14)&&(m!=15)&&(m!=16)&&(m!=17)&&(m!=18)&&(m!=19)&&(m!=20)&&(m!=21)&&(m!=23)&&(m!=24)&&(m!=25)&&(m!=26)&&(m!=27)&&(m!=29)&&(m!=30)&&(m!=31))
	 ||(which_list=="_O1b"&&(m!=3)&&(m!=5)&&(m!=7)&&(m!=8)&&(m!=9)&&(m!=11)&&(m!=12)&&(m!=13)&&(m!=14)&&(m!=15)&&(m!=16)&&(m!=17)&&(m!=18)&&(m!=20)&&(m!=21)&&(m!=23)&&(m!=24)&&(m!=25)&&(m!=26)&&(m!=27)&&(m!=29)&&(m!=30)&&(m!=31))
	 ||(which_list=="_O2b"&&(m!=6)&&(m!=7)&&(m!=8)&&(m!=9)&&(m!=10)&&(m!=11)&&(m!=12)&&(m!=13)&&(m!=14)&&(m!=15)&&(m!=16)&&(m!=17)&&(m!=18)&&(m!=19)&&(m!=20)&&(m!=21)&&(m!=22)&&(m!=23)&&(m!=24)&&(m!=25)&&(m!=26)&&(m!=27)&&(m!=29)&&(m!=30)&&(m!=31)))
	if(macro_name!=""){
	    
	  Occup_0_0_MA[m] = new TH2F("Plot_"+macro_name,"Plot_"+macro_name,80,-0.5,79.5,336,-0.5,335.5);

	  a=0,b=0,c=0,d=0,e=0,f=1;

	  list_name = "tables/"+macro_name+".txt";
	  ifstream input3(list_name); 
	  while(!input3.eof()){
	    input3 >> ff[f];
	    if(d==0)
	      if(ff[2]!=0){
		if(f%3==2) {aa[f/3+1]=ff[f]; e=f/3+1;
		  if(ff[f]==0) d=1;}
		if(f%3==0){bb[f/3]=ff[f];}
	      }
	      else{
		if(f%4==3) {aa[f/4]=ff[f]; e=f/4;
		  if(ff[f]==0) d=1;}
		if(f%4==0){bb[f/4]=ff[f];}
	      }
	    f++;
	  }
  
    
	  //for debug purpose
	  //for(int x = 1; x < 26; x++)
	  // for(int x = 1; x < e; x++)
	  //   cout<<x<<"   "<<aa[x]<<"   "<<bb[x]<<<<endl;
    
	  for(int x = 1; x < e; x++)
	    {
	      // if(bb[x]==1.053496) bb[x]=0;
	      Occup_0_0_MA[m]->SetBinContent(aa[x],bb[x]);
	    }

	  // Occup_0_0_MA[1]->SetTitle(macro_name);//Change Title for every file!
	  Occup_0_0_MA[m]->SetTitle(macro_name);//Change Title for every file!
	  Occup_0_0_MA[m]->GetXaxis()->SetTitle("Column");
	  // Occup_0_0_MA[m]->GetXaxis()->SetNdivisions(-401);
	  // Occup_0_0_MA[m]->GetXaxis()->SetLabelFont(42);
	  // Occup_0_0_MA[m]->GetXaxis()->SetTitleSize(0.03);
	  // Occup_0_0_MA[m]->GetXaxis()->SetTitleOffset(0.8);
	  // Occup_0_0_MA[m]->GetXaxis()->SetTitleFont(42);
	  Occup_0_0_MA[m]->GetYaxis()->SetTitle("Row");
	  // Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(336,"0");
	  // Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(236,"100");
	  // Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(136,"200");
	  // Occup_0_0_MA[m]->GetYaxis()->SetBinLabel(36,"300");
	  // Occup_0_0_MA[m]->GetYaxis()->SetNdivisions(-401);
	  // Occup_0_0_MA[m]->GetYaxis()->SetLabelFont(42);
	  // Occup_0_0_MA[m]->GetYaxis()->SetLabelSize(0.045);
	  // Occup_0_0_MA[m]->GetYaxis()->SetTitleSize(0.03);
	  // Occup_0_0_MA[m]->GetYaxis()->SetTickLength(0.015);
	  // Occup_0_0_MA[m]->GetYaxis()->SetTitleOffset(1.1);
	  // Occup_0_0_MA[m]->GetYaxis()->SetTitleFont(42);
	  // Occup_0_0_MA[m]->GetZaxis()->SetLabelFont(42);
	  // Occup_0_0_MA[m]->GetZaxis()->SetLabelSize(0.035);
	  // Occup_0_0_MA[m]->GetZaxis()->SetTitleSize(0.035);
	  // Occup_0_0_MA[m]->GetZaxis()->SetTitleFont(42);      
	  Occup_0_0_MA[m]->SetMinimum(0);
	  Occup_0_0_MA[m]->SetMaximum(200);
	  if(which_list=="_T1b") Occup_0_0_MA[m]->SetMaximum(500);
	  if(which_list=="_T2b") Occup_0_0_MA[m]->SetMaximum(500);
	  if(which_list=="_O1b") Occup_0_0_MA[m]->SetMaximum(500);
	  if(which_list=="_O2b") Occup_0_0_MA[m]->SetMaximum(500);
	  if(which_list=="_T1") Occup_0_0_MA[m]->SetMaximum(500);
	  if(which_list=="_T2") Occup_0_0_MA[m]->SetMaximum(500);
	  if(which_list=="_S1") Occup_0_0_MA[m]->SetMaximum(100);
	  if(which_list=="_S2") Occup_0_0_MA[m]->SetMaximum(100);
	  if(which_list=="_D") Occup_0_0_MA[m]->SetMaximum(220);
	  if(which_list=="_A") Occup_0_0_MA[m]->SetMaximum(220);

	  if(m>start)//>start+1
	    {
	      
	      noise_name = "Canvas_Noise_n_"; noise_name+=m;
	      noise_name+="_"; noise_name+=macro_name;
	      plot_mc_data[m] = new TCanvas(noise_name,noise_name,700,550);
	      Occup_0_0_MA[m]->Draw("COLZ");
	      plotName="saved_plots/"+noise_name+version+".png";
	      plot_mc_data[m]->Print(plotName);

	      

	      // if(separate_plots=="Yes"){ 
	      // 	plot_mc_data[m] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);
	      // 	Occup_0_0_MA[m]->Draw("COLZ");
	      // }
	      // else  {
	      // 	if(m==1) {
	      // 	  plot_mc_data[1] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);
	      // 	  Occup_0_0_MA[m]->Draw("COLZ");
	      // 	}
	      // 	else Occup_0_0_MA[m]->Draw("COLZSAME");
      
	      // 	if(save_GIF=="Yes"){
	      // 	  plotName="saved_plots/Animated_2D"+which_list+".gif";
	      // 	  plot_mc_data[1]->Print(plotName+"+150");
	      // 	}
	      // }
    
	      // m--;//always together
	      if(m>1)
		if(do_correlation=="Yes"){

		  if(m>1&&do_differential=="Yes") Occup_0_0_MA[m]->Add(Occup_0_0_MA[1],-1);//subtract 2D scan at shot 0

		  //one vector for every scan
		  for(int p = 0; p < pix; p++){
		    // h2[m]->SetBinContent(p,h2[m]->GetBinContent(p));//*5e4);//(1e6)*5e4=5e10//(1e7)*5e3=5e10
		    dos[p]=h2[m]->GetBinContent(p);
		    dam[p]=Occup_0_0_MA[m]->GetBinContent(p);
		    if(dam[p]<=0) {dam[p]=0; dos[p]=0;}
		  }

		  //Draw, separate plots for points out of the trends ")"
		  if(which_list=="_T1b") 
		    if(m == 8 || m == 10|| m == 11 || m == 12|| m == 13|| m == 18|| m == 19|| m == 20|| m == 22|| m == 28|| m == 32){
		      dose_name = "Canvas_Extra_n_"; dose_name+=m; dose_name+="_"; dose_name+=macro_name;
		      histo_dose_name = "Histo_T1_extra_n_"; histo_dose_name+=m;
		      plot_extra[m+100] = new TCanvas(dose_name,dose_name,700,550);
		      h2[m+100] = new TH2D(histo_dose_name,histo_dose_name,80,-0.5,79.5,336,-0.5,335.5);
		      cout<<"New canvas for m: "<<m<<endl;
		      for(int p = 0; p < pix; p++){
			// double value = graph[7]->GetBinContent(p);
			if(m==8)
			  if(dos[p] > 5e6 && dos[p] < 1e8)
			    if(dam[p] > 400 && dam[p] < 3000){
			      // h2[15]->Fill(p,n_bunches);   
			      h2[m+100]->SetBinContent(p,1111);//*100);
			    }
			if(m==10)
			  if(dos[p] > 1e7 && dos[p] < 3e8)
			    if(dam[p] > 400 && dam[p] < 3000){
			      // h2[15]->Fill(p,n_bunches);   
			      h2[m+100]->SetBinContent(p,1111);//*100);
			    }
			if(m==11)
			  if(dos[p] > 1e7 && dos[p] < 5e8)
			    if(dam[p] > 400 && dam[p] < 3000){
			      // h2[15]->Fill(p,n_bunches);   
			      h2[m+100]->SetBinContent(p,1111);//*100);
			    }
			if(m==12)
			  if(dos[p] > 8e7 && dos[p] < 1e9)
			    if(dam[p] > 400 && dam[p] < 3000){
			      // h2[15]->Fill(p,n_bunches);   
			      h2[m+100]->SetBinContent(p,1111);//*100);
			    }
			if(m==13){
			  // if(dos[p] > 4e8 && dos[p] < 2e9)
			  //   if(dam[p] > 400 && dam[p] < 3000){
			  //     // h2[15]->Fill(p,n_bunches);   
			  //     h2[m+100]->SetBinContent(p,1111);//*100);
			  if(dos[p] > 2.8e9 && dos[p] < 3.6e9)
			    h2[m+100]->SetBinContent(p,666);
			  if(dos[p] > 3.6e9 && dos[p] < 2e11)
			    h2[m+100]->SetBinContent(p,1111);
			}
			if(m==18){
			  // if(dos[p] > 5e9 && dos[p] < 2e11){
			  // if(dam[p] > 300 ) h2[m+100]->SetBinContent(p,1111);
			  // if(dam[p] < 300 ) h2[m+100]->SetBinContent(p,555);
			  // }
			  if(dos[p] > 2.8e9 && dos[p] < 3.6e9)
			    h2[m+100]->SetBinContent(p,111);
			  if(dos[p] > 3.6e9 && dos[p] < 4.2e9)
			    h2[m+100]->SetBinContent(p,666);
			  if(dos[p] > 4.2e9 && dos[p] < 2e11)
			    h2[m+100]->SetBinContent(p,1111);
			}
			if(m==19)
			  if(dos[p] > 5e9 && dos[p] < 2e11){
			    if(dam[p] > 300 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 300 ) h2[m+100]->SetBinContent(p,555);
			  }
			if(m==20)
			  if(dos[p] > 5e9 && dos[p] < 2e11){
			    if(dam[p] > 300 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 300 ) h2[m+100]->SetBinContent(p,555);
			  }
			if(m==22)
			  if(dos[p] > 5e9 && dos[p] < 2e11){
			    if(dam[p] > 300 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 300 ) h2[m+100]->SetBinContent(p,555);
			  }
			if(m==28)
			  if(dos[p] > 5e9 && dos[p] < 2e11){
			    if(dam[p] > 300 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 300 ) h2[m+100]->SetBinContent(p,555);
			  }
			if(m==32)
			  if(dos[p] > 5e9 && dos[p] < 2e11){
			    if(dam[p] > 300 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 300 ) h2[m+100]->SetBinContent(p,555);
			  }
		      }
		      h2[m+100]->Draw("COLZ");
		      plotName="saved_plots/"+dose_name+version+".png";
		      plot_extra[m+100]->Print(plotName);
		    }

		  if(which_list=="_T2b") 
		    if(m == 7 || m == 8 || m == 11 || m == 12 || m == 22 || m == 28 || m == 32){
		      dose_name = "Canvas_Extra_n_"; dose_name+=m; dose_name+="_"; dose_name+=macro_name;
		      histo_dose_name = "Histo_T2_extra_n_"; histo_dose_name+=m;
		      plot_extra[m+100] = new TCanvas(dose_name,dose_name,700,550);
		      h2[m+100] = new TH2D(histo_dose_name,histo_dose_name,80,-0.5,79.5,336,-0.5,335.5);
		      cout<<"New canvas for m: "<<m<<endl;
		      for(int p = 0; p < pix; p++){
			// double value = graph[7]->GetBinContent(p);
			if(m==7)
			  if(dos[p] > 1e6 && dos[p] < 1e8)
			    if(dam[p] > 500 && dam[p] < 3000){
			      // h2[15]->Fill(p,n_bunches);   
			      h2[m+100]->SetBinContent(p,1111);//*100);
			    }
			if(m==8)
			  if(dos[p] > 5e6 && dos[p] < 2e8)
			    if(dam[p] > 500 && dam[p] < 3000){
			      // h2[15]->Fill(p,n_bunches);   
			      h2[m+100]->SetBinContent(p,1111);//*100);
			    }
			if(m==11)
			  if(dos[p] > 5e7 && dos[p] < 1e9)
			    if(dam[p] > 600 && dam[p] < 3000){
			      // h2[15]->Fill(p,n_bunches);   
			      h2[m+100]->SetBinContent(p,1111);//*100);
			    }
			if(m==12)
			  if(dos[p] > 1e8 && dos[p] < 2e9)
			    if(dam[p] > 600 && dam[p] < 3000){
			      // h2[15]->Fill(p,n_bunches);   
			      h2[m+100]->SetBinContent(p,1111);//*100);
			    }
			if(m==22)
			  if(dos[p] > 0e9 && dos[p] < 10e9){
			    // if(dam[p] > 500 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 10 ) h2[m+100]->SetBinContent(p,1111);
			  }
			if(m==28)
			  if(dos[p] > 3e9 && dos[p] < 5e9){
			    if(dam[p] > 500 ) h2[m+100]->SetBinContent(p,1111);
			    // if(dam[p] < 500 ) h2[m+100]->SetBinContent(p,555);
			  }
			if(m==32)
			  if(dos[p] > 3e9 && dos[p] < 5e9){
			    if(dam[p] > 500 ) h2[m+100]->SetBinContent(p,1111);
			    // if(dam[p] < 500 ) h2[m+100]->SetBinContent(p,555);
			  }
		      }
		      h2[m+100]->Draw("COLZ");
		      plotName="saved_plots/"+dose_name+version+".png";
		      plot_extra[m+100]->Print(plotName);
		    }

		  if(which_list=="_O1b") 
		    if(m == 4 || m == 6 || m == 10 || m == 19 || m == 22 || m == 28 || m == 32){
		      dose_name = "Canvas_Extra_n_"; dose_name+=m; dose_name+="_"; dose_name+=macro_name;
		      histo_dose_name = "Histo_O1_extra_n_"; histo_dose_name+=m;
		      plot_extra[m+100] = new TCanvas(dose_name,dose_name,700,550);
		      h2[m+100] = new TH2D(histo_dose_name,histo_dose_name,80,-0.5,79.5,336,-0.5,335.5);
		      cout<<"New canvas for m: "<<m<<endl;
		      for(int p = 0; p < pix; p++){
			// double value = graph[7]->GetBinContent(p);
			if(m==4)
			  // if(dos[p] > 5e6 && dos[p] < 1e8)
			  if(dam[p] > 170 && dam[p] < 3000){
			    h2[m+100]->SetBinContent(p,1111);//*100);
			  }
			if(m==6)
			  // if(dos[p] > 1e7 && dos[p] < 3e8)
			  if(dam[p] > 170 && dam[p] < 3000){
			    h2[m+100]->SetBinContent(p,1111);//*100);
			  }
			if(m==10)
			  if(dos[p] > 0 && dos[p] < 4e8)
			    if(dam[p] > 180 && dam[p] < 3000){
			      h2[m+100]->SetBinContent(p,1111);//*100);
			    }
			if(m==19){
			  if(dos[p] > 4.9e9 && dos[p] < 10e11){
			    if(dam[p] > 190 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 190 ) h2[m+100]->SetBinContent(p,666);
			  }
			  else 
			    if(dos[p] > 3e9 && dos[p] < 4.9e9)  h2[m+100]->SetBinContent(p,111);
			}
			if(m==22)
			  if(dos[p] > 4.9e9 && dos[p] < 10e11){
			    if(dam[p] > 190 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 190 ) h2[m+100]->SetBinContent(p,555);
			  }
			if(m==28)
			  if(dos[p] > 4.9e9 && dos[p] < 10e11){
			    if(dam[p] > 190 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 190 ) h2[m+100]->SetBinContent(p,555);
			  }
			if(m==32)
			  if(dos[p] > 4.9e9 && dos[p] < 10e11){
			    if(dam[p] > 190 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 190 ) h2[m+100]->SetBinContent(p,555);
			  }

		      }
		      h2[m+100]->Draw("COLZ");
		      plotName="saved_plots/"+dose_name+version+".png";
		      plot_extra[m+100]->Print(plotName);
		    }

		  if(which_list=="_O2b") 
		    if(m == 3 || m == 4 || m == 5 || m == 28 || m == 32){
		      dose_name = "Canvas_Extra_n_"; dose_name+=m; dose_name+="_"; dose_name+=macro_name;
		      histo_dose_name = "Histo_O2_extra_n_"; histo_dose_name+=m;
		      plot_extra[m+100] = new TCanvas(dose_name,dose_name,700,550);
		      h2[m+100] = new TH2D(histo_dose_name,histo_dose_name,80,-0.5,79.5,336,-0.5,335.5);
		      cout<<"New canvas for m: "<<m<<endl;
		      for(int p = 0; p < pix; p++){
			// double value = graph[7]->GetBinContent(p);
			if(m==3)
			  // if(dos[p] > 1e6 && dos[p] < 1e8)
			  if(dam[p] > 190 && dam[p] < 3000){
			    // h2[15]->Fill(p,n_bunches);   
			    h2[m+100]->SetBinContent(p,1111);//*100);
			  }
			if(m==4)
			  // if(dos[p] > 5e6 && dos[p] < 2e8)
			  if(dam[p] > 190 && dam[p] < 3000){
			    // h2[15]->Fill(p,n_bunches);   
			    h2[m+100]->SetBinContent(p,1111);//*100);
			  }
			if(m==5)
			  // if(dos[p] > 5e7 && dos[p] < 1e9)
			  if(dam[p] > 190 && dam[p] < 3000){
			    // h2[15]->Fill(p,n_bunches);   
			    h2[m+100]->SetBinContent(p,1111);//*100);
			  }
			if(m==28)
			  if(dos[p] > 3.77e9 && dos[p] < 1e10){
			    if(dam[p] > 190 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 190 ) h2[m+100]->SetBinContent(p,555);
			  }
			if(m==32){
			  if(dos[p] > 4.9e9 && dos[p] < 10e11){
			    if(dam[p] > 190 ) h2[m+100]->SetBinContent(p,1111);
			    if(dam[p] < 190 ) h2[m+100]->SetBinContent(p,666);
			  }
			  else  if(dam[p] > 500 ) h2[m+100]->SetBinContent(p,111);
			}
		      }
		      h2[m+100]->Draw("COLZ");
		      plotName="saved_plots/"+dose_name+version+".png";
		      plot_extra[m+100]->Print(plotName);
		    }
	       

		  graph[m] = new TGraph(pix,dos,dam);//0,10e10,0,10e10 //"Hello","Hello"
		  graph[m]->SetTitle("Damage vs Dose "+macro_name);
		  graph[m]->GetXaxis()->SetTitle("Flux [protons/pixel]");
		  graph[m]->GetYaxis()->SetTitle("Noise [e^{-}]");
		  graph[m]->GetYaxis()->SetTitleOffset(1.2);
		  graph[m]->GetXaxis()->SetTitleOffset(1.2);
		  graph[m]->GetYaxis()->SetRangeUser(0,1500);
		  if(which_list=="_O1b") graph[m]->GetYaxis()->SetRangeUser(0,800);
		  // if(which_list=="_O2b") graph[m]->GetYaxis()->SetRangeUser(0,600);
		  if(which_list=="_O2b") graph[m]->GetYaxis()->SetRangeUser(0,800);
		  graph[m]->SetMarkerStyle(20);
		  graph[m]->SetMarkerSize(0.2);
		  step++;
		  if(step==10) step = 1;
		  graph[m]->SetMarkerColor(step);
		  if(step==5) graph[m]->SetMarkerColor(kOrange-3);
		  // if(step==10) graph[m]->SetMarkerColor(1);

		  correl_name = "Canvas_Correl_n_"; correl_name+=m;
		  correl_name+="_"; correl_name+=macro_name;
		  can[m] = new TCanvas(correl_name,correl_name,200,10,700,500);
		  can[m]->cd();
		  graph[m]->Draw("AP");
		  plotName="saved_plots/"+correl_name+version+".png";
		  can[m]->Print(plotName);
		  cout<<"  Correl. drawn for Shot m: "<<m<<" --> Step: "<<step<<endl;
	      
		}//always together	    
	      // m++;
	  
	    }
	  
	}

  }
    
  gStyle->SetOptTitle(0);
 
  if(which_list=="_T1b"){
    TCanvas *final_T1 = new TCanvas("Canvas_final_T1","Canvas_final_T1",200,10,1500,500);
    final_T1->SetLogx();
    graph[32]->GetXaxis()->SetRangeUser(1e7,1e11);
    graph[32]->Draw("AP");
    graph[8]->Draw("sameP");
    graph[10]->Draw("sameP");
    graph[11]->Draw("sameP");
    graph[12]->Draw("sameP");
    graph[13]->Draw("sameP");
    graph[18]->Draw("sameP");
    graph[19]->Draw("sameP");
    graph[20]->Draw("sameP");
    graph[22]->Draw("sameP");
    graph[28]->Draw("sameP");
    graph[32]->Draw("sameP");
    cout<<"   Drawn together: 8 10 11 12 13 18 19 20 22 28 32"<<endl;
    plotName="saved_plots/Canvas_Final_T1"+version+".png";
    final_T1->Print(plotName);    
  }

  if(which_list=="_T2b"){
    TCanvas *final_T2 = new TCanvas("Canvas_final_T2","Canvas_final_T2",200,10,1500,500);
    final_T2->SetLogx();
    graph[32]->GetXaxis()->SetRangeUser(1e7,1e11);
    graph[32]->Draw("AP");
    graph[7]->Draw("sameP");
    graph[8]->Draw("sameP");
    graph[11]->Draw("sameP");
    graph[12]->Draw("sameP");
    graph[22]->Draw("sameP");
    graph[28]->Draw("sameP");
    graph[32]->Draw("sameP");
    cout<<"   Drawn together: 7 8 11 12 22 28 32"<<endl;
    plotName="saved_plots/Canvas_Final_T2"+version+".png";
    final_T2->Print(plotName);
  }

  if(which_list=="_O1b"){
    // TCanvas *final_O1 = new TCanvas("Canvas_final_O1","Canvas_final_O1",200,10,1500,500);
    TCanvas *final_O1 = new TCanvas("Canvas_final_O1","Canvas_final_O1",200,10,700,550);
    // final_O1->SetLogx();
    // graph[32]->GetXaxis()->SetRangeUser(1e6,1e11);
    graph[32]->GetXaxis()->SetRangeUser(0,1e11);
    graph[32]->Draw("AP");
    graph[28]->Draw("sameP");
    graph[22]->Draw("sameP");
    graph[19]->Draw("sameP");
    graph[10]->Draw("sameP");
    graph[6]->Draw("sameP");
    graph[4]->Draw("sameP");  
    cout<<"   Drawn together: 4 6 10 19 22 28 32"<<endl;
    plotName="saved_plots/Canvas_Final_O1"+version+".png";
    final_O1->Print(plotName);
  }

  if(which_list=="_O2b"){
    // TCanvas *final_O2 = new TCanvas("Canvas_final_O2","Canvas_final_O2",200,10,1500,500);
    TCanvas *final_O2 = new TCanvas("Canvas_final_O2","Canvas_final_O2",200,10,700,550);
    final_O2->SetLogx();
    graph[32]->GetXaxis()->SetRangeUser(1e6,1e11);
    graph[32]->Draw("AP");
    graph[3]->Draw("sameP");
    graph[4]->Draw("sameP");
    graph[5]->Draw("sameP");
    graph[28]->Draw("sameP");
    graph[32]->Draw("sameP");
    graph[28]->Draw("AP");
    cout<<"   Drawn together: 3 4 5 28 32"<<endl;
    plotName="saved_plots/Canvas_Final_O2"+version+".png";
    final_O2->Print(plotName);
  }

  // if(save_GIF=="Yes") cout<<"GIF Saved in: "<<plotName<<endl;
  
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           
  app.Run(true);
  return 0;
}

//one vector, many plots
// step++;
// if(step<=how_many_plots)
// for(int p = 0; p < pix; p++){
// 	h2[m]->SetBinContent(p,h2[m]->GetBinContent(p)*5*100000);
// 	dos[p*step]=h2[m]->GetBinContent(p);
// 	dam[p*step]=Occup_0_0_MA[m+1]->GetBinContent(p);
// 	if(dam[p]<=0) {dam[p]=0; dos[p]=0;}
// }

// //One vector, many plots approach
// can[m] = new TCanvas("Canvas_Correl_"+macro_name,"Canvas_Correl_"+macro_name,200,10,700,500);
// can[m]->cd();
// graph[m] = new TGraph(max,dos,dam);//0,10e10,0,10e10 //"Hello","Hello"
// graph[m]->SetTitle("Damage vs Dose");
// graph[m]->GetXaxis()->SetTitle("Flux [n protons]");
// graph[m]->GetYaxis()->SetTitle("Damage [noise]");
// graph[m]->GetYaxis()->SetTitleOffset(1.2);
// graph[m]->GetXaxis()->SetTitleOffset(1.2);
// graph[m]->GetYaxis()->SetRangeUser(0,1500);
// graph[m]->SetMarkerStyle(20);
// graph[m]->SetMarkerSize(1);
// graph[m]->Draw("AP");


// double bigaussian_pdf(double x, double y, double sigmax , double sigmay , double rho , double x0 , double y0 ) {
// double u = (x-x0)/sigmax;
// double v = (y-y0)/sigmay;
// double c = 1. - rho*rho;
// double z = u*u - 2.*rho*u*v + v*v;
// return 1./(2*M_PI*sigmax*sigmay*std::sqrt(c) )*std::exp(- z / (2.*c) );
// }

//http://mathworld.wolfram.com/BivariateNormalDistribution.html


// 17 Luglio
// b[1] = new TDatime(2017,07,17,10,00,00); //several shots in the morning...
// b[2] = new TDatime(2017,07,17,13,37,00); //13:37 1 b; 5 10e10
// b[3] = new TDatime(2017,07,17,14,0,00); //14:00 1 b; 5 10e10
// b[4] = new TDatime(2017,07,17,14,31,00); //14:31 1 b; 5 10e10
// b[5] = new TDatime(2017,07,17,15,3,00); //15:03 4 b sep 1.7 us; x 5 E10
// b[6] = new TDatime(2017,07,17,15,37,00); //15:37 4 b sep 1.7 us; x 5 E10
// b[7] = new TDatime(2017,07,17,17,6,00); //17:06 12 b sep 25 ns; x 5 E10
// b[8] = new TDatime(2017,07,17,17,37,00); //17:37 24 b sep 25 ns; x 5 E10
// b[9] = new TDatime(2017,07,17,17,55,00); //17:55 36 b sep 25 ns; x 5 E10
// b[10] = new TDatime(2017,07,17,18,9,00); //18:09 72 b sep 25 ns; x 5 E10
// b[11] = new TDatime(2017,07,17,18,48,00); //18:48 144 b sep 25 ns;  5 E10
// b[12] = new TDatime(2017,07,17,19,14,00);//19:14 288 b sep 25 ns;  5 E10
// b[13] = new TDatime(2017,07,17,19,56,00);//19:56 288 b sep 25 ns; 10 E10
// Already simulated in the "old_list.cxx file"
// b[14] = new TDatime(2017,07,17,20,39,00);//20:39 1 b; 5 E10
// b[15] = new TDatime(2017,07,17,20,47,00);//20:47 1 b; 5 E10
// b[16] = new TDatime(2017,07,17,20,49,00);//20:49 1 b; 5 E10
// b[17] = new TDatime(2017,07,17,20,57,00);//20:57 1 b; 5 E10
// b[18] = new TDatime(2017,07,17,21,12,00);//21:12 1 b; 8.5 E10
// b[19] = new TDatime(2017,07,17,22,1,00);//22:01 12 b; 10 E10 (08:26 T1 O1)
// 18 Luglio
// b[20] = new TDatime(2017,07,18,12,48,00);//12:48 1 b; 10 E10
// b[21] = new TDatime(2017,07,18,13,56,00);//13:56 1 b; 10 E10
// b[22] = new TDatime(2017,07,18,14,40,00);//14:20-14:40 1+1+1... = 23 b; 5 E10
// LAST SIMULATION
// b[23] = new TDatime(2017,07,18,17,53,00);//17:53 1+1+1... = 5 b; 5 E10
// b[24] = new TDatime(2017,07,18,17,56,00);//17:56 1+1+1... = 25 b; 5 E10
// b[25] = new TDatime(2017,07,18,19,07,00);//19:07 1+1+1... = 15 b; 5 E10
// b[26] = new TDatime(2017,07,18,21,20,00);//21:20 4 b; 10 E10
// b[27] = new TDatime(2017,07,18,23,58,00);//23:58 1+1+1... = 20 b; 5 E10
// b[28] = new TDatime(2017,07,18,00,37,00);//00:37 12 b; 10 E10
// 19 Luglio
// b[29] = new TDatime(2017,07,19,23,02,00);//23:02 1+1+1... = 21 b; 5 E10
// b[30] = new TDatime(2017,07,19,23,42,00);//23:42 24 b; 10 E10
// 20 Luglio
// b[31] = new TDatime(2017,07,20,14,27,00);//14:27 1+1+1... = 19 b; 5 E10
// b[32] = new TDatime(2017,07,20,14,40,00);//14:40 1+1+1... = 24 b; 5 E10
// STOP BEAM SIMULATION at 15.30 del 20 Luglio
