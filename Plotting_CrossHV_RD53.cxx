#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  TString PRINT = "Yes";
  TString fol = "./RD53_CrossHV/";
  
  //TString M_name = "F02_91_02_25_1E";
  //TString M_name = "F02_02_50_1E_01";
  //TString M_name = "F02_02_50_1E_03";
  
  //TString M_name="M2_Lin_1000e", type = "1E", title = "F02_91_02_25_1E";
  //TString M_name="A12_Lin_1000e", type = "2E", title="F03_30_25x100-2E_A1.2"; //40V
  //TString M_name="D113_Lin_1000e", type="1E", title="F03_30_25x100-1E_D11.3"; //8 V
  //TString M_name="W53_Lin_1000e", type = "1E", title="F02_05_25x100-1E_03";//70VV
  //TString M_name="B44_Lin_1000e", type="2E", title="F03_30_25x100-2E_B4.4"; //30 V
  TString M_name="C43_Lin_1000e", type="2E", title="F03_30_25x100-2E_C4.3"; //30 V


  int ccc = 100;
  TString u[ccc];
  Float_t v[ccc], ev[ccc], a[ccc], ea[ccc];
  int cc=0;
 
  ifstream input3(fol+M_name+".dat");//reduced 0 5 10-20-100
  while(!input3.eof()){
    input3 >>v[cc]>>ev[cc]>>a[cc]>>ea[cc];
    cout<<cc<<"  v  "<<v[cc]<<"  at  "<<a[cc]<<"  +/-  "<<ea[cc]<<endl;
    cc++;
  }

  // TString uB[ccc];
  // Float_t vB[ccc], aB[ccc];
  // int ccB=0;
 
  // ifstream input3B(fol+M_name+"_bared.dat");//from wafer 0-80
  // while(!input3B.eof()){
  //   input3B >>vB[ccB]>>aB[ccB];
  //   cout<<ccB<<"  vB  "<<vB[ccB]<<"  aB  "<<aB[ccB]<<endl;
  //   ccB++;
  // }


  TCanvas *c3 = new TCanvas("c3","c3",20,10,800,500);
  c3->cd();  
  TGraphErrors *IV = new TGraphErrors(cc,&v[0],&a[0],&ev[0],&ea[0]);
  IV->SetTitle(title);
  IV->GetYaxis()->SetTitle("Crosstalk Threshold [e]");
  IV->GetYaxis()->SetRangeUser(0,25000);
  IV->GetXaxis()->SetTitle("Bias [V]");
  IV->SetMarkerStyle(20);
  IV->SetMarkerSize(1);
  IV->Draw("ALP");
  // TGraph *IVB = new TGraph(ccB,&vB[0],&aB[0]);
  // for (int i=0;i<IVB->GetN();i++) IVB->GetY()[i] /= 1e-6; 
  // IVB->SetMarkerStyle(20);
  // IVB->SetMarkerSize(1);
  // IVB->SetMarkerColor(2);
  // IVB->Draw("sameLP");

  TLegend *leg2 = new TLegend(0.15,0.87,0.25,0.73);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.033);
  leg2->AddEntry(IV,"25x100 "+type+", inject up, LIN 1000e");
  // leg2->AddEntry(IVB,"wafer");
  leg2->Draw("SAME");   

  if(PRINT=="Yes") {
    TString plotName="plots_to_eos/"+title+"_CrossHV.png";
    c3->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }

  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           

  app.Run(true);
  return 0;
}
