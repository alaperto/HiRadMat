
The HiRadMat project is here online:

https://gitlab.cern.ch/alaperto/HiRadMat

Please, create a directory and download the git package:

mkdir HiRadMat_git
cd HiRadMat_git/
setupATLAS
lsetup git
git clone ssh://git@gitlab.cern.ch:7999/alaperto/HiRadMat.git

Then enter into the folder:

cd HiRadMat

Now you're ready! Read the README file and enjoy the project!

_____________________________________________________________________________
Welcome to HiRadMat experiment data analysis framework!

0) The original .root files with plots are in the linux PC at CERN
   You may connect to it remotely, if it is On and connected to ethernet, doing:
      ssh -X testbeam@pixeldet
   There is no need of those plots, unless one wants to look for further plots...
   In case you switch off the PC and then on again, you need to enable GPIB, doing:
      ssh root@localhost
        gpib_config
        chmod 777 /dev/gpib*
      exit
   And then you are ready to control LV/HV supplies (be aware of Logging with Keithley HV)

1) To unzip the folders, do:
   source unzip_folders

2) The macro .C extracted from useful plots are in:
   2D_macros (all 2D plots: Analog, Digital, Treshold, Source scans)
   IV_macros (IV curves)
   Sources (Sources scans with timing information)

3) Tables containing values extracted from .C macros are in:
   tables (I: IV, A: Analog, D: Digital, S: Source, T: Treshold HV Off, O: HV On)

   Naming convention examples:
   A_1_17_1503.C is Analog Scan from Module 1 done on 17 July at 15:03
   D_2_18_0826.C is Digital Scan from Module 2 done on 18 July at 08:26

4) Tables are already generated and are in tables directory.
   To generate tables again (if needed), source the corresponding scripts:
   source generate_2D_tables
   source generate_IV_tables

5) The list of the available files are in:
   lists/list_2D_macros_* (with * = A/D/S1/S2/T1/T2/O1/O2)
   lists/list_IV_macros_* (with * = 1/2)
   lists/list_2D_macros_*b (with * = T1/T2/O1/O2) "b" lists are meant for Noise-Dose correlation plots

6) Definitions of plotting a single 2D plot are in:
   Plotting_2D.cxx

7) To generate a single 2D plot, change the macro_name variable:
   TString macro_name = "S_1_17_1849.C";
   save and then:
   source qualsiasi_launch Plotting_2D.cxx

General:
   Macros are named like *.cxx
   To execute them, just do
   source qualsiasi_launch name_of_the_macro.cxx

8) Definitions of plotting a list of 2D plots are in:
   Plotting_2D_list.cxx

9) To generate a GIF of 2D plots, change the which_list variable:
   TString which_list = "_S";
   and the module variable:
   TString module = "1";
   and set "Yes" to the save_GIF variable:
   TString save_GIF = "Yes";
   save and then exectue

10) To show separate 2D plots from a list, change the which_list variable:
    TString which_list = "_S";
    and the module variable:
    TString module = "1";
    and set "Yes" to the separate_plots variable:
    TString separate_plots = "Yes";
    save and then execute

11) Similarly for the IV scans

12) To launch projections, select proper configurations in:
    Fitting_2D_list.cxx
    save and then execute

13) The analysis of LV currents is in LogLV_analysis folder:
    cd LogLV_analysis
    It is possible to modify the plot style and options (range of dates):
    emacs Plotting_LV.cxx
    save and then execute

14) The analysis of Noise-Damage Correlation is done with file Correlating_2D_list.cxx:
    to modify the plot style and list of scans:
    emacs Correlating_2D_list.cxx
    save and then execute

15) The fitting of raw Data (position and width of the beam pulses) is done with:
    emacs X_and_Y_raw_data.cxx
    save and then execute

EXTRA: now Correlating_2D_list.cxx is using a modified list of fitted parameters:
       rawData/Mean_Sigma_17_X.txt
       rawData/Mean_Sigma_17_Y.txt
       they have 0 RotColl protons (in Correlating_2D_list.cxx)
                 and 0.5 width for "narrow beam" 1 and 12 bunches shots

EXTRA 2018: Correlating_2D_HRM2.cxx is used for 2018 TB analysis.

16) The fitting of Decay curves from Source scans timing measurements is done with:
    emacs Decaying_2d.cxx
    save and then execute

17) The 2018 HRM test beam data is in the HRM2 folder

18) Plotting_IV_HRM2.cxx and "_list" are used to plot different IV curves from HRM2 data-taking. Modify with:
    emacs Plotting_IV_list_HRM2.cxx
    save and then execute

19) Volume_HRM2.cxx and is used to plot Leakage Current @80V from HRM2 data:
    emacs Volume_HRM2.cxx
    save and then execute

_________________________________________________________________________________
Some examples on how to work with this gitlab project!

To remove something:

git rm -r something

Then modify something:

emacs something 

Then add the modified files to the commit:

git add something

Then commit (offline) and push online!

git commit -m "Some message here"
git push

#git pw is "fb44.." in this case

Enjoy!!!

