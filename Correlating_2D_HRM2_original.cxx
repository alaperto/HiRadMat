#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TRandom3.h> 
#include <TF1.h> 
#include <TF2.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include "TProfile.h"
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  TString version = "_v10"; //HRM2
  TString which_list = "_OS"; //"_T" for threshold off, "_O" for threshold on
                              //"S" for SIGMA, "M" for MEAN
  TString module = "3"; //"3" for 2018 Planar module
  TString only_useful = "x"; //"" = all, "x" = yes
  which_list+=module+only_useful;
  TString list_name= "lists/list_2D_macros"+which_list;
  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  int qq = 200;
  TCanvas *plot_mc_data[qq];
  TH2F *Occup_0_0_MA[qq]; //Noise scans
  TProfile *Prof[qq]; //Dose vs Noise
  TProfile *ProfT = new TProfile("Profile Tot: DvsD","Profile Tot: DvsD",100,0,15e9,0,400); //Dose vs Noise Total (All points from all scans)
  TCanvas *ProfC= new TCanvas("PT_Canvas","PT_Canvas",200,10,700,500);//Its canvas
  int aa[40000]; double bb[40000], cc[40000], dd[40000], ff[400000];
  int a=0,b=0,c=0,d=0,e=0,f=1;
  TString plotName="";
  TString macro_name = "", f_name = ""; int m = 0, start = 0;
  TString do_correlation = "Yes"; //"Yes" show the correlation plots, "whatever" do not show
  TString do_differential = "Yeso"; //"Yes" subtract time "0" distribution, "whatever" do classic
  TString Generate_new = "Yeso";//"Yes" generate new beam profiles, or use saved
  TString Generate_show = "Yeso";//"Yes" show generated beam profiles, or not
  TString SAVE = "Yeso";//"Yes" save or not
  TString separate_plots = "Yeso"; //"Yes" separate canvas, "whatever" superimpose on the same canvas
  TString save_GIF="Yeso"; //"Yes" save animated GIF, "whatever" not save
  if(separate_plots=="Yes") save_GIF="No";
  // TString do_logX = "Yeso"; //"Yes" do log X axis & move range, "whatever" keep linear X axis and usual range

  cout<<"______________________________________________________________"<<endl;
  cout<<"______WELCOME____TO___THE____DAMAGE-DOSE____CORRELATING_______"<<endl;
  cout<<"__________________Beam_Profile_Simulation_____________________"<<endl<<endl;

  //Dose simulation  
  TGraph *graph[qq];
  TCanvas *graphC= new TCanvas("PC_Canvas","PC_Canvas",200,10,700,500);//Its ca.
  TF2 *f2[qq];
  int pix = 26880, step = 0, steP = 0;
  int how_many_plots = 4; //T1 4, T2 3
  int max = pix * how_many_plots;
  Double_t dam[pix], dos[pix], damT[pix*11], dosT[pix*11];
  TCanvas *plot_dose[qq];
  TCanvas *plot_extra[qq];
  TCanvas *can[qq];
  TCanvas *canP[qq];
  TH2D *h1[qq], *h2[qq];
  TString dose_name= "", histo_dose_name= "", correl_name= "", noise_name= "";
  double X[qq], Y[qq], j[qq], k[qq], l[qq], w[qq], q[qq]; int content[qq], be[qq]; int n_bunches = -1;
  double x[qq], y[qq], sx[qq], sy[qq];
  int last = 10;//10 in 2018... 32 in 2017 //last available simulated beam

  // SIGMA X and Y from elog
  w[1]=1.0;  q[1]=1.0;  be[1]=1;//0b  22:00  Scan 22:10
  w[2]=1.81; q[2]=1.63; be[2]=1;//1b  23:06  Scan 23:23
  w[3]=1.68; q[3]=1.52; be[3]=1;//1b  23:26  Scan NO
  w[4]=1.67; q[4]=1.52; be[4]=1;//4b  23:50  Scan 23:55 (1b + 4b)
  w[5]=2.10; q[5]=1.81; be[5]=1;//12  00:33  Scan 00:38
  w[6]=2.08; q[6]=1.79; be[6]=1;//24  01:44  Scan 02:04
  w[7]=2.18; q[7]=1.85; be[7]=1;//36  02:08  Scan 02:19
  w[8]=2.35; q[8]=1.97; be[8]=1;//72  03:45  Scan 03:56
  w[9]=2.30; q[9]=1.95; be[9]=1;//... 04:00  Scan 04:28 -->not av. interpolated
  w[10]=2.24; q[10]=1.90; be[10]=1;//288 (first) 04:40 Scan 04:54

  w[11]=1.70; q[11]=1.28; be[11]=1;
  w[12]=1.79; q[12]=1.29; be[12]=1;
  w[13]=2.44; q[13]=1.93; be[13]=1;
  w[14]=0.18; q[14]=0.20; be[14]=1;
  w[15]=0.20; q[15]=0.18; be[15]=1;
  w[16]=0.19; q[16]=0.19; be[16]=1;
  w[17]=0.18; q[17]=0.19; be[17]=1;
  w[18]=0.19; q[18]=0.17; be[18]=1;
  w[19]=0.19; q[19]=0.19; be[19]=1;
  w[20]=0.22; q[20]=0.18; be[20]=1;
  w[21]=0.30; q[21]=0.27; be[21]=1;
  w[22]=0.25; q[22]=0.25; be[22]=23;
  w[23]=0.26; q[23]=0.23; be[23]=5;
  w[24]=0.26; q[24]=0.24; be[24]=25;
  w[25]=0.28; q[25]=0.23; be[25]=15;
  w[26]=0.39; q[26]=0.28; be[26]=1;
  w[27]=0.28; q[27]=0.22; be[27]=20;
  w[28]=0.34; q[28]=0.31; be[28]=1;
  w[29]=0.27; q[29]=0.22; be[29]=21;
  w[30]=0.34; q[30]=0.31; be[30]=1;
  w[31]=0.28; q[31]=0.25; be[31]=19;
  w[32]=0.27; q[32]=0.24; be[32]=24;

  content[1]=1e7; l[1]=0;
  content[2]=1e7; l[2]=1;
  content[3]=1e7; l[3]=1;
  content[4]=1e7; l[4]=4;
  content[5]=1e7; l[5]=12;
  content[6]=1e7; l[6]=24;
  content[7]=1e6; l[7]=36;
  content[8]=1e6; l[8]=72;
  content[9]=1e6; l[9]=144;
  content[10]=1e6; l[10]=288;

  content[11]=5e5;
  content[12]=5e5;
  content[13]=5e5;
  content[14]=1e7;
  content[15]=1e7;
  content[16]=1e7;
  content[17]=1e7;
  content[18]=1e7;
  content[19]=1e6;
  content[20]=1e7;
  content[21]=1e7;
  content[22]=1e7;
  content[23]=1e7;
  content[24]=1e7;
  content[25]=1e7;
  content[26]=1e7;
  content[27]=1e7;
  content[28]=1e7;
  content[29]=1e7;
  content[30]=1e6;
  content[31]=1e7;
  content[32]=1e7;

 X[1]=56; Y[1]=75;
 X[2]=56; Y[2]=75;
 X[3]=56; Y[3]=75;//...Extrapolated
 X[4]=56; Y[4]=75;//Measured...
 X[5]=56; Y[5]=74;
 X[6]=56; Y[6]=83;
 X[7]=56; Y[7]=81;
 X[8]=56; Y[8]=85;
 X[9]=56; Y[9]=90;
 X[10]=56; Y[10]=95;

  // ATTENTION HERE.... THE NEW PARAMETERS ARE COMING FROM raw data FITTING!!! Here!


  TString simu_name = "lists/list_Simulated_Beams_HRM2.root";
  if(Generate_new=="Yes")
    {

      TFile simu (simu_name,"RECREATE");

      for(int g = 2; g <= last; g++){ //nothing to generate for shot 0
	dose_name = "Canvas_Dose_n_"; dose_name+=g;
	histo_dose_name = "Histo_h1_Dose_n_"; histo_dose_name+=g;
	f_name = "f2["+g; f_name+="]";
	f2[g] = new TF2(f_name,"[0]*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4])",-0.5,79.5,-0.5,335.5);
	n_bunches = content[g]*l[g];//1e6(*5e4)=5e10
	cout<<"Shot g: "<<g<<"  bunches: "<<l[g]<<endl;
	// X[g] = 3.90 * ( y[g] - 3.68) + 62.5; //Calibrated OK
	// Y[g] = -21.03 * ( x[g] + 2.61) + 163; //Calibrated OK

	// X[g] = 55.5; //From Source Triple 288  Fit
	// Y[g] = 95; //From Source Triple 288 Fit

	f2[g]->SetParameters(1,X[g],q[g]/0.250,Y[g],w[g]/0.050);
	//Sigma invert, since it is inverted, and divide per Pixel (in Pixel)
	
	cout<<" g loop: "<<g<<"  x_beam: "<<X[g]<<"  y_beam: "<<Y[g]<<"  X_sig: "<<q[g]/0.250<<"  Y_sig: "<<w[g]/0.050<<endl;

	h1[g] = new TH2D(histo_dose_name,histo_dose_name,80,-0.5,79.5,336,-0.5,335.5);
	h1[g]->FillRandom(f_name,n_bunches); 
	for(int p = 1; p < pix; p++){
	  h1[g]->SetBinContent(p,h1[g]->GetBinContent(p)*5e10/content[g]);//*5e4);//(1e6)*5e4=5e10//(1e7)*5e3=5e10
	}	
	// cout<<" g = "<<g<<"  and be[g] = "<<be[g]<<endl;
	if(be[g]>1) h1[g]->Add(h1[g],be[g]-1);
	// for(int cu = 0; cu < be[g]; cu++){}
	simu.cd();
	h1[g]->Write(); //Save single beam plot "h1"
	
	if(g>2)  h1[g]->Add(h1[g-1],1);
	simu.cd(); //Save integrated dose beam plots "h2"
	histo_dose_name = "Histo_h2_Dose_n_"; histo_dose_name+=g;
	h1[g]->SetName(histo_dose_name);//+"_integr");
	h1[g]->Write();
	// plot_dose[g] = new TCanvas(dose_name,dose_name,700,550);
	// plot_dose[g]->cd();
	// h1[g]->Draw("COLZ");
	// plot_dose[g]->Update();
      }
      
      simu.Close();
    }
  
  
  TFile *simu_file = new TFile(simu_name);
  for(int g = 2; g <= last; g++){
    dose_name = "Canvas_Dose_n_"; dose_name+=g;
    histo_dose_name = "Histo_h2_Dose_n_"; histo_dose_name+=g;// histo_dose_name+="_integr";
    h2[g]= (TH2D*) simu_file->Get(histo_dose_name);
    if(Generate_show=="Yes"){
      plot_dose[g] = new TCanvas(dose_name,dose_name,700,550);
      plot_dose[g]->cd();
      h2[g]->GetXaxis()->SetTitle("Column");
      h2[g]->GetYaxis()->SetTitle("Row");  
      h2[g]->Draw("COLZ");
      plot_dose[g]->Update();
      if(SAVE=="Yes") plotName="saved_plots/"+dose_name+version+".png";
      if(SAVE=="Yes") plot_dose[g]->Print(plotName);
    }
  }
  
  cout<<"______________________________________________________________"<<endl;
  cout<<"________PREPARATION___FOR___DAMAGE-DOSE____CORRELATION________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;

  ifstream input2(list_name); 
  // if(4>8)
  while(!input2.eof()){
    m++;
    input2 >> macro_name;
    cout<<" Shot m: "<<m<<" macro name: "<<macro_name<<endl;//" which list: "<<which_list<<endl;
    if(m<=last)
      if(macro_name!=""){
	    
	Occup_0_0_MA[m] = new TH2F("Plot_"+macro_name,"Plot_"+macro_name,80,-0.5,79.5,336,-0.5,335.5);

	a=0,b=0,c=0,d=0,e=0,f=1;

	list_name = "tables/"+macro_name+".txt";
	ifstream input3(list_name); 
	while(!input3.eof()){
	  input3 >> ff[f];
	  if(d==0)
	    if(ff[2]!=0){
	      if(f%3==2) {aa[f/3+1]=ff[f]; e=f/3+1;
		if(ff[f]==0) d=1;}
	      if(f%3==0){bb[f/3]=ff[f];}
	    }
	    else{
	      if(f%4==3) {aa[f/4]=ff[f]; e=f/4;
		if(ff[f]==0) d=1;}
	      if(f%4==0){bb[f/4]=ff[f];}
	    }
	  f++;
	}
    
	//for debug purpose
	//for(int x = 1; x < 26; x++)
	// for(int x = 1; x < e; x++)
	//   cout<<x<<"   "<<aa[x]<<"   "<<bb[x]<<<<endl;
    
	for(int x = 1; x < e; x++)
	  {
	    // if(bb[x]==1.053496) bb[x]=0;
	    Occup_0_0_MA[m]->SetBinContent(aa[x],bb[x]);
	  }

	Occup_0_0_MA[m]->SetTitle(macro_name);//Change Title for every file!
	Occup_0_0_MA[m]->GetXaxis()->SetTitle("Column");
	Occup_0_0_MA[m]->GetYaxis()->SetTitle("Row");  
	Occup_0_0_MA[m]->SetMinimum(0);
	Occup_0_0_MA[m]->SetMaximum(250);
	if(which_list=="_OS3x") Occup_0_0_MA[m]->SetMaximum(250);

	if(m>start)//>start+1
	  {
	      
	    noise_name = "Canvas_Noise_n_"; noise_name+=m;
	    noise_name+="_"; noise_name+=macro_name;
	      
	    if(separate_plots=="Yes"){ 
	      plot_mc_data[m] = new TCanvas(noise_name,noise_name,700,550);
	      Occup_0_0_MA[m]->Draw("COLZ");
	      if(SAVE=="Yes") plotName="saved_plots/"+noise_name+version+".png";
	      if(SAVE=="Yes") plot_mc_data[m]->Print(plotName);
	    }
	    else  {
	      if(m==1) {
		plot_mc_data[1] = new TCanvas(noise_name,noise_name,700,550);
		Occup_0_0_MA[1]->Draw("COLZ");
	      }
	      else{
		plot_mc_data[1]->cd();
		Occup_0_0_MA[1]->SetTitle(macro_name);//Change Title for every!
		Occup_0_0_MA[m]->Draw("COLZSAME");
	      }
      
	      if(save_GIF=="Yes"){
		plotName="saved_plots/Animated_HRM2"+version+".gif";
		plot_mc_data[1]->Print(plotName+"+150");
	      }
	    }
    
	    // m--;//always together
	    if(m>1)
	      if(do_correlation=="Yes"){

		if(m>1&&do_differential=="Yes") Occup_0_0_MA[m]->Add(Occup_0_0_MA[1],-1);//subtract 2D scan at shot 0


		Prof[m] = new TProfile("Profile: DvsD","Profile: DvsD",100,0,15e9,0,400);
		Prof[m]->SetTitle("Profile: Damage vs Dose "+macro_name);
		Prof[m]->GetXaxis()->SetTitle("Fluence [protons/pixel]");
		Prof[m]->GetYaxis()->SetTitle("Noise [e^{-}]");
		Prof[m]->GetYaxis()->SetTitleOffset(1.2);
		Prof[m]->GetXaxis()->SetTitleOffset(1.2);
		Prof[m]->GetYaxis()->SetRangeUser(0,400);
		Prof[m]->SetMarkerStyle(20);
		steP++;
		if(steP==10) steP = 1;
		Prof[m]->SetMarkerColor(steP);
		if(steP==5) Prof[m]->SetMarkerColor(kOrange-3);

		//one vector for every scan
		for(int p = 0; p < pix; p++){
		  // h2[m]->SetBinContent(p,h2[m]->GetBinContent(p));//*5e4);//(1e6)*5e4=5e10//(1e7)*5e3=5e10
		  dos[p]=h2[m]->GetBinContent(p);
		  dam[p]=Occup_0_0_MA[m]->GetBinContent(p);
		  if(dam[p]<=0) {dam[p]=0; dos[p]=0;}
		  else
		    Prof[m]->Fill(h2[m]->GetBinContent(p),Occup_0_0_MA[m]->GetBinContent(p),1);

		  //one vector for ALL scans
		  dosT[p*(m-1)]=h2[m]->GetBinContent(p);
		  damT[p*(m-1)]=Occup_0_0_MA[m]->GetBinContent(p);
		  if(damT[p*(m-1)]<=0) {damT[p*(m-1)]=0; dosT[p*(m-1)]=0;}
		  else
		    ProfT->Fill(h2[m]->GetBinContent(p),Occup_0_0_MA[m]->GetBinContent(p),1);
		}

		// //Draw, separate plots for points out of the trends ")"
		// if(which_list=="_OS3x") 
		//   {
		//     dose_name = "Canvas_Extra_n_"; dose_name+=m; dose_name+="_"; dose_name+=macro_name;
		//     histo_dose_name = "Histo_T1_extra_n_"; histo_dose_name+=m;
		//     plot_extra[m+100] = new TCanvas(dose_name,dose_name,700,550);
		//     h2[m+100] = new TH2D(histo_dose_name,histo_dose_name,80,-0.5,79.5,336,-0.5,335.5);
		//     cout<<"New canvas for m: "<<m<<endl;
		//   	if(m==32)
		// 	  if(dos[p] > 5e9 && dos[p] < 2e11){
		// 	    if(dam[p] > 300 ) h2[m+100]->SetBinContent(p,1111);
		// 	    if(dam[p] < 300 ) h2[m+100]->SetBinContent(p,555);
		// 	  }
		      
		//     h2[m+100]->Draw("COLZ");
		//     plotName="saved_plots/"+dose_name+version+".png";
		//     plot_extra[m+100]->Print(plotName);
		//   }


		graph[m] = new TGraph(pix,dos,dam);//0,10e10,0,10e10 //"Hello","Hello"
		graph[m]->SetTitle("Damage vs Dose "+macro_name);
		graph[m]->GetXaxis()->SetTitle("Fluence [protons/pixel]");
		graph[m]->GetYaxis()->SetTitle("Noise [e^{-}]");
		graph[m]->GetYaxis()->SetTitleOffset(1.2);
		graph[m]->GetXaxis()->SetTitleOffset(1.2);
		graph[m]->GetYaxis()->SetRangeUser(0,400);
		graph[m]->SetMarkerStyle(20);
		graph[m]->SetMarkerSize(0.2);
		step++;
		if(step==10) step = 1;
		graph[m]->SetMarkerColor(step);
		if(step==5) graph[m]->SetMarkerColor(kOrange-3);


		correl_name = "Canvas_Correl_n_"; correl_name+=m;
		correl_name+="_"; correl_name+=macro_name;
		can[m] = new TCanvas(correl_name,correl_name,200,10,700,500);
		can[m]->cd();
		graph[m]->Draw("AP");
		if(SAVE=="Yes") plotName="saved_plots/"+correl_name+version+".png";
		if(SAVE=="Yes") can[m]->Print(plotName);
		cout<<"  Correl. drawn for Shot m: "<<m<<" --> Step: "<<step<<endl;


		if(m==10){
		  //TOTAL Graph
		  graphC->cd();
		  TGraph *graphT = new TGraph(pix*11,dosT,damT);//Total all points in 1 vect; //Total scatter plot (All points from all plots)
		  graphT->SetTitle("Damage vs Dose "+macro_name);
		  graphT->GetXaxis()->SetTitle("Fluence [protons/pixel]");
		  graphT->GetYaxis()->SetTitle("Noise [e^{-}]");
		  graphT->GetYaxis()->SetTitleOffset(1.2);
		  graphT->GetXaxis()->SetTitleOffset(1.2);
		  graphT->GetYaxis()->SetRangeUser(0,400);
		  graphT->SetMarkerStyle(20);
		  graphT->SetMarkerSize(0.2);
		  // graphT->SetMarkerColor(step);
		  // if(step==5) graphT->SetMarkerColor(kOrange-3);
		  graphT->Draw("AP");
		  if(SAVE=="Yes") plotName="saved_plots/TOT_"+correl_name+version+".png";
		  if(SAVE=="Yes") graphC->Print(plotName);
		  cout<<"  TOT Correl. drawn for Shot m: "<<m<<" --> Step: "<<step<<endl;
		}

		//Single Profile
		canP[m] = new TCanvas("P_"+correl_name,"P_"+correl_name,200,10,700,500);
		canP[m]->cd();
		Prof[m]->Draw("LP");
		if(SAVE=="Yes") plotName="saved_plots/P_"+correl_name+version+".png";
		if(SAVE=="Yes") canP[m]->Print(plotName);
		cout<<"  Profile Correl. drawn for Shot m: "<<m<<" --> Step: "<<step<<endl;

		if(m==10){
		  //TOTAL Profile
		  ProfC->cd();
		  ProfT->SetTitle("Profile: Damage vs Dose "+macro_name);
		  ProfT->GetXaxis()->SetTitle("Fluence [protons/pixel]");
		  ProfT->GetYaxis()->SetTitle("Noise [e^{-}]");
		  ProfT->GetYaxis()->SetTitleOffset(1.2);
		  ProfT->GetXaxis()->SetTitleOffset(1.2);
		  ProfT->GetYaxis()->SetRangeUser(0,400);
		  ProfT->SetMarkerStyle(20);
		  // ProfT->SetMarkerColor(steP);
		  // if(steP==5) ProfT->SetMarkerColor(kOrange-3);
		  ProfT->Draw("LP");
		  if(SAVE=="Yes") plotName="saved_plots/PT_"+correl_name+version+".png";
		  if(SAVE=="Yes") ProfC->Print(plotName);
		  cout<<"  Profile TOT Correl. drawn for Shot m: "<<m<<" --> Step: "<<step<<endl;
		}
	      
	      }//always together	    
	    // m++;
	  
	  }
	  
      }

  }
    
  gStyle->SetOptTitle(0);
 
 
 const char *IVbunch[11] = {"","","1 b.","1 b.","4 b.","12 b.","24 b.","36 b.","72 b.","144 b.","288 b."};
  TLegend *leg2 = new TLegend(0.13,0.88,0.25,0.6);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.033);

 if(which_list=="_OS3x"){
    TCanvas *final_Corr = new TCanvas("Canvas_final_Corr","Canvas_final_Corr",200,10,700,500);//1000?
    // final_Corr->SetLogx();
    graph[10]->GetXaxis()->SetRangeUser(0,15e9);
    graph[10]->GetYaxis()->SetRangeUser(0,400);
    graph[10]->Draw("AP");
    graph[9]->Draw("sameP");
    graph[8]->Draw("sameP");
    graph[7]->Draw("sameP");
    graph[6]->Draw("sameP");
    graph[5]->Draw("sameP");
    graph[4]->Draw("sameP");
    graph[3]->Draw("sameP");
    graph[2]->Draw("sameP");
    for(int m=2; m<11; m++)
      {	leg2->AddEntry(Prof[m],IVbunch[m],"p");
	if(m==10) leg2->Draw("SAME");    
      }
    cout<<"   Drawn together all Correl 2-10"<<endl;
    // if(SAVE=="Yes")
 plotName="saved_plots/Canvas_Final_Corr"+version+".png";
    // if(SAVE=="Yes")
 final_Corr->Print(plotName);    

    TCanvas *final_Prof = new TCanvas("Canvas_final_Prof","Canvas_final_Prof",200,10,700,500);//1000?
    // final_Prof->SetLogx();
    Prof[10]->GetXaxis()->SetRangeUser(0,15e9);
    Prof[10]->GetYaxis()->SetRangeUser(0,400);
    Prof[10]->Draw("LP");
    Prof[9]->Draw("sameP");
    Prof[8]->Draw("sameP");
    Prof[7]->Draw("sameP");
    Prof[6]->Draw("sameP");
    Prof[5]->Draw("sameP");
    Prof[4]->Draw("sameP");
    Prof[3]->Draw("sameP");
    Prof[2]->Draw("sameP");
    leg2->Draw("SAME");    
    cout<<"   Drawn together all Profile 2-10"<<endl;
    // if(SAVE=="Yes")
      plotName="saved_plots/Canvas_Final_Prof"+version+".png";
    // if(SAVE=="Yes") 
      final_Prof->Print(plotName);    


  }

  // if(save_GIF=="Yes") cout<<"GIF Saved in: "<<plotName<<endl;
  
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           
  app.Run(true);
  return 0;
}

