#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  TString PRINT = "Yes";
  TString fol = "./RD53_IV/";

  //TString M_name = "F02_91_02_25_1E";
  // TString M_name = "F02_02_50_1E_01";
  // TString M_name = "F02_02_50_1E_03";

  //TString M_name = "F02_03_01_25_1E";

   // TString M_name = "IFAE";

  // TString M_name = "F03_30_25x100-2E_A1.2";
  //TString M_name = "F03_30_25x100-1E_D5.3";//--> high 250uA;
  //TString M_name = "F03_30_25x100-1E_D9.5";//Probe
  // TString M_name = "F03_30_25x100-1E_D11.3";
  // TString M_name = "F03_30_50x50-1E_E3.3";
  
  //TString M_name = "F02_05_25x100-1E_03";

  // TString M_name = "F03_30_25x100-2E_A8.5";
  //TString M_name = "F03_30_25x100-2E_B4.4";
  //TString M_name = "F03_30_25x100-2E_C4.3";

  TString M_name = "F02_02_50_1E_01";
  
  TString invert = "Yeso";
  //TString compare_b = "_mod", compare_w = "_waf", scale = "yes";
  //TString compare_b = "_irr-20", compare_w = "_irr-25", scale = "yes";
  TString compare_b = "", compare_w = "", scale = "yes";
  //ifstream input3(fol+M_name+"_high.dat"); //bare module 0-50 to 250uA

   TString M_name2=M_name;
   // M_name2 = "F02_02_50_1E_01";

   //Power measurement
   M_name  = "M2_25C_268V";
   M_name2 = "M3_25C_107V";

  int ccc = 400;
  TString u[ccc];
  Float_t v[ccc], a[ccc];
  int cc=0;

  cout<<"Name 1 "<<fol+M_name+compare_b<<".dat"<<endl;
  cout<<"Name 1 "<<fol+M_name2+compare_w<<".dat"<<endl;

 
  ifstream input3(fol+M_name+compare_b+".dat"); //bare module 0 5 10-20-100
  while(!input3.eof()){
    input3 >>v[cc]>>a[cc];
    cout<<cc<<"  v  "<<v[cc]<<"  a  "<<a[cc]<<endl;
    cc++;
  }

  cc--;

  TString uB[ccc];
  Float_t vB[ccc], aB[ccc];
  int ccB=0;
 
  //How to extract numbers from table
  //Copy row, paste as column (special paste)
  //Paste in emacs file
  //Change commas in dots --> cat step0.txt | sed 's/,/\./g' > stepa.txt
  //Done 

  ifstream input3B(fol+M_name2+compare_w+".dat"); //from wafer 0-80/50
  while(!input3B.eof()){
    input3B >>vB[ccB]>>aB[ccB];
    cout<<ccB<<"  vB  "<<vB[ccB]<<"  aB  "<<aB[ccB]<<endl;
    ccB++;
  }

  ccB--;

  TCanvas *c3 = new TCanvas("c3","c3",20,10,800,500);
  c3->cd();  
  TGraph *IV = new TGraph(cc,&v[0],&a[0]);
  if(scale=="yes") for (int i=0;i<IV->GetN();i++) IV->GetY()[i] /= 1e-6; 
  IV->SetTitle(M_name);
  IV->GetYaxis()->SetTitle("Current [#mu A]");
  IV->GetXaxis()->SetTitle("Volt [V]");
  IV->SetMarkerStyle(20);
  IV->SetMarkerSize(1);
  IV->Draw("ALP");
  //IV->GetYaxis()->SetRangeUser(-0.5,11.5);
  IV->GetYaxis()->SetRangeUser(0,210.5);
  TGraph *IVB = new TGraph(ccB,&vB[0],&aB[0]);
  if(scale=="yes") for (int i=0;i<IVB->GetN();i++) IVB->GetY()[i] /= 1e-6; 
  IVB->SetTitle(M_name2);
  IVB->GetYaxis()->SetTitle("Current [#mu A]");
  IVB->GetXaxis()->SetTitle("Volt [V]");
  IVB->SetMarkerStyle(20);
  IVB->SetMarkerSize(1);
  IVB->SetMarkerColor(2);
  IVB->Draw("sameLP");

  if(invert=="Yes"){
    IVB->Draw("ALP");
    IV->Draw("sameLP");
  }

  TLegend *leg2 = new TLegend(0.15,0.87,0.25,0.73);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.033);
  if(scale=="yes"){
    //leg2->AddEntry(IV,"25x100-1E at -25#circC");
    //leg2->AddEntry(IVB,"50x50 at -25#circC");
    leg2->AddEntry(IV,"25x100 #mum^{2}");
    leg2->AddEntry(IVB,"50x50 #mum^{2}");
  }
  else{ leg2->AddEntry(IV,M_name+compare_b);
    leg2->AddEntry(IVB,M_name2+compare_w);
  }
  leg2->Draw("SAME");   

  if(PRINT=="Yes") {
    TString plotName="plots_to_eos/"+M_name+"_IV.png";
    c3->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }

  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           
  TCanvas *c4 = new TCanvas("c4","c4",20,10,800,500);
  c4->cd();  

  Float_t IVcor[ccc], IVcorB[ccc];
  Float_t p[ccc], pB[ccc];

  //no annealing correction, just scaled to uA/cm2
  //for (int i=0;i<IV->GetN();i++) IVcor[i] = a[i]/(1e-3*1.92); 
  //for (int i=0;i<IVB->GetN();i++) IVcorB[i] = aB[i]/(1e-3*1.92); 

  //M2 25x100 CYRIC
  //90d 3.6
  //60d 3.4
  //from 90d to 0d *3.6, from 0d to 7d /2.5

  //M3 50x50 Birmingam
  //2d 2.1
  //7d 2.5
  //from 2d to 0d *2.1, from 0d to 7d /2.5

  //Correct IV of modules (left to muA/cm2)
  for (int i=0;i<IV->GetN();i++) IVcor[i] = a[i]/(1e-6*1.92)*3.6/2.5; //90d-->0d-->7d --> JAP M2 
  for (int i=0;i<IVB->GetN();i++) IVcorB[i] = aB[i]/(1e-6*1.92)*2.1/2.5;//2d-->0d-->7d --> UK M3 

  //Correct IV of modules (scaled to uA/cm2)
  //for (int i=0;i<IV->GetN();i++) IVcor[i] = a[i]/(1e-3*1.92)*3.6/2.5; //90d-->0d-->7d --> JAP M2 
  //for (int i=0;i<IVB->GetN();i++) IVcorB[i] = aB[i]/(1e-3*1.92)*2.1/2.5;//2d-->0d-->7d --> UK M3 

  //Estimate Power dissipation from corrected IV of modules (already scaled to uA/cm2)
  for (int i=0;i<IV->GetN();i++) p[i] = IVcor[i] * v[i]; 
  for (int i=0;i<IVB->GetN();i++) pB[i] = IVcorB[i] * vB[i]; 

  TGraph *IVcorr = new TGraph(cc,&v[0],&IVcor[0]);
  IVcorr->SetTitle("25x100-1E vs 50x50 IV at -25#circC");
  //IVcorr->GetYaxis()->SetTitle("Leakage current [mA/cm^{2}]");
  IVcorr->GetYaxis()->SetTitle("Leakage current [#muA/cm^{2}]");
  IVcorr->GetXaxis()->SetTitle("Bias voltage [V]");
  IVcorr->SetMarkerStyle(20);
  IVcorr->SetMarkerSize(1);
  IVcorr->Draw("ALP");
  IVcorr->GetYaxis()->SetRangeUser(0,0.2);
  TGraph *IVcorrB = new TGraph(ccB,&vB[0],&IVcorB[0]);
  IVcorrB->SetMarkerStyle(20);
  IVcorrB->SetMarkerSize(1);
  IVcorrB->SetMarkerColor(2);
  IVcorrB->Draw("sameLP");
  leg2->Draw("same");
  if(PRINT=="Yes") {
    TString plotName="plots_to_eos/25x100-1E_vs_50x50_IVcorr_@-25C.png";
    c4->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }

  TCanvas *c5 = new TCanvas("c5","c5",20,10,800,500);
  c5->cd();  

  TGraph *Pow = new TGraph(cc,&v[0],&p[0]);
  Pow->SetTitle("25x100-1E vs 50x50 Power dissipation at -25#circC");
  Pow->GetYaxis()->SetTitle("Power dissipation [mW/cm^{2}]");
  Pow->GetXaxis()->SetTitle("Bias voltage [V]");
  Pow->SetMarkerStyle(20);
  Pow->SetMarkerSize(1);
  Pow->Draw("ALP");
  Pow->GetYaxis()->SetRangeUser(0,50.1);
  TGraph *PowB = new TGraph(ccB,&vB[0],&pB[0]);
  PowB->SetMarkerStyle(20);
  PowB->SetMarkerSize(1);
  PowB->SetMarkerColor(2);
  PowB->Draw("sameLP");
  leg2->Draw("same");
  if(PRINT=="Yes") {
    TString plotName="plots_to_eos/25x100-1E_vs_50x50_Dissipation_@-25C.png";
    c5->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }

  app.Run(true);
  return 0;
}
