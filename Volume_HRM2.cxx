#include <TStyle.h>
#include <TCanvas.h>
#include <TF1.h> 
#include <TAxis.h>
#include <TGraph.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

void rescaleXaxis(TGraph *g,double scale)
{ int N = g->GetN();
 double *x = g->GetX();
 int i=0;
 while(i<N){
   x[i]=x[i]*scale;
   i=i+1;
 }
 g->GetHistogram()->Delete();
 g->SetHistogram(0);
}

int main(){
  // TString Module = "6"; //6 = 1 - 288 separated
  // TString Module = "7"; //integrated, 600 um (wrong)
  // TString Module = "8"; //integrated, 230 um sensore
  //TString Module = "9"; //integrated, 380 um (150 FE + 230 sensore)
  TString Module = "10"; //integrated, 230 um sensore NO 0-0
  //TString Module = "11"; //integrated, 380 um (150 FE + 230 sensore) NO 0-0
  TString list_name = "lists/list_Volume_"+Module+".txt";
  //Values calculated according to slides 22 (July 2018).
  //Numbers from table in Google Doc table
  TString macro_name = Module;
  gStyle->SetOptStat(0);   
   gStyle->SetOptFit(1); //Show Fit Parameters
  // gStyle->SetOptFit(0);
  gStyle->SetOptTitle(0);
  int qq =200;
  TCanvas *plot_mc_data[qq];

  TF1 *expo[qq];
  int m = 0;
  plot_mc_data[m] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);

  TGraph *graph24 = new TGraph(list_name);
  rescaleXaxis(graph24,1/0.1256);
  graph24->GetXaxis()->SetTitle("Fluence [protons/cm^{2}]");
  graph24->GetYaxis()->SetTitle("#DeltaI/V [#muA/cm^{3}]");
  graph24->GetYaxis()->SetTitleOffset(1.55);
  graph24->SetMarkerStyle(20);
  graph24->SetMarkerSize(1.2);
  plot_mc_data[m]->Update();
  graph24->Draw("AP");

  double Start, Stop;
  if(Module=="6") {Start = 0; Stop = 3e13;} //ua/cm3 vs protons (base)
  if(Module=="7"||Module=="8"||Module=="9"||Module=="10"||Module=="11") {Start = 0; Stop = 6e13;} //ua/cm3 vs protons (integrated)
  Stop = 600e12;

  expo[m] = new TF1("hey","[0]*x",Start, Stop);// v1
 
  double alfa = 0.1e-11;
  expo[m]->SetParameter(0,alfa);
  expo[m]->SetParName(0,"a");

   graph24->Fit("hey","R");
  expo[m]->Draw("SAME");
 
  double ciop0 = expo[m]->GetParameter(0);
  cout<<endl<<" "<<ciop0<<" * x "<<endl;

  app.Run(true);
  return 0;
}
