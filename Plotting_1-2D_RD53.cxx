#include <TStyle.h>
#include <TCanvas.h>
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  TString PRINT = "Yes";
  TString fol = "./RD53_1-2D_Map/";
  //TString M_name = "F03_30_25x100-2E_B44_Map.dat"; //fake
  //TString M_name = "F03_30_25x100-2E_C43_Map.dat"; //fake
  TString M_name = "F03_30_25x100-1E_D53_Map.dat"; //fake
  
  int max = 150, min = 1;

  gStyle->SetOptStat(0);
  //gStyle->SetOptFit(0);
  gStyle->SetOptTitle(0);
  
  int rowno = 192;
  int colno = 400;
  
  TH2F *h_plot[3];
  TCanvas *plot_mc_data[3];
  std::string filepath;
  filepath = fol+M_name;
  cout<<" Input file --> "<<filepath<<endl;

  std::fstream infile(filepath.c_str(), std::ios::in);
  TString plotname = "Plo";
  TString canvname = "Can";
  plot_mc_data[1] = new TCanvas(canvname,"Canvas_",700,550);//550
  h_plot[1] = new TH2F(plotname, "", colno, 0, colno, rowno, 0, rowno); 

  
  //Fill Noise Map plots	
  for (int i=0; i<rowno; i++) {
    for (int j=0; j<colno; j++) {
      double tmp;
      infile >> tmp;
      h_plot[1]->SetBinContent(j+1,i+1,tmp);	
    }
  }
  
  h_plot[1]->SetMaximum(max);
  h_plot[1]->SetMinimum(min);
  h_plot[1]->GetXaxis()->SetTitle("Column");
  h_plot[1]->GetYaxis()->SetTitle("Row");
  h_plot[1]->GetZaxis()->SetTitle("Noise");
  h_plot[1]->Draw("COLZ");
    

  plot_mc_data[2] = new TCanvas("Canvas_1D","Canvas_1D",700,550);//550
  TH1D *h1 = new TH1D("1D","1D",max,0,max);

  double bc=1;

  for(int x = 1; x < colno; x++)
    for(int y = 1; y < rowno; y++)
      {
	bc=h_plot[1]->GetBinContent(x+1,y+1);
        if(bc!=0) h1->Fill(bc);
        // if(bc<50) h_plot[1]->SetBinContent(x+1,y+1,0);
      }

  h1->Draw("HIST");

  if(PRINT=="Yes") {
    TString plotName="plots_to_eos/"+M_name+"_2D_Map.png";
    plot_mc_data[1]->Print(plotName.Data());
    plotName="plots_to_eos/"+M_name+"_1D_Map.png";
    plot_mc_data[2]->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }

  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           
      
  app.Run(true);
  return 0;
}
