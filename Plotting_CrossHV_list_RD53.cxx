#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraphErrors.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  // TString folder = "RD53_NoiseHV_all";
  TString folder = "RD53_CrossHV";
  TString list_name ="lists/list_"+folder;

  // int limit = 240;  
  // if(folder == "list_Ramps_limit_10uA") limit = 11;

  TString PRINT = "Yes";
  TString separate_plots = "Yeso"; //"Yes" separate canvas, "whatever" superimpose on the same canvas
  TString save_GIF="Yeso"; //"Yes" save animated GIF, "whatever" not save
  if(save_GIF=="Yes") separate_plots="Yeso";
  if(separate_plots=="Yes") save_GIF="No";

  int ccc = 100;

  gStyle->SetOptStat(0);   //gStyle->SetOptFit(0);
  // gStyle->SetOptTitle(0);
  int qq =200;
  TCanvas *plot_mc_data[qq];
  TGraphErrors *IV[qq];
  TString plotName="";
  TString macro_name = ""; int m = 0;

  //const char *IVbunch[8] = {"","25x100-2E (Thickness 150 #mum)","25x100-1E (Thickness 150 #mum)","25x100-1E (Thickness 130 #mum)","25x100-1E (Thickness 130 #mum)"}; //Official 3D sensor ITk week
  const char *IVbunch[10] = {"","25x100-2E (Thickness 150 #mum)","25x100-2E (Thickness 150 #mum)","25x100-2E (Thickness 150 #mum)","25x100-1E (Thickness 150 #mum)","25x100-1E (Thickness 130 #mum)","25x100-1E (Thickness 130 #mum)"};
  //TLegend *leg2 = new TLegend(0.15,0.87,0.25,0.6);
  TLegend *leg2 = new TLegend(0.5,0.15,0.8,0.3);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.033);
 
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________WELCOME____TO___THE____PLOTTING_______________"<<endl;
  cout<<"______________________________________________________________"<<endl<<endl;

  cout<<list_name<<endl;

  ifstream input2(list_name); 
  while(!input2.eof()){
    input2 >> macro_name;
    cout<<macro_name<<endl;
    m++;
    
    if(macro_name!=""){

      if(separate_plots=="Yes")
	plot_mc_data[m] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);
      else
       	if(m==1) {plot_mc_data[1] = new TCanvas("Canvas_"+macro_name,"Canvas_"+macro_name,700,550);
	  // plot_mc_data[1]->DrawFrame(-100,-1e-7,0,0);
	}
      
      TString u[ccc];
      Float_t v[ccc], ev[ccc], a[ccc], ea[ccc], sa[ccc];
      int cc=0;
      for(int o = 0;  o<ccc; o++) {v[o] = 0; a[o]=0;}
      
      ifstream input3(macro_name); 
      while(!input3.eof()){

    input3 >>v[cc]>>ev[cc]>>a[cc]>>ea[cc];
    cout<<cc<<"  v  "<<v[cc]<<"  at  "<<a[cc]<<"  +/-  "<<ea[cc]<<endl;
	cc++;

      }

      // cc--;

      // Float_t vp[ccc], ap[ccc];
      // for(int o = 0;  o<ccc; o++) {vp[o] = -1*v[o]; ap[o] = -1*a[o];}
  
      IV[m] = new TGraphErrors(cc,&v[0],&a[0],&ev[0],&ea[0]);
      IV[m]->GetYaxis()->SetTitle("Crosstalk Threshold [e]");
      IV[m]->GetXaxis()->SetTitle("Bias [V]");
      IV[m]->SetTitle("Injection: pixel Up, Reading: tuned 1000e LIN FE");
      IV[m]->GetYaxis()->SetTitleOffset(1.55);
      // IV[m]->GetYaxis()->SetTitleOffset(1.2);
      IV[m]->SetMarkerColor(m);
      if(m>=5)IV[m]->SetMarkerColor(m+1);
      if(m>=9)IV[m]->SetMarkerColor(m-8);
      IV[m]->SetMarkerStyle(20);
      IV[m]->SetMarkerSize(1);
      // for (int i=0;i<IV[m]->GetN();i++) IV[m]->GetY()[i] /= 1e-6; 
      IV[m]->GetYaxis()->SetRangeUser(0,30000);
      IV[m]->GetXaxis()->SetRangeUser(0,40);//V

     
      if(separate_plots=="Yes") IV[m]->Draw("ALP");
      else  {
      	if(m==1) IV[m]->Draw("ALP");
      	else IV[m]->Draw("sameLP");

	leg2->AddEntry(IV[m],IVbunch[m],"p");
	if(folder == "RD53_CrossHV" && m==4) leg2->Draw("SAME");    
 	
      	if(save_GIF=="Yes"){
      	  plotName="GIF/"+folder+".gif";
      	  plot_mc_data[1]->Print(plotName+"+100");
      	}
      }

    } 
  } 

    
  
  if(save_GIF=="Yes") cout<<"GIF Saved in: "<<plotName<<endl<<endl;

  if(PRINT=="Yes") {
    TString plotName="plots_to_eos/RD53A_CrossHV_list.png";
    plot_mc_data[1]->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }
    
  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           

  app.Run(true);
  return 0;
}
