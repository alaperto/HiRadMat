#include <TStyle.h>
#include <TCanvas.h>
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  TString PRINT = "Yes";
  TString fol = "./RD53_NoiseMap/";

  //TString M_name = "F02_91_02_25_1E";
  // TString M_name = "F02_02_50_1E_01";
  // TString M_name = "F02_02_50_1E_03";
  // TString M_name = "F03_30_25x100-2E_A1.2";
  // TString M_name = "F03_30_25x100-1E_D9.5";
  // TString M_name = "F03_30_25x100-1E_D11.3";
  // TString M_name = "F03_30_50x50-1E_E3.3";
  
  //TString M_name = "F03_30_25x100-1E_D5.3", volt = "2";
  TString M_name = "F03_30_25x100-2E_C4.3", volt = "10";
  
  //int max = 250, min = -100;
  int max = 150, min = 50;

  gStyle->SetOptStat(0);
   //gStyle->SetOptFit(0);
  gStyle->SetOptTitle(0);
  
  int rowno = 192;
  int colno = 400;
  
  TH2F *h_plot[3];
  TCanvas *plot_mc_data[3];
  std::string filepath;
  h_plot[2] = new TH2F("diff", "", colno, 0, colno, rowno, 0, rowno); 

  for(int e = 0; e<2; e++){

    if (e==0) filepath = fol+M_name+"_c1V.dat";
    if (e==1) filepath = fol+M_name+"_c"+volt+"V.dat";
    std::fstream infile(filepath.c_str(), std::ios::in);
    TString plotname = "ciao"+e;
    TString canvname = "ciao"+e;
    h_plot[e] = new TH2F(plotname, "", colno, 0, colno, rowno, 0, rowno); 
    // plot_mc_data[e] = new TCanvas(canvname,"Canvas_",700,550);//550
  
    //Fill Noise Map plots	
    for (int i=0; i<rowno; i++) {
      for (int j=0; j<colno; j++) {
	double tmp;
	infile >> tmp;
	h_plot[e]->SetBinContent(j+1,i+1,tmp);	
	if(e==0) h_plot[2]->SetBinContent(j+1,i+1,tmp);	
      }
    }
  
    // h_plot[e]->Draw("COLZ");
  }

  plot_mc_data[2] = new TCanvas("diff","Canvas_",700,550);//550
  h_plot[2]->Add(h_plot[1],-1);
  h_plot[2]->SetMaximum(max);
  h_plot[2]->SetMinimum(min);
  h_plot[2]->GetXaxis()->SetTitle("Column");
  h_plot[2]->GetYaxis()->SetTitle("Row");
  h_plot[2]->GetZaxis()->SetTitle("Noise");

  plot_mc_data[3] = new TCanvas("Canvas_1D","Canvas_1D",700,550);//550
  TH1D *h1 = new TH1D("1D","1D",1000,-500,500);

  double bc=1;

  for(int x = 1; x < colno; x++)
    for(int y = 1; y < rowno; y++)
      {
	bc=h_plot[2]->GetBinContent(x+1,y+1);
        if(bc!=0) h1->Fill(bc);
        //if(bc<50) h_plot[2]->SetBinContent(x+1,y+1,0);
      }

  h1->Draw("HIST");

  plot_mc_data[2]->cd();
  h_plot[2]->Draw("COLZ");

  if(PRINT=="Yes") {
    TString plotName="plots_to_eos/"+M_name+"_NoiseMap_"+volt+"V.png";
    plot_mc_data[2]->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }
  // plot_ca[0]->cd();
  // Occup_0_0_MA->Draw("COLZ");


  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           
      
  app.Run(true);
  return 0;
}
