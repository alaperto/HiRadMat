#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  TString PRINT = "Yes";
  TString fol = "./RD53_IV/";

  //TString M_name = "F02_91_02_25_1E";
  // TString M_name = "F02_02_50_1E_01";
  // TString M_name = "F02_02_50_1E_03";

  //TString M_name = "F02_03_01_25_1E";

   // TString M_name = "IFAE";

  // TString M_name = "F03_30_25x100-2E_A1.2";
  //TString M_name = "F03_30_25x100-1E_D5.3";//--> high 250uA;
  //TString M_name = "F03_30_25x100-1E_D9.5";//Probe
   TString M_name = "F03_30_25x100-1E_D11.3";
  // TString M_name = "F03_30_50x50-1E_E3.3";
  
  //TString M_name = "F02_05_25x100-1E_03";

  // TString M_name = "F03_30_25x100-2E_A8.5";
  //TString M_name = "F03_30_25x100-2E_B4.4";
  //TString M_name = "F03_30_25x100-2E_C4.3";

  //TString M_name = "F02_02_50_1E_01";
  
  TString invert = "Yes";
  //TString compare_b = "_mod", compare_w = "_waf", scale = "yes";
  TString compare_b = "_mod", compare_w = "_box3", scale = "yeso";
  //TString compare_b = "_irr-20", compare_w = "_irr-25", scale = "yes";
  //ifstream input3(fol+M_name+"_high.dat"); //bare module 0-50 to 250uA


   TString M_name2=M_name;
   
   // compare_b = "_mod"; compare_w = "_mod"; scale = "yeso";
   // M_name = "F02_91_02_25_1E";
   // M_name2 = "F02_02_50_1E_01";

  int ccc = 100;
  TString u[ccc];
  Float_t v[ccc], a[ccc];
  int cc=0;

  cout<<"Name 1 "<<fol+M_name+compare_b<<".dat"<<endl;
  cout<<"Name 1 "<<fol+M_name2+compare_w<<".dat"<<endl;

 
  ifstream input3(fol+M_name+compare_b+".dat"); //bare module 0 5 10-20-100
  while(!input3.eof()){
    input3 >>v[cc]>>a[cc];
    cout<<cc<<"  v  "<<v[cc]<<"  a  "<<a[cc]<<endl;
    cc++;
  }

  TString uB[ccc];
  Float_t vB[ccc], aB[ccc];
  int ccB=0;
 
  //How to extract numbers from table
  //Copy row, paste as column (special paste)
  //Paste in emacs file
  //Change commas in dots --> cat step0.txt | sed 's/,/\./g' > stepa.txt
  //Done 

  ifstream input3B(fol+M_name2+compare_w+".dat"); //from wafer 0-80/50
  while(!input3B.eof()){
    input3B >>vB[ccB]>>aB[ccB];
    cout<<ccB<<"  vB  "<<vB[ccB]<<"  aB  "<<aB[ccB]<<endl;
    ccB++;
  }

  TCanvas *c3 = new TCanvas("c3","c3",20,10,800,500);
  c3->cd();  
  TGraph *IV = new TGraph(cc,&v[0],&a[0]);
  if(scale=="yes") for (int i=0;i<IV->GetN();i++) IV->GetY()[i] /= 1e-6; 
  IV->SetTitle(M_name);
  IV->GetYaxis()->SetTitle("Current [#mu A]");
  IV->GetXaxis()->SetTitle("Volt [V]");
  // for (int i=0;i<IV->GetN();i++) IV->GetY()[i]/=(1.92);
  // IV->GetYaxis()->SetTitle("Leakage current [#muA/cm^{2}]");
  IV->SetMarkerStyle(20);
  IV->SetMarkerSize(1);
  //IV->SetMarkerColor(2);
  IV->Draw("ALP");
  IV->GetYaxis()->SetRangeUser(-0.5,10.5);
  //IV->GetYaxis()->SetRangeUser(-0.5,210.5);
  TGraph *IVB = new TGraph(ccB,&vB[0],&aB[0]);
  if(scale=="yes") for (int i=0;i<IVB->GetN();i++) IVB->GetY()[i] /= 1e-6; 
  IVB->SetTitle(M_name);
  IVB->GetYaxis()->SetTitle("Current [#muA]");
  IVB->GetXaxis()->SetTitle("Volt [V]");
  // for (int i=0;i<IVB->GetN();i++) IVB->GetY()[i]/=(1.92);
  // IVB->GetYaxis()->SetTitle("Leakage current [#muA/cm^{2}]");
  IVB->SetMarkerStyle(20);
  IVB->SetMarkerSize(1);
  IVB->SetMarkerColor(2);
  IVB->Draw("sameLP");

  if(invert=="Yes"){
    IVB->Draw("ALP");
    IV->Draw("sameLP");
  }

  TLegend *leg2 = new TLegend(0.15,0.87,0.25,0.73);
  leg2->SetBorderSize(0);
  leg2->SetTextSize(0.033);
  if(scale=="yes"){
    // leg2->AddEntry(IV,"Module");
    // leg2->AddEntry(IVB,"Wafer");
  }
  else{
    // leg2->AddEntry(IV,M_name+compare_b);
    // leg2->AddEntry(IVB,M_name2+compare_w);
    leg2->AddEntry(IV,"Unirradiated, 10 #circC");
    leg2->AddEntry(IVB,"5e15 n_{eq}/cm^{2}, -50 #circC");
  }
  leg2->Draw("SAME");   

  if(PRINT=="Yes") {
    TString plotName="plots_to_eos/"+M_name+"_IV.png";
    c3->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }

  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           

  app.Run(true);
  return 0;
}
