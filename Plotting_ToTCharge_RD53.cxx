#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h> 
#include <TDatime.h> 
#include <TF1.h> 
#include <TAxis.h>
#include <TFile.h>
#include <TMath.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TPaveStats.h>
#include <boost/format.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <TDatime.h>
//#include <typeinfo>
using namespace std;
TApplication app ("gui",0,NULL);

int main(){

  gStyle->SetOptFit(111);

  TString PRINT = "Yes";
  TString fol = "./RD53_ToTCharge/";

  //TString M_name = "F02_91_02_25_1E";
  // TString M_name = "F02_02_50_1E_01";
  TString M_name = "F02_02_50_1E_03";

  // TString Threshold = "1000";
  TString Threshold = "1500";

  // TString ToT_Tuning = "_8000_8BC_";
  TString ToT_Tuning = "_";

  int ccc = 100;
  TString u[ccc];
  Float_t v[ccc], a[ccc];
  int cc=0;
 
  ifstream input3(fol+M_name+"_"+Threshold+ToT_Tuning+"0V.txt");// 10 V bias
  while(!input3.eof()){
    input3 >>u[cc]>>u[cc]>>u[cc]>>u[cc]>>v[cc]>>u[cc]>>a[cc];
    cout<<cc*1000<<"  ToT  "<<v[cc]<<"  +/-  "<<a[cc]<<endl;
    cc++;
  }

  TString uB[ccc];
  Float_t vB[ccc], aB[ccc];
  int ccB=0;

  // M_name = "F02_02_50_1E_03";
  // Threshold = "1500";
  // ToT_Tuning ="_";
  
  ifstream input3B(fol+M_name+"_"+Threshold+ToT_Tuning+"0V.txt");// 0 V bias
  while(!input3B.eof()){
    input3B >>uB[ccB]>>uB[ccB]>>uB[ccB]>>uB[ccB]>>vB[ccB]>>uB[ccB]>>aB[ccB];
    cout<<ccB*1000<<"  ToT  "<<vB[ccB]<<"  +/-  "<<aB[ccB]<<endl;
    ccB++;
  }

  cc--;
  ccB--;

  Float_t Ex[ccc], x[ccc];
  for(int b = 0; b < cc+1; b++){
    x[b]=b*1000;
    Ex[b]=0.1;
    cout<<b<<"  "<<x[b]<<endl;
  }
   
  TCanvas *c3 = new TCanvas("c3","c3",20,10,800,500);
  c3->cd();  

  int st = 2;//*1000 => start from 2000e (2000e > 1000/1500e thresholds)
  TGraphErrors *IV = new TGraphErrors(cc-st,&x[st],&v[st],&Ex[st],&a[st]);
  IV->SetTitle(M_name);
  IV->GetYaxis()->SetTitle("Current [#mu A]");
  IV->GetXaxis()->SetTitle("Volt [V]");
  IV->SetMarkerStyle(20);
  IV->SetMarkerSize(1);
  IV->Draw("ALP");
  TGraphErrors *IVB = new TGraphErrors(ccB-st,&x[st],&vB[st],&Ex[st],&aB[st]);

  IV->SetTitle(M_name);
  IV->GetYaxis()->SetTitle("ToT [BC]");
  IV->GetXaxis()->SetTitle("Injected charge [e]");
  IV->SetMarkerStyle(20);
  IV->SetLineWidth(2);
  IV->Draw("ALP");
  IVB->SetMarkerStyle(20);
  IVB->SetLineWidth(2);
  IVB->SetLineColor(2);
  // IVB->Draw("sameLP");//Do not draw second plot

  c3->Update();

  // TLegend *leg2 = new TLegend(0.15,0.87,0.25,0.73);
  // leg2->SetBorderSize(0);
  // leg2->SetTextSize(0.033);
  // leg2->AddEntry(IVB,"0 V bias");
  // leg2->AddEntry(IV,"10 V bias");
  // leg2->Draw("SAME");   

  if(PRINT=="Yes") {
    TString plotName="plots_to_eos/"+M_name+"_ToTCharge.png";
    c3->Print(plotName.Data());
    cout<<endl<<"Saved files in: "<<plotName<<endl;
  }

  cout<<"______________________________________________________________"<<endl;
  cout<<"_________________ENJOY____&_____CLOSE_________________________"<<endl<<endl;
           

  app.Run(true);
  return 0;
}
